/* Fungsi formatRupiah */
function formatRupiah(angka, prefix) {
    if (angka) {
        var number_string = angka.replace(/[^,\d]/g, "").toString(),
            split = number_string.split(","),
            sisa = split[0].length % 3,
            rupiah = split[0].substr(0, sisa),
            ribuan = split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if (ribuan) {
            separator = sisa ? "." : "";
            rupiah += separator + ribuan.join(".");
        }

        rupiah = split[1] != undefined ? rupiah + "," + split[1] : rupiah;
        return prefix == undefined ? rupiah : rupiah ? "Rp. " + rupiah : "";
    }
}
function executeCurrency(obj, data = "") {
    if (data != "") {
        obj = $(data).find(".form-currency input");
    }
    if ($(obj).data("prefix")) {
        $(obj)
            .parent()
            .find(".label-currency")
            .text(formatRupiah($(obj).val(), $(obj).data("prefix")));
    } else {
        $(obj)
            .parent()
            .find(".label-currency")
            .text(formatRupiah($(obj).val()));
    }
}

// Add Jquery Lib Function
(function ($) {
    // Restricts input for the set of matched elements to the given inputFilter function.
    $.fn.inputFilter = function (inputFilter) {
        return this.on(
            "input keydown keyup mousedown mouseup select contextmenu drop",
            ".form-currency input",
            function () {
                if (inputFilter(this.value)) {
                    this.oldValue = this.value;
                    this.oldSelectionStart = this.selectionStart;
                    this.oldSelectionEnd = this.selectionEnd;
                } else if (this.hasOwnProperty("oldValue")) {
                    this.value = this.oldValue;
                    this.setSelectionRange(
                        this.oldSelectionStart,
                        this.oldSelectionEnd
                    );
                } else {
                    this.value = "";
                }
            }
        );
    };
})(jQuery);
$("body").inputFilter(function (value) {
    return /^\d*$/.test(value); // Allow digits only, using a RegExp
});

/* focusing onclick label */
$("body").on("click", ".form-currency", function () {
    getForm = $(this).find("input");
    valueForm = getForm.val();
    getForm.val("").focus().val(valueForm);
});

/* Event Keyup pada .form-currency */
$("body").on("keyup", ".form-currency input", function () {
    executeCurrency(this);
});

function load_curency(data) {
    executeCurrency(this, data);
}
