/* eslint-disable object-shorthand */

/* global Chart, coreui, coreui.Utils.getStyle, coreui.Utils.hexToRgba */

/**
 * --------------------------------------------------------------------------
 * CoreUI Boostrap Admin Template (v3.4.0): main.js
 * Licensed under MIT (https://coreui.io/license)
 * --------------------------------------------------------------------------
 */

/* eslint-disable no-magic-numbers */
// Disable the on-canvas tooltip
Chart.defaults.global.pointHitDetectionRadius = 1;
Chart.defaults.global.tooltips.enabled = false;
Chart.defaults.global.tooltips.mode = "index";
Chart.defaults.global.tooltips.position = "nearest";
Chart.defaults.global.tooltips.custom = coreui.ChartJS.customTooltips;
Chart.defaults.global.defaultFontColor = "#646470"; // eslint-disable-next-line no-unused-vars

var base_url = $("base").attr("href");

$.getJSON(
    base_url + "dashboard/flash_pelanggan/multiple",
    function (returnData) {
        var cardChart1 = new Chart(document.getElementById("card-chart1"), {
            type: "line",
            data: {
                labels: returnData.bulan,
                datasets: [
                    {
                        label: "Total Pelanggan",
                        backgroundColor: "transparent",
                        borderColor: "rgba(255,255,255,.55)",
                        data: returnData.total,
                    },
                ],
            },
            options: {
                maintainAspectRatio: false,
                legend: {
                    display: false,
                },
                scales: {
                    xAxes: [
                        {
                            gridLines: {
                                color: "transparent",
                                zeroLineColor: "transparent",
                            },
                            ticks: {
                                fontSize: 2,
                                fontColor: "transparent",
                            },
                        },
                    ],
                    yAxes: [
                        {
                            display: false,
                            beginAtZero: false,
                            ticks: {
                                display: false,
                            },
                        },
                    ],
                },
                elements: {
                    line: {
                        tension: 0.00001,
                        borderWidth: 1,
                    },
                    point: {
                        radius: 4,
                        hitRadius: 10,
                        hoverRadius: 4,
                    },
                },
            },
        }); // eslint-disable-next-line no-unused-vars
    }
).fail(function (data) {});

$.getJSON(base_url + "dashboard/flash_website/multiple", function (returnData) {
    var cardChart2 = new Chart(document.getElementById("card-chart2"), {
        type: "line",
        data: {
            labels: returnData.bulan,
            datasets: [
                {
                    label: "Pengunjung Website",
                    backgroundColor: "transparent",
                    borderColor: "rgba(255,255,255,.55)",
                    data: returnData.total,
                },
            ],
        },
        options: {
            maintainAspectRatio: false,
            legend: {
                display: false,
            },
            scales: {
                xAxes: [
                    {
                        gridLines: {
                            color: "transparent",
                            zeroLineColor: "transparent",
                        },
                        ticks: {
                            fontSize: 2,
                            fontColor: "transparent",
                        },
                    },
                ],
                yAxes: [
                    {
                        display: false,
                        beginAtZero: false,
                        ticks: {
                            display: false,
                        },
                    },
                ],
            },
            elements: {
                line: {
                    tension: 0.00001,
                    borderWidth: 1,
                },
                point: {
                    radius: 4,
                    hitRadius: 10,
                    hoverRadius: 4,
                },
            },
        },
    }); // eslint-disable-next-line no-unused-vars
}).fail(function (data) {});

$.getJSON(
    base_url + "dashboard/flash_transaksi/multiple",
    function (returnData) {
        var cardChart3 = new Chart(document.getElementById("card-chart3"), {
            type: "line",
            data: {
                labels: returnData.bulan,
                datasets: [
                    {
                        label: "Total Transaksi",
                        backgroundColor: "transparent",
                        borderColor: "rgba(255,255,255,.55)",
                        pointBackgroundColor:
                            coreui.Utils.getStyle("--warning"),
                        data: returnData.total,
                    },
                ],
            },
            options: {
                maintainAspectRatio: false,
                legend: {
                    display: false,
                },
                scales: {
                    xAxes: [
                        {
                            gridLines: {
                                color: "transparent",
                                zeroLineColor: "transparent",
                            },
                            ticks: {
                                fontSize: 2,
                                fontColor: "transparent",
                            },
                        },
                    ],
                    yAxes: [
                        {
                            display: false,
                            beginAtZero: false,
                            ticks: {
                                display: false,
                            },
                        },
                    ],
                },
                elements: {
                    line: {
                        tension: 0.00001,
                        borderWidth: 1,
                    },
                    point: {
                        radius: 4,
                        hitRadius: 10,
                        hoverRadius: 4,
                    },
                },
            },
        }); // eslint-disable-next-line no-unused-vars
    }
).fail(function (data) {});

$.getJSON(base_url + "dashboard/flash_website/multiple", function (returnData) {
    var cardChart2 = new Chart(document.getElementById("card-chart2"), {
        type: "line",
        data: {
            labels: returnData.bulan,
            datasets: [
                {
                    label: "Pengunjung Website",
                    backgroundColor: "transparent",
                    borderColor: "rgba(255,255,255,.55)",
                    data: returnData.total,
                },
            ],
        },
        options: {
            maintainAspectRatio: false,
            legend: {
                display: false,
            },
            scales: {
                xAxes: [
                    {
                        gridLines: {
                            color: "transparent",
                            zeroLineColor: "transparent",
                        },
                        ticks: {
                            fontSize: 2,
                            fontColor: "transparent",
                        },
                    },
                ],
                yAxes: [
                    {
                        display: false,
                        beginAtZero: true,
                        ticks: {
                            display: false,
                            min: -4,
                            max: 39,
                        },
                    },
                ],
            },
            elements: {
                line: {
                    tension: 0.00001,
                    borderWidth: 1,
                },
                point: {
                    radius: 4,
                    hitRadius: 10,
                    hoverRadius: 4,
                },
            },
        },
    }); // eslint-disable-next-line no-unused-vars
}).fail(function (data) {});

$.getJSON(base_url + "dashboard/flash_pendapatan", function (returnData) {
    var chartPendapatan = new Chart(
        document.getElementById("chart-pendapatan"),
        {
            type: "line",
            data: {
                labels: returnData.bulan,
                datasets: [
                    {
                        label: "Pendapatan",
                        backgroundColor: "transparent",
                        borderColor: coreui.Utils.getStyle("--info"),
                        pointHoverBackgroundColor: "#fff",
                        borderWidth: 2,
                        data: returnData.total,
                    },
                ],
            },
            options: {
                maintainAspectRatio: false,
                legend: {
                    display: false,
                },
                scales: {
                    xAxes: [
                        {
                            gridLines: {
                                drawOnChartArea: false,
                            },
                        },
                    ],
                    yAxes: [
                        {
                            ticks: {
                                beginAtZero: true,
                                maxTicksLimit: 5,
                            },
                            callback: function (value, index, values) {
                                // Convert the number to a string and splite the string every 3 charaters from the end
                                value = value.toString();
                                value = value.split(/(?=(?:...)*$)/);
                                // Convert the array to a string and format the output
                                value = value.join(".");
                                return "Rp. " + value;
                            },
                        },
                    ],
                },
                elements: {
                    line: {
                        borderWidth: 2,
                        tension: 0.00001,
                    },
                    point: {
                        radius: 4,
                        hitRadius: 10,
                        hoverRadius: 4,
                    },
                },
            },
        }
    ); // eslint-disable-next-line no-unused-vars
}).fail(function (data) {});

var chartMember = new Chart(document.getElementById("chart-member"), {
    type: "line",
    data: {
        labels: ["Januari", "Februari", "Maret", "April", "Mei"],
        datasets: [
            {
                label: "Android",
                backgroundColor: "transparent",
                borderColor: coreui.Utils.getStyle("--info"),
                pointHoverBackgroundColor: "#fff",
                borderWidth: 2,
                data: [0, 0, 0, 0, 0],
            },
            {
                label: "IOS",
                backgroundColor: "transparent",
                borderColor: coreui.Utils.getStyle("--primary"),
                pointHoverBackgroundColor: "#fff",
                borderWidth: 2,
                data: [0, 0, 0, 0, 0],
            },
            {
                label: "Website",
                backgroundColor: "transparent",
                borderColor: coreui.Utils.getStyle("--warning"),
                pointHoverBackgroundColor: "#fff",
                borderWidth: 2,
                data: [0, 13, 9, 9, 6],
            },
        ],
    },
    options: {
        maintainAspectRatio: false,
        legend: {
            display: false,
        },
        scales: {
            xAxes: [
                {
                    gridLines: {
                        drawOnChartArea: false,
                    },
                },
            ],
            yAxes: [
                {
                    ticks: {
                        beginAtZero: true,
                        maxTicksLimit: 5,
                    },
                },
            ],
        },
        elements: {
            line: {
                borderWidth: 2,
                tension: 0.00001,
            },
            point: {
                radius: 4,
                hitRadius: 10,
                hoverRadius: 4,
            },
        },
    },
});
//# sourceMappingURL=main.js.map
