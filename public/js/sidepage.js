var base_url = $("base").attr("href");
var modalLoading = "";
const sidePage = {
    admin: "users/admin_form",
};
$("#modalSide").modal({ backdrop: "static", show: false });
$("#modalSide").on("shown.coreui.modal", function (e) {
    var link = "";
    modal = $(this);
    modalContent = modal.find(".modal-content");
    modalPage = $(e.relatedTarget).data("page");
    link = base_url + modalPage;
    if ($(e.relatedTarget).attr("key")) {
        link = `${link}/` + $(e.relatedTarget).attr("key");
    }

    modalLoading = modalContent.html();
    modalContent.load(link, function (data) {
        ckEditor();
        tagify();
        load_curency(data);
    });
});
$("#modalSide").on("hidden.coreui.modal", function () {
    modal = $(this);
    modal.find(".modal-content").html(modalLoading);
});
