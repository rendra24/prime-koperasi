var url = $('.select2').attr('select-url');
var placeholder = $('.select2').attr('data-placeholder');
$('.select2').select2({
    minimumInputLength: 3,
    allowClear: true,
    placeholder: placeholder,
    ajax: {
       dataType: 'json',
       url: url,
       delay: 800,
       data: function(params) {
         return {
           search: params.term
         }
       },
       processResults: function (data, page) {
       return {
         results: data
       };
     },
   }
}).on('select2:select', function (evt) {
  var data = $(".select2 option:selected").text();
  alert("Data yang dipilih adalah "+data);
});