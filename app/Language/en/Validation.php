<?php

// override core en language system validation or define your own en language validation message

return [

    'alpha_dash'        => '{field} hanya boleh berisi karakter alfanumerik, garis bawah, dan tanda hubung!',
    'required'          => '{field} harus diisi !',
    'numeric'           => '{field} harus berupa angka !',
    'valid_email'       => '{field} tidak valid !',
    'is_unique'         => '{field} sudah terdaftar !',
    'uploaded'          => '{field} bukan file unggahan yang valid.',
    'max_size'          => 'File {field} terlalu besar.',
    'is_image'          => '{field} tidak valid, unggah file gambar.',
    'mime_in'           => '{field} tidak memiliki jenis ekstensi yang valid.',
    'img_scale'         => '{field} tidak memiliki ukuran yang benar.',
    'img_size'          => '{field} tidak memiliki ukuran yang benar.',
    'matches'           => '{field} tidak sama dengan {param}.',
];
