<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Masterangsuran extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'id' => ['type' => 'bigint', 'constraint' => 20, 'unsigned' => true, 'auto_increment' => true],
			'nama_angsuran' => ['type' => 'varchar', 'constraint' => 255],
			'angsuran' => ['type' => 'int', 'constraint' => 11],
			'flag' => ['type' => 'int', 'constraint' => 1],
			'created_at' => ['type' => 'datetime', 'null' => true],
			'updated_at' => ['type' => 'datetime', 'null' => true],
			'deleted_at' => ['type' => 'datetime', 'null' => true]
		]);
		$this->forge->addKey('id', true);
		$this->forge->createTable('data_angsuran', true);
	}

	public function down()
	{
		$this->forge->dropTable('data_angsuran');
	}
}