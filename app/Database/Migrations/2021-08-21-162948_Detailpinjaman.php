<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Detailpinjaman extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'id' => ['type' => 'bigint', 'constraint' => 20, 'unsigned' => true, 'auto_increment' => true],
			'id_transaksi_pinjaman' => ['type' => 'int', 'constraint' => 11],
			'angsuran' => ['type' => 'int', 'constraint' => 11],
			'cicilan' => ['type' => 'int', 'constraint' => 11],
			'bunga' => ['type' => 'float', 'constraint' => 11],
			'status_pembayaran' => ['type' => 'int', 'constraint' => 1],
			'created_at' => ['type' => 'datetime', 'null' => true],
			'updated_at' => ['type' => 'datetime', 'null' => true],
			'deleted_at' => ['type' => 'datetime', 'null' => true]
		]);
		$this->forge->addKey('id', true);
		$this->forge->createTable('transaksi_pinjaman_detail', true);
	}

	public function down()
	{
		$this->forge->dropTable('transaksi_pinjaman_detail');
	}
}