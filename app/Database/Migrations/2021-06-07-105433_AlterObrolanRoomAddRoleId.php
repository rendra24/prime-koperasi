<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AlterObrolanRoomAddRoleId extends Migration
{
	public function up()
	{
		$fields = [
			'role' => [
				'type' => 'tinyint',
				'constraint' => 1,
				'after' => 'id',
				'comment' => '1 = member, 2 = vendor',
			],
			'user_id' => [
				'type' => 'bigint',
				'constraint' => 20,
				'after' => 'role',
			],
		];

		$this->forge->addColumn('obrolan_room', $fields);
	}

	public function down()
	{
		$this->forge->dropColumn('obrolan_room', 'role');
		$this->forge->dropColumn('obrolan_room', 'user_id');
	}
}
