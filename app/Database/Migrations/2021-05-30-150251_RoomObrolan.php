<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class RoomObrolan extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'id' => [
				'type'           => 'varchar',
				'constraint'     => 225,
			],
			'nama_room' => [
				'type' => 'varchar',
				'constraint' => 225,
			],
			'pesan_baru_admin' => [
				'type' => 'int',
				'constraint' => 5,
			],
			'pesan_baru_member' => [
				'type' => 'int',
				'constraint' => 5,
			],
			'created_at' => ['type' => 'datetime',],
			'updated_at' => ['type' => 'datetime',],
		]);
		$this->forge->addKey('id', true);
		$this->forge->createTable('obrolan_room');
	}

	public function down()
	{
		$this->forge->dropTable('obrolan_room');
	}
}
