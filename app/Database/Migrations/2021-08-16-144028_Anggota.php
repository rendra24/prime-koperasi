<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Anggota extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'id' => ['type' => 'bigint', 'constraint' => 20, 'unsigned' => true, 'auto_increment' => true],
			'kode_anggota' => ['type' => 'varchar', 'constraint' => 50], 
			'nrp' => ['type' => 'int', 'constraint' => 11], 
			'nama' => ['type' => 'varchar', 'constraint' => 255],
			'pangkat' => ['type' => 'varchar', 'constraint' => 255],
			'jabatan' => ['type' => 'varchar', 'constraint' => 255],
			'alamat' => ['type' => 'varchar', 'constraint' => 255, 'null' => TRUE],
			'email' => ['type' => 'varchar', 'constraint' => 255, 'null' => TRUE],
			'password' => ['type' => 'varchar', 'constraint' => 255],
			'onesignal_token' => ['type' => 'varchar', 'constraint' => 255],
			'remember_token' => ['type' => 'varchar', 'constraint' => 100],
			'simpanan_wajib' => ['type' => 'int', 'constraint' => 11, 'default' => '0'],
			'simpanan_sukarela' => ['type' => 'int', 'constraint' => 11, 'default' => '0'],
			'simpanan_pokok' => ['type' => 'int', 'constraint' => 11, 'default' => '0'],
			'flag' => ['type' => 'int', 'constraint' => 1],
			'created_at' => ['type' => 'datetime', 'null' => true],
			'updated_at' => ['type' => 'datetime', 'null' => true],
			'deleted_at' => ['type' => 'datetime', 'null' => true]
		]);
		$this->forge->addKey('id', true);
		$this->forge->createTable('anggota', true);
	}

	public function down()
	{
		$this->forge->dropTable('anggota');
	}
}