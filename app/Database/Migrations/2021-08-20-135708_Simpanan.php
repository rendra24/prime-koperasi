<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Simpanan extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'id' => ['type' => 'int', 'constraint' => 11, 'unsigned' => true, 'auto_increment' => true],
			'kode_transaksi' => ['type' => 'varchar', 'constraint' => 50],
			'id_anggota' => ['type' => 'int', 'constraint' => 11],
			'id_simpanan' => ['type' => 'int', 'constraint' => 11],
			'total_simpanan' => ['type' => 'bigint', 'constraint' => 20],
			'saldo_akhir' => ['type' => 'bigint', 'constraint' => 20],
			'nama_anggota' => ['type' => 'varchar', 'constraint' => 255],
			'status_simpanan' => ['type' => 'int', 'constraint' => 1],
			'keterangan' => ['type' => 'varchar', 'constraint' => 100, 'null' => true],
			'created_at' => ['type' => 'datetime', 'null' => true],
			'updated_at' => ['type' => 'datetime', 'null' => true],
			'deleted_at' => ['type' => 'datetime', 'null' => true]
		]);
		$this->forge->addKey('id', true);
		$this->forge->createTable('transaksi_simpanan', true);
	}

	public function down()
	{
		$this->forge->dropTable('transaksi_simpanan');
	}
}