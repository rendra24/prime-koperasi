<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Transaksi extends Migration
{

	// status Transaksi
	// 1=masuk
	// 2=kelar
	public function up()
	{
		$this->forge->addField([
			'id' => ['type' => 'int', 'constraint' => 5, 'unsigned' => true, 'auto_increment' => true],
			'nama_transaksi' => ['type' => 'varchar', 'constraint' => 50],
			'created_at' => ['type' => 'datetime', 'null' => true],
			'updated_at' => ['type' => 'datetime', 'null' => true],
			'deleted_at' => ['type' => 'datetime', 'null' => true]
		]);
		$this->forge->addKey('id', true);
		$this->forge->createTable('data_transaksi', true);

		$this->forge->addField([
			'id' => ['type' => 'bigint', 'constraint' => 20, 'unsigned' => true, 'auto_increment' => true],
			'kode_transaksi' => ['type' => 'varchar', 'constraint' => 50],
			'id_transaksi' => ['type' => 'int', 'constraint' => 11],
			'id_asset' => ['type' => 'int', 'constraint' => 11],
			'id_anggota' => ['type' => 'int', 'constraint' => 11],
			'total' => ['type' => 'bigint', 'constraint' => 20],
			'saldo_akhir' => ['type' => 'bigint', 'constraint' => 20, 'default' => 0],
			'status_transaksi' => ['type' => 'int', 'constraint' => 1],
			'id_user' => ['type' => 'int', 'constraint' => 11],
			'keterangan' => ['type' => 'varchar', 'constraint' => 100, 'null' => true],
			'created_at' => ['type' => 'datetime', 'null' => true],
			'updated_at' => ['type' => 'datetime', 'null' => true],
			'deleted_at' => ['type' => 'datetime', 'null' => true]
		]);
		$this->forge->addKey('id', true);
		$this->forge->createTable('transaksi', true);
	}

	public function down()
	{
		$this->forge->dropTable('data_transaksi');
		$this->forge->dropTable('transaksi');
	}
}