<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AlterObrolanRoooAddPesanTerakhir extends Migration
{
	public function up()
	{
		$fields = [
			'pesan_terakhir' => [
				'type' => 'text',
				'after' => 'nama_room',
			],
		];

		$this->forge->addColumn('obrolan_room', $fields);
	}

	public function down()
	{
		$this->forge->dropColumn('obrolan_room', 'pesan_terakhir');
	}
}
