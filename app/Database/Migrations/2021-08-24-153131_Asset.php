<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Asset extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'id' => ['type' => 'int', 'constraint' => 11, 'unsigned' => true, 'auto_increment' => true],
			'nama_asset' => ['type' => 'varchar', 'constraint' => 50],
			'id_jenis_asset' => ['type' => 'int', 'constraint' => 11, 'comment' => '1=Cash 2=Bank'],
			'total' => ['type' => 'int', 'constraint' => 11],
			'nama_jenis_asset' => ['type' => 'varchar', 'constraint' => 255, 'comment' => 'Cash/Bank'],
			'flag' => ['type' => 'int', 'constraint' => 1],
			'created_at' => ['type' => 'datetime', 'null' => true],
			'updated_at' => ['type' => 'datetime', 'null' => true],
			'deleted_at' => ['type' => 'datetime', 'null' => true]
		]);
		$this->forge->addKey('id', true);
		$this->forge->createTable('data_asset', true);

		$this->forge->addField([
			'id' => ['type' => 'int', 'constraint' => 11, 'unsigned' => true, 'auto_increment' => true],
			'nama_bank' => ['type' => 'varchar', 'constraint' => 50],
			'no_rekening' => ['type' => 'int', 'constraint' => 11],
			'nama_rekening' => ['type' => 'varchar', 'constraint' => 255],
			'flag' => ['type' => 'int', 'constraint' => 1],
			'created_at' => ['type' => 'datetime', 'null' => true],
			'updated_at' => ['type' => 'datetime', 'null' => true],
			'deleted_at' => ['type' => 'datetime', 'null' => true]
		]);
		$this->forge->addKey('id', true);
		$this->forge->createTable('data_bank', true);
	}

	public function down()
	{
		$this->forge->dropTable('data_asset');
		$this->forge->dropTable('data_bank');
	}
}