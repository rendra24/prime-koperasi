<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Pelanggan extends Migration
{
	public function up()
	{
		/*
		 * Pelanggan
		 */
		$this->forge->addField([
			'id' => ['type' => 'bigint', 'constraint' => 20, 'unsigned' => true, 'auto_increment' => true],
			'nama' => ['type' => 'varchar', 'constraint' => 255],
			'email' => ['type' => 'varchar', 'constraint' => 255],
			'tgl_email_verifikasi' => ['type' => 'datetime', 'null' => true],
			'foto' => ['type' => 'varchar', 'constraint' => 255],
			'penyedia' => ['type' => 'varchar', 'constraint' => 255],
			'id_penyedia' => ['type' => 'varchar', 'constraint' => 255],
			'password' => ['type' => 'varchar', 'constraint' => 255],
			'api_token' => ['type' => 'varchar', 'constraint' => 80],
			'google_id_token' => ['type' => 'varchar', 'constraint' => 2048],
			'fcm_token' => ['type' => 'varchar', 'constraint' => 2048],
			'onesignal_token' => ['type' => 'varchar', 'constraint' => 255],
			'remember_token' => ['type' => 'varchar', 'constraint' => 100],
			'flag' => ['type' => 'int', 'constraint' => 1],
			'created_at' => ['type' => 'datetime', 'null' => true],
			'updated_at' => ['type' => 'datetime', 'null' => true],
			'deleted_at' => ['type' => 'datetime', 'null' => true]
		]);
		$this->forge->addKey('id', true);
		$this->forge->createTable('pelanggan', true);

		/*
		 * pelanggan_telepon
		 */
		$this->forge->addField([
			'id' 				=> ['type' => 'bigint', 'constraint' => 20, 'unsigned' => true, 'auto_increment' => true],
			'id_pelanggan' 		=> ['type' => 'bigint', 'constraint' => 20, 'null' => false],
			'telp' 				=> ['type' => 'varchar', 'constraint' => 20],
			'created_at' 		=> ['type' => 'datetime', 'null' => true],
			'updated_at' 		=> ['type' => 'datetime', 'null' => true],
			'deleted_at' 		=> ['type' => 'datetime', 'null' => true]
		]);
		$this->forge->addPrimaryKey('id', true);
		$this->forge->createTable('pelanggan_telp', true);

		/*
		 * pelanggan_alamat
		 */
		$this->forge->addField([
			'id' 				=> ['type' => 'bigint', 'constraint' => 20, 'unsigned' => true, 'auto_increment' => true],
			'id_pelanggan' 		=> ['type' => 'bigint', 'constraint' => 20, 'null' => false],
			'alamat' 			=> ['type' => 'text'],
			'latitude' 			=> ['type' => 'varchar', 'constraint' => 50],
			'longitude'			=> ['type' => 'varchar', 'constraint' => 50],
			'created_at' 		=> ['type' => 'datetime', 'null' => true],
			'updated_at' 		=> ['type' => 'datetime', 'null' => true],
			'deleted_at' 		=> ['type' => 'datetime', 'null' => true]
		]);
		$this->forge->addPrimaryKey('id', true);
		$this->forge->createTable('pelanggan_alamat', true);
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('pelanggan');
		$this->forge->dropTable('pelanggan_telp');
		$this->forge->dropTable('pelanggan_alamat');
	}
}
