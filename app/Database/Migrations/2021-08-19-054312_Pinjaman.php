<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Pinjaman extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'id' => ['type' => 'int', 'constraint' => 11, 'unsigned' => true, 'auto_increment' => true],
			'kode_transaksi' => ['type' => 'varchar', 'constraint' => 50],
			'id_asset' => ['type' => 'int', 'constraint' => 5],
			'id_anggota' => ['type' => 'int', 'constraint' => 11],
			'id_pinjaman' => ['type' => 'int', 'constraint' => 11],
			'total_pinjaman' => ['type' => 'bigint', 'constraint' => 20],
			'angsuran' => ['type' => 'int', 'constraint' => 11],
			'cicilan' => ['type' => 'int', 'constraint' => 11],
			'bunga' => ['type' => 'float', 'constraint' => 11],
			'nama_anggota' => ['type' => 'varchar', 'constraint' => 255],
			'status_pinjaman' => ['type' => 'int', 'constraint' => 1],
			'created_at' => ['type' => 'datetime', 'null' => true],
			'updated_at' => ['type' => 'datetime', 'null' => true],
			'deleted_at' => ['type' => 'datetime', 'null' => true]
		]);
		$this->forge->addKey('id', true);
		$this->forge->createTable('transaksi_pinjaman', true);
	}

	public function down()
	{
		$this->forge->dropTable('transaksi_pinjaman');
	}
}