<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class ObrolanAdmin extends Migration
{
	/**
	 * ROLE
	 * 0 - admin
	 * 1 - member
	 * 2 - vendor
	 */

	public function up()
	{
		$this->forge->addField([
			'id' => [
				'type'           => 'varchar',
				'constraint'     => 225,
			],
			'room_id' => [
				'type' => 'varchar',
				'constraint' => 225,
			],
			'role' => [
				'type' => 'tinyint',
				'constraint' => 1,
			],
			'user_id' => [
				'type' => 'bigint',
				'constraint' => 20,
			],
			'user_nama' => [
				'type' => 'varchar',
				'constraint' => 255,
			],
			'isi' => [
				'type' => 'text',
			],
			'read' => [
				'type' => 'tinyint',
				'constraint' => 1,
			],
			'created_at' => ['type' => 'datetime',],
			'updated_at' => ['type' => 'datetime',],
		]);
		$this->forge->addKey('id', true);
		$this->forge->createTable('obrolan');
	}

	public function down()
	{
		$this->forge->dropTable('obrolan');
	}
}
