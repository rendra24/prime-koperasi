<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Cms extends Migration
{
	public function up()
	{
		/*
		 * CMS Menu
		 */
		$this->forge->addField([
			'id' => ['type' => 'int', 'constraint' => 11, 'unsigned' => true, 'auto_increment' => true],
			'nama' => ['type' => 'varchar', 'constraint' => 225],
			'keterangan' => ['type' => 'text'],
			'img' => ['type' => 'varchar', 'constraint' => 50],
			'slug' => ['type' => 'varchar', 'constraint' => 225],
			'created_at' => ['type' => 'datetime'],
			'updated_at' => ['type' => 'datetime'],
		]);
		$this->forge->addKey('id', true);
		$this->forge->createTable('cms_menu', true);

		/*
		 * CMS Konten
		 */
		$this->forge->addField([
			'id' => ['type' => 'int', 'constraint' => 11, 'unsigned' => true, 'auto_increment' => true],
			'id_menu' => ['type' => 'int', 'constraint' => 11],
			'judul' => ['type' => 'varchar', 'constraint' => 225],
			'keterangan' => ['type' => 'text'],
			'file' => ['type' => 'varchar', 'constraint' => 50],
			'slug' => ['type' => 'varchar', 'constraint' => 225],
			'META_TITLE' => ['type' => 'text',],
			'META_DESC' => ['type' => 'text',],
			'META_KEYWORD' => ['type' => 'text',],
			'created_at' => ['type' => 'datetime',],
			'updated_at' => ['type' => 'datetime',],
		]);
		$this->forge->addKey('id', true);
		$this->forge->createTable('cms_konten', true);

		/*
		 * CMS Pengaturan
		 */
		$this->forge->addField([
			'id' => ['type' => 'int', 'constraint' => 11, 'unsigned' => true, 'auto_increment' => true],
			'judul' => ['type' => 'varchar', 'constraint' => 100],
			'data' => ['type' => 'text'],
			'flag' => ['type' => 'int', 'constraint' => 1],
			'created_at' => ['type' => 'datetime'],
			'updated_at' => ['type' => 'datetime'],
		]);
		$this->forge->addKey('id', true);
		$this->forge->createTable('cms_pengaturan', true);
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('cms_menu');
		$this->forge->dropTable('cms_konten');
		$this->forge->dropTable('cms_pengaturan');
	}
}
