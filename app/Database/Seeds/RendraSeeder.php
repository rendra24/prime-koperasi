<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class RendraSeeder extends Seeder
{
	public function run()
	{
		$db = \Config\Database::connect();
		$builder = $db->table('pembayaran')->truncate();
		$builder = $db->table('pelanggan')->truncate();
		$builder = $db->table('transaksi')->truncate();
		$builder = $db->table('transaksi_detail')->truncate();
		$builder = $db->table('transaksi_softcleaning')->truncate();

		$builder = $db->table('pembayaran');
		$builder->insert([
			'nama'      => 'Gopay',
			'midtrans' => 'gopay',
			'flag'		=> '1',
			'created_at'	=> date('Y-m-d H:i:s'),
			'updated_at'	=> date('Y-m-d H:i:s'),
		]);

		$builder->insert([
			'nama'      => 'BCA VA',
			'midtrans' => 'bca',
			'flag'		=> '1',
			'created_at'	=> date('Y-m-d H:i:s'),
			'updated_at'	=> date('Y-m-d H:i:s'),
		]);

		$builder->insert([
			'nama'      => 'BNI VA',
			'midtrans' => 'bni',
			'flag'		=> '1',
			'created_at'	=> date('Y-m-d H:i:s'),
			'updated_at'	=> date('Y-m-d H:i:s'),
		]);

		$builder = $db->table('pelanggan');

		$builder->insert([
			'nama'     					=> 'Rendra Saputra',
			'email' 					=> 'rendraseptian24@gmail.com',
			'tgl_email_verifikasi'		=> date('Y-m-d H:i:s'),
			'password'					=> password_hash('123456', PASSWORD_BCRYPT),
			'api_token'					=> '123456',
			'google_id_token'			=> '123456',
			'onesignal_token'			=> '123456',
			'flag'						=> 1,
			'created_at'				=> date('Y-m-d H:i:s'),
			'updated_at'				=> date('Y-m-d H:i:s'),
		]);

		$builder = $db->table('transaksi');

		$builder->insert([
			'order_id'     				=> rand(),
			'kode_transaksi' 			=> 'TR' . date('YmdHis'),
			'tgl_transaksi'				=> date('Y-m-d H:i:s'),
			'id_layanan'				=> 1,
			'id_pelanggan'				=> 1,
			'id_cabang'					=> 1,
			'kota'						=> 'Malang',
			'nama_pelanggan'			=> 'Rendra Saputra',
			'telp_pelanggan'			=> '085933008404',
			'alamat'					=> 'Jalan Prenjak Timur no 7',
			'latitude'					=>	'-7.992669',
			'longitude'					=> '112.617467',
			'total_harga'				=> '50000',
			'id_pembayaran'				=> 2,
			'status_transaksi'			=> 1,
			'created_at'				=> date('Y-m-d H:i:s'),
			'updated_at'				=> date('Y-m-d H:i:s'),
		]);

		$builder = $db->table('transaksi_detail');

		$builder->insert([
			'id_transaksi'		=> 1,
			'id_layanan_sub'	=> 1,
			'id_produk'			=> 1,
			'qty'				=> 2,
			'harga_satuan'		=> '139000',
			'sub_total'			=> '248000',
			'created_at'		=> date('Y-m-d H:i:s'),
			'updated_at'		=> date('Y-m-d H:i:s'),
		]);

		$builder = $db->table('transaksi_softcleaning');

		$builder->insert([
			'id_transaksi'		=> 1,
			'catatan'			=> 'deket masjid',
			'tgl_pengerjaan'	=> date('Y-m-d'),
			'jam_pengerjaan'	=> date('H:i:s'),
			'created_at'		=> date('Y-m-d H:i:s'),
			'updated_at'		=> date('Y-m-d H:i:s'),
		]);
	}
}
