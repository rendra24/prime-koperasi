<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class BasicData extends Seeder
{
	public function run()
	{
		$db = \Config\Database::connect();

		// Basic Data Layanan
		$builder = $db->table('layanan');
		$builder->truncate();

		$layanan = ['Soft Cleaning', 'Renovasi', 'Carwash'];
		foreach ($layanan as $item) {
			$data_layanan['nama'] = $item;
			$data_layanan['keterangan'] = '-';
			$data_layanan['created_at'] = date('Y-m-d H:i:s');
			$data_layanan['flag'] = '1';
			$builder->insert($data_layanan);
		}


		// Basic Data Sub Layanan
		$builder = $db->table('layanan_sub');
		$builder->truncate();

		$layanan_sub = [
			['id_layanan' => 1, 'nama' => 'Cuci'],
			['id_layanan' => 1, 'nama' => 'Vacuum'],
			['id_layanan' => 2, 'nama' => 'Renovasi'],
			['id_layanan' => 3, 'nama' => 'Cuci'],
			['id_layanan' => 3, 'nama' => 'Fogging'],
		];
		foreach ($layanan_sub as $item) {
			$data_layanansub['id_layanan'] = $item['id_layanan'];
			$data_layanansub['nama'] = $item['nama'];
			$data_layanansub['keterangan'] = '-';
			$data_layanansub['created_at'] = date('Y-m-d H:i:s');
			$data_layanansub['flag'] = '1';
			$builder->insert($data_layanansub);
		}


		// Basic Data Jam Layanan
		$builder = $db->table('layanan_jam');
		$builder->truncate();

		$layanan_jam = [
			['id_layanan' => 1, 'jam' => '09:00'],
			['id_layanan' => 1, 'jam' => '10:00'],
			['id_layanan' => 2, 'jam' => '09:00'],
			['id_layanan' => 2, 'jam' => '10:00'],
			['id_layanan' => 3, 'jam' => '09:00'],
			['id_layanan' => 3, 'jam' => '10:00'],
		];
		foreach ($layanan_jam as $item) {
			$data_layananjam['id_layanan'] = $item['id_layanan'];
			$data_layananjam['jam'] = $item['jam'];
			$data_layananjam['created_at'] = date('Y-m-d H:i:s');
			$builder->insert($data_layananjam);
		}


		// Basic data kategori produk
		$builder = $db->table('produk_kategori');
		$builder->truncate();

		$produk_kategori = ['Kecil', 'Sedang', 'Besar'];
		foreach ($produk_kategori as $item) {
			$kategori['id_layanan'] = 3;
			$kategori['nama'] = $item;
			$kategori['flag'] = 1;
			$kategori['created_at'] = date('Y-m-d H:i:s');
			$builder->insert($kategori);
		}


		// Basic data produk
		$builder = $db->table('produk');
		$builder->truncate();

		$produk = [
			[1, 1, null, 'Kursi Kantor', 29000, 29000, 29000],
			[1, 1, null, 'Kursi Meja Makan', 29000, 29000, 29000],
			[1, 1, null, 'Sofa Single', 58000, 58000, 58000],
			[1, 2, null, 'Single Bed', 139000, 139000, 139000],
			[1, 2, null, 'Queen Bed', 159000, 159000, 159000],
			[1, 2, null, 'Super King Bed', 189000, 189000, 189000],
			[3, 4, 1, 'Honda Brio', 50000, 50000, 50000],
			[3, 4, 2, 'Toyota Inova', 75000, 75000, 75000],
			[3, 4, 3, 'Mitsubishi Pajero Sport', 100000, 100000, 100000],
			[3, 5, 1, 'Honda Brio', 135000, 135000, 135000],
			[3, 5, 2, 'Toyota Inova', 135000, 135000, 135000],
			[3, 5, 3, 'Mitsubishi Pajero Sport', 135000, 135000, 135000],
		];
		foreach ($produk as $item) {
			$data['id_layanan'] = $item[0];
			$data['id_layanan_sub'] = $item[1];
			$data['id_produk_kategori'] = $item[2];
			$data['nama'] = $item[3];
			$data['harga'] = $item[4];
			$data['harga_vendor'] = $item[5];
			$data['harga_owner'] = $item[6];
			$data['created_at'] = date('Y-m-d H:i:s');
			$data['flag'] = '1';
			$builder->insert($data);
		}
	}
}
