<?php

namespace App\Database\Seeds;

class Cms extends \CodeIgniter\Database\Seeder
{
    public function run()
    {
        // Basic data cms
        $builder = $this->db->table('cms_menu');
        $builder->truncate();

        $data_menu['nama'] = 'About Us';
        $data_menu['slug'] = 'about-us';
        $data_menu['created_at'] = date('Y-m-d H:i:s');
        $builder->insert($data_menu);

        $builder = $this->db->table('cms_konten');
        $builder->truncate();

        $data_konten['id_menu'] = '1';
        $data_konten['created_at'] = date('Y-m-d H:i:s');
        $builder->insert($data_konten);
    }
}
