<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class Adminpermisiondata extends Seeder
{
	public function run()
	{
		$model = model('AuthPermissionModel');

		$data = [
			['name' => 'Manajemen Pengguna - Hak Akses'],
			['name' => 'Manajemen Pengguna - Hak Akses - Mengelola Data'],
			['name' => 'Manajemen Pengguna - Admin'],
			['name' => 'Manajemen Pengguna - Admin - Mengelola Data'],
			['name' => 'Manajemen Pengguna - Vendor / Penyedia'],
			['name' => 'Manajemen Pengguna - Vendor / Penyedia - Mengelola Data'],
			['name' => 'Manajemen Pengguna - Member'],
			['name' => 'Manajemen Pengguna - Member - Mengelola Data'],
			['name' => 'Obrolan'],
			['name' => 'Layanan - Area Layanan'],
			['name' => 'Layanan - Area Layanan - Mengelola Data'],
			['name' => 'Layanan - Rawatin Yuk'],
			['name' => 'Layanan - RawatinYuk - Mengelola Data'],
			['name' => 'Layanan - RenovinYuk'],
			['name' => 'Layanan - RenovinYuk - Mengelola Data'],
			['name' => 'Layanan - Carwash'],
			['name' => 'Layanan - Carwash - Mengelola Data'],
			['name' => 'Pesanan - RawatinYuk'],
			['name' => 'Pesanan - RenovinYuk'],
			['name' => 'Pesanan - Carwash'],
			['name' => 'Laporan - Transaksi'],
			['name' => 'Laporan - Pemasukan'],
			['name' => 'Voucher'],
			['name' => 'Voucher - Mengelola Data'],
			['name' => 'CMS - Iklan'],
			['name' => 'CMS - Iklan -Mengelola Data'],
			['name' => 'CMS - Parter'],
			['name' => 'CMS - Parter - Mengelola Data'],
			['name' => 'CMS - Testimoni'],
			['name' => 'CMS - Testimoni - Mengelola Data'],
			['name' => 'CMS - Menu'],
			['name' => 'CMS - Menu - Mengelola Data'],
			['name' => 'CMS - Pengaturan'],
			['name' => 'CMS - Berita Dan Blog'],
			['name' => 'CMS - Berita Dan Blog - Mengelola Data'],
			['name' => 'CMS - FAQ'],
			['name' => 'CMS - FAQ - Mengelola Data'],
		];

		foreach ($data as $item) {
			$model->save($item);
		}
	}
}
