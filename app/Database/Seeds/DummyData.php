<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;
use Faker\Factory;

class DummyData extends Seeder
{
	public function run()
	{
		$faker = Factory::create('id_ID');

		$db = \Config\Database::connect();

		$pelanggan = $db->table('pelanggan');
		$pelanggan_telp = $db->table('pelanggan_telp');
		$pelanggan_alamat = $db->table('pelanggan_alamat');

		$pelanggan->truncate();
		$pelanggan_telp->truncate();
		$pelanggan_alamat->truncate();
		$dataPelanggan = [];
		for ($i = 0; $i < 10; $i++) {
			$pnama = $faker->name;
			$ptelp = $faker->phoneNumber;

			$pelanggan->insert([
				'nama' => $pnama,
				'email' => $faker->email,
				'flag' => 1,
			]);
			$id_pelanggan = $db->insertID();

			$pelanggan_telp->insert([
				'id_pelanggan' => $id_pelanggan,
				'telp' => $ptelp,
			]);

			$pelanggan_alamat->insert([
				'id_pelanggan' => $id_pelanggan,
				'alamat' => $faker->address,
				'latitude' => $faker->latitude,
				'longitude' => $faker->longitude,
			]);
			$dataPelanggan[$id_pelanggan] = [$pnama, $ptelp];
		}

		$loop = 100;

		$builder = $db->table('transaksi');
		$buildersc = $db->table('transaksi_softcleaning');
		$builderrn = $db->table('transaksi_renovasi');
		$buildercw = $db->table('transaksi_carwash');
		$builderd = $db->table('transaksi_detail');

		$builder->truncate();
		$buildersc->truncate();
		$builderrn->truncate();
		$buildercw->truncate();
		$builderd->truncate();

		for ($i = 0; $i < $loop; $i++) {
			$random_member = $faker->numberBetween($min = 1, $max = 10);
			$layanan = $faker->numberBetween($min = 1, $max = 3);
			$max_detail = $faker->numberBetween($min = 1, $max = 5);
			$tanggal_transaksi = $faker->dateTimeBetween('-7 day', '+7 day', 'Asia/Jakarta');

			if ($layanan == 2) {
				$statusTrans = $faker->numberBetween($min = 0, $max = 1);
			} else {
				$statusTrans = $faker->numberBetween($min = 0, $max = 4);
			}

			$builder->insert([
				'order_id' => strtoupper($faker->bothify('##?##-??????????')),
				'kode_transaksi' => strtoupper($faker->bothify('##?##-??????????')),
				'tgl_transaksi' => $tanggal_transaksi->format('Y-m-d H:i:s'),
				'id_layanan' => $layanan,
				'id_pelanggan' => $random_member,
				'nama_pelanggan' => $dataPelanggan[$random_member][0],
				'telp_pelanggan' => $dataPelanggan[$random_member][1],
				'alamat' => $faker->address,
				'latitude' => $faker->latitude,
				'longitude' => $faker->longitude,
				'id_pembayaran' => '1',
				'status_transaksi' => $statusTrans,
				'created_at' => $tanggal_transaksi->format('Y-m-d H:i:s'),
			]);
			$trID = $db->insertID();
			$trDate = $tanggal_transaksi->format('Y-m-d H:i:s');

			if ($layanan == 1) {
				$buildersc->insert([
					'id_transaksi' => $trID,
					'catatan' => $faker->text(200),
					'tgl_pengerjaan' => date('Y-m-d', strtotime($trDate . ' +2 day')),
					'jam_pengerjaan' => $faker->time('H:i', '21:00'),
				]);
			} else if ($layanan == 2) {
				$builderrn->insert([
					'id_transaksi' => $trID,
					'catatan' => $faker->text(200),
					'tgl_survei' => date('Y-m-d', strtotime($trDate . ' +2 day')),
					'jam_survei' => $faker->time('H:i', '21:00'),
					'lampiran_foto' => 'test.png',
					'kebutuhan' => $faker->text(50),
					'anggaran' => 200000,
				]);
			} else {
				$buildercw->insert([
					'id_transaksi' => $trID,
					'catatan' => $faker->text(200),
					'tgl_pengerjaan' => date('Y-m-d', strtotime($trDate . ' +2 day')),
					'jam_pengerjaan' => $faker->time('H:i', '21:00'),
				]);
			}

			$gt = 0;
			if ($layanan != 2) {
				$builderProd = $db->table('produk');
				$builderProd->where('id_layanan', $layanan);
				$get_produk = $builderProd->get()->getResultArray();

				for ($id = 0; $id < $max_detail; $id++) {
					$prod = $faker->numberBetween($min = 0, $max = (count($get_produk) - 1));

					$builderd->insert([
						'id_transaksi' => $trID,
						'id_layanan_sub' => $get_produk[$prod]['id_layanan_sub'],
						'id_produk_kategori' => $get_produk[$prod]['id_produk_kategori'],
						'id_produk' => $get_produk[$prod]['id'],
						'qty' => 1,
						'harga_satuan' => $get_produk[$prod]['harga'],
						'sub_total' => $get_produk[$prod]['harga'],
					]);

					$gt += $get_produk[$prod]['harga'];
				}
				$builder->where('id', $trID);
				$builder->update([
					'total_harga' => $gt,
				]);
			}
		}
	}
}
