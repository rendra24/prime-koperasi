<?php

namespace App\Controllers;

use App\Controllers\BaseController;

class Transfer extends BaseController
{
	public function index()
	{
		$data['menu'] = 'transfer';
        return view('transfer/data', $data);
	}

	public function data(){
		$model = model('TransaksiModel');
		$post = $this->request->getVar();
		$data = $model->LaporanTransaksi()->getResultArray();

		$gData['table']['title'] = 'No|Kode Transaksi|Keterangan|Tanggal Transaksi|Total|Jenis Transaksi|Status';
        $gData['table']['data'] = 'no|kode_transaksi|keterangan|created_at|total|nama_transaksi|status_transaksi';
        $gData['data'] = $data;
        $gData['no'] = true;
        $gData['helper']['created_at'] = ['formatTglIndo', '{data}'];
		$gData['helper']['total'] = ['formatDecimals', '{data}|{status_transaksi}'];
		$gData['helper']['status_transaksi'] = ['formatStatus', 'transaksi|{data}'];
        $data = jsonGenrateTables('success', $gData);
        return $this->respond($data);
	}

	public function form($id = null)
    {  
       
		$asset = model('MasterAsset');
		$data['asset'] = $asset->findAll();
        return view('transfer/_form',$data);
    }

	public function save(){
        $model = model('TransaksiModel');
		$ModAsset = model('MasterAsset');
        $validation =  \Config\Services::validation();
        $post = $this->request->getPost();
		

        if (!$validation->run($post, 'transfer')) {
            return $this->respond(jsonRes('validation_error', $validation->getErrors()));
        } else {
			
			if($post['id_asset_dari']){
				$id_asset = $post['id_asset_dari'];
				$asset = $ModAsset->where('id',$id_asset)->first();

            	$post['kode_transaksi'] = 'TF'.rand();
				$post['id_transaksi'] = 6;
				$post['saldo_akhir'] = $asset['total'] - $post['total'];
				$post['status_transaksi'] = 2;
				$add['total'] = $asset['total'] - $post['total'];
		
            	$model->save($post);

				$add['id'] = $id_asset;
				$ModAsset->save($add);

			}

			if($post['id_asset_ke']){
				$id_asset = $post['id_asset_ke'];
				$asset = $ModAsset->where('id',$id_asset)->first();

            	$post['kode_transaksi'] = 'TF'.rand();
				$post['id_transaksi'] = 6;
				$post['saldo_akhir'] = $asset['total'] + $post['total'];
				$post['status_transaksi'] = 1;
				$add['total'] = $asset['total'] + $post['total'];
		
            	$model->save($post);

				$add['id'] = $id_asset;
				$ModAsset->save($add);

			}
			
            return $this->respond(jsonRes('success'));
        }
        
    }
}