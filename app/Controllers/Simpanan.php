<?php

namespace App\Controllers;

use App\Controllers\BaseController;

class Simpanan extends BaseController
{
    public function index()
    {
        $data['menu'] = 'simpanan';
        return view('transaksi/simpanan_data', $data);
    }

    public function data()
    {
        $model = model('SimpananModel');
        $data = $model->getSimpanan()->getResultArray();

        $gData['table']['title'] = 'No|Anggota|Jenis Simpanan|Total|Tanggal Simpan|Aksi';
        $gData['table']['data'] = 'no|nama_anggota|nama_simpanan|total_simpanan|created_at|action';
        $gData['data'] = $data;
        $gData['no'] = true;
        $gData['action'] = 'detail';
        $gData['url']['detail'] = 'simpanan/detail';
        $gData['helper']['total_simpanan'] = ['formatdecimals', '{data}'];
        $gData['helper']['created_at'] = ['formatTglIndo', '{data}'];

        $data = jsonGenrateTables('success', $gData);
        return $this->respond($data);
    }

    public function data_excel()
    {
        $model = model('Simpananexcel');
        $data = $model->getSimpanan()->getResultArray();

        $gData['table']['title'] = 'No|NRP|Anggota|Pokok|Wajib|Sukarela|Tabungan|Aksi';
        $gData['table']['data'] = 'no|nrp|nama|pokok|wajib|sukarela|tabungan|action';
        $gData['data'] = $data;
        $gData['no'] = true;
        $gData['action'] = 'delete';
        $gData['url']['delete'] = 'import/simpanan/delete_excel/';

        $data = jsonGenrateTables('success', $gData);
        return $this->respond($data);
    }

    public function form_excel()
    {
        $model = model('Simpananexcel');
        $data['total'] = $model->countAll();
        $data['menu'] = 'simpanan';
        return view('transaksi/simpanan_excel', $data);
    }



    public function form()
    {
        $model = model('Mastersimpanan');
        $asset = model('MasterAsset');
        $data['simpanan'] = $model->findAll();
        $data['asset'] = $asset->findAll();
        return view('transaksi/form_simpanan', $data);
    }

    public function detail($id)
    {
        $model = model('SimpananModel');
        $data = $model->where('id', $id)->first();
        return view('transaksi/detail_simpanan', $data);
    }


    public function save()
    {
        $model = model('SimpananModel');
        $transaksi = model('TransaksiModel');
        $anggota = model('AnggotaModels');
        $ModAsset = model('MasterAsset');

        $validation =  \Config\Services::validation();
        $post = $this->request->getPost();

        if (!$validation->run($post, 'transaksi_simpanan')) {
            return $this->respond(jsonRes('validation_error', $validation->getErrors()));
        } else {
            $id_asset = $post['id_asset'];
            $asset = $ModAsset->where('id', $id_asset)->first();

            $kode_transaksi = 'TR' . rand();
            $post['kode_transaksi'] = $kode_transaksi;
            $model->save($post);

            $add['kode_transaksi'] = $kode_transaksi;
            $add['status_transaksi'] = 1;
            $add['id_transaksi'] = 1;
            $add['total'] = $post['total_simpanan'];
            $add['id_anggota'] = $post['id_anggota'];
            $add['id_user'] = user()->id;
            $add['created_at'] = date('Y-m-d H:i:s');
            $add['updated_at'] = date('Y-m-d H:i:s');
            $transaksi->saveTransaksi($add);

            $upp['total'] = $asset['total'] + $post['total_simpanan'];
            $upp['id'] = $id_asset;
            $ModAsset->save($upp);



            $anggota->updaetSaldo($post['id_anggota'], $post['total_simpanan'], $post['id_simpanan']);

            return $this->respond(jsonRes('success'));
        }
    }
}
