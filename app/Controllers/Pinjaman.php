<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use Exception;

class Pinjaman extends BaseController
{
	public function index(){
        $data['menu'] = 'pinjaman';
        return view('transaksi/pinjaman_data', $data);
    }

	public function data()
    {
        $model = model('PinjamanModel');
        $data = $model->getPinjaman()->getResultArray();

        $gData['table']['title'] = 'No|Anggota|Total|Angsuran|Bunga|Pinjaman|Tanggal Pengajuan|Status|Aksi';
        $gData['table']['data'] = 'no|nama_anggota|total_pinjaman|angsuran|bunga|nama_pinjaman|created_at|status_pinjaman|action';
        $gData['data'] = $data;
        $gData['no'] = true;
        $gData['action'] = 'detail|print';
        $gData['url']['detail'] = 'pinjaman/detail';
        $gData['url']['print'] = 'pinjaman/perjanjian';
		$gData['helper']['total_pinjaman'] = ['formatdecimals', '{data}'];
        $gData['helper']['status_pinjaman'] = ['formatStatus', 'pinjaman|{data}'];
		$gData['helper']['angsuran'] = ['formatAngsuran', '{data}'];
		$gData['helper']['created_at'] = ['formatTglIndo', '{data}'];
		$gData['helper']['bunga'] = ['formatPersen', '{data}'];
		
        $data = jsonGenrateTables('success', $gData);
        return $this->respond($data);
    }

	public function form()
    {  
		$model = model('Masterpinjaman');
		$angsuran = model('MasterAngsuran');
        $asset = model('MasterAsset');
		$data['pinjaman'] = $model->findAll();
		$data['angsuran'] = $angsuran->findAll();
        $data['asset'] = $asset->findAll();
        return view('transaksi/form_pinjaman',$data);
    }

	public function detail($id){
		$model = model('PinjamanModel');
		$data = $model->where('id',$id)->first();
		$data['detail'] = $model->get_detail($id)->getResultArray();
        return view('transaksi/detail_pinjaman',$data);
	}

	public function verifikasi(){
		$model = model('PinjamanModel');

        if ($post = $this->request->getPost()) {
			$model->verifikasiPinjaman($post);
            return $this->respond(jsonRes('success'));
            
        } else {
            $body = "Halaman tidak ada";
			return $this->response->setStatusCode(404)->setBody($body);
        }
	}

    public function tolak(){
        $model = model('PinjamanModel');

        if ($post = $this->request->getPost()) {
            $data['id'] = $post['id'];
            $data['status_pinjaman'] = 3;
            if($model->save($data)){
                return $this->respond(jsonRes('success'));
            }
            
        } else {
            $body = "Halaman tidak ada";
            return $this->response->setStatusCode(404)->setBody($body);
        }
    }

	public function angsur(){
		$model = model('PinjamanModel');
		if ($post = $this->request->getPost()) {
			$model->angsurPinjaman($post);
            return $this->respond(jsonRes('success'));
            
        } else {
            $body = "Halaman tidak ada";
			return $this->response->setStatusCode(404)->setBody($body);
        }
	}

    public function save(){
        $model = model('PinjamanModel');
        $validation =  \Config\Services::validation();
        $post = $this->request->getPost();
        
        $Modasset = model('MasterAsset');
            $asset = $Modasset->getsaldo($post['id_asset'])->getRowArray();
            if((int)$post['total'] <= (int)$asset['total']){
                
                if (!$validation->run($post, 'transaksi_pinjaman')) {
                    return $this->respond(jsonRes('validation_error', $validation->getErrors()));
                } else {
                    $post['total_pinjaman'] = $post['total'];
                    $model->save($post);
                    return $this->respond(jsonRes('success'));
                }

            }else{
                $error = array('msg' => 'Kas tidak cukup');
                return $this->respond(jsonRes('error', $error));
                die;
            }         
        
    }

    public function perjanjian($id=''){
        $model = model('PinjamanModel');
        $anggota = model('AnggotaModels');
        $data = $model->where('id',$id)->first();
        $anggota = $anggota->where('id',$data['id_anggota'])->first();
        $data['anggota'] = $anggota;
        return view('surat/ajuan_pinjaman', $data);
    }
}