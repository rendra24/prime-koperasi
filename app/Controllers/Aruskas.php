<?php

namespace App\Controllers;

use App\Controllers\BaseController;

class Aruskas extends BaseController
{
	public function index()
	{
		if($this->request->getVar()){
			$post = $this->request->getVar();
			$data['request'] = $post;
		}
		$data['menu'] = 'aruskas';
        return view('aruskas/data', $data);
	}

	public function data(){
		$model = model('LaporanModel');
		$post = $this->request->getVar();
		$data = $model->LaporanTransaksi($post)->getResultArray();

		$gData['table']['title'] = 'No|Kode Transaksi|Keterangan|Tanggal Transaksi|Total|Jenis Transaksi|Status';
        $gData['table']['data'] = 'no|kode_transaksi|keterangan|created_at|total|nama_transaksi|status_transaksi';
        $gData['data'] = $data;
        $gData['no'] = true;
        $gData['helper']['created_at'] = ['formatTglIndo', '{data}'];
		$gData['helper']['total'] = ['formatDecimals', '{data}|{status_transaksi}'];
		$gData['helper']['status_transaksi'] = ['formatStatus', 'transaksi|{data}'];
        $data = jsonGenrateTables('success', $gData);
        return $this->respond($data);
	}

	public function form($id = null)
    {  
        $data = [];
		$asset = model('MasterAsset');
		$data['asset'] = $asset->findAll();
        if($id != null){
            $model = model('LaporanModel');
            $data = $model->first($id);
        }
        return view('aruskas/_form',$data);
    }

	public function save(){
        $model = model('TransaksiModel');
		$ModAsset = model('MasterAsset');
        $validation =  \Config\Services::validation();
        $post = $this->request->getPost();
		$id_asset = $post['id_asset'];
		$asset = $ModAsset->where('id',$id_asset)->first();

		$rules = [
            'status_transaksi' => 'required',
            'id_asset' => 'required',
			'total' => 'required|CekTotalAsset[id_asset, total]',
			'keterangan' => 'required',
        ];


        $errors = [
            'status_transaksi' => [
                'required' => 'Jenis Kas harus diisi'
			],
			'id_asset' => [
                'required' => 'Jenis Asset harus diisi'
            ],
			'total' => [
                'required' => 'Total Kas harus diisi',
				'CekTotalAsset' => 'Kas Asset Tidak cukup'
            ],
			'keterangan' => [
                'required' => 'Keterangan harus diisi'
            ],
        ];

		$input = $this->getRequestInput($this->request);
    

        if (!$this->validateRequest($input, $rules, $errors)) {
			return $this->respond(jsonRes('validation_error', $validation->getErrors()));
        } else {
			$kode_transaksi = 'AK'.rand();
            $post['kode_transaksi'] = $kode_transaksi;

			if($post['status_transaksi'] == '1'){
				$post['id_transaksi'] = 4;
				$post['saldo_akhir'] = $asset['total'] + $post['total'];

				$add['total'] = $asset['total'] + $post['total'];
			}else{
				$post['id_transaksi'] = 5;
				$post['saldo_akhir'] = $asset['total'] - $post['total'];

				$add['total'] = $asset['total'] - $post['total'];
			}
            $model->save($post);

			$add['id'] = $id_asset;
			$ModAsset->save($add);
			
            return $this->respond(jsonRes('success'));
        }
        
    }
}