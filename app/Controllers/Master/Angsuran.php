<?php

namespace App\Controllers\Master;

use App\Controllers\BaseController;

class Angsuran extends BaseController
{
	public function index(){
        $data['menu'] = 'master';
        $data['submenu'] = 'angsuran';
        return view('angsuran/data', $data);
    }

	public function data()
    {
        $model = model('MasterAngsuran');
        $data = $model
            ->orderBy('created_at', 'desc')
            ->findAll();

        $gData['table']['title'] = 'No|Angsuran|Status|Aksi';
        $gData['table']['data'] = 'no|nama_angsuran|flag|action';
        $gData['data'] = $data;
        $gData['no'] = true;
        $gData['action'] = 'edit|delete';
        $gData['url']['edit'] = 'master/angsuran/form';
        $gData['url']['delete'] = 'angsuran/admin_save';
        $gData['helper']['flag'] = ['formatStatus', 'anggota|{data}'];
        $data = jsonGenrateTables('success', $gData);
        return $this->respond($data);
    }

	public function form($id = null)
    {  
        $data = [];
        if($id != null){
            $model = model('MasterAngsuran');
            $data = $model->where('id',$id)->first();
    
        }
        return view('angsuran/_form',$data);
    }

    public function save(){
        $model = model('MasterAngsuran');
        $validation =  \Config\Services::validation();
        $post = $this->request->getPost();

        if (!$validation->run($post, 'angsuran')) {
            return $this->respond(jsonRes('validation_error', $validation->getErrors()));
        } else {
            $model->save($post);
            return $this->respond(jsonRes('success'));
        }
        
    }
}