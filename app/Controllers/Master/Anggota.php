<?php

namespace App\Controllers\Master;
use App\Controllers\BaseController;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Anggota extends BaseController
{
    public function index(){
        
        $data['menu'] = 'master';
        $data['submenu'] = 'anggota';
        return view('anggota/data', $data);
    }

    

    public function anggota_data()
    {
        $model = model('AnggotaModels');
        $data = $model
            ->orderBy('created_at', 'desc')
            ->findAll();

        $gData['table']['title'] = 'No|Nama|NRP/NIP|Wajib|Sukarela|Tanggal Daftar|Status|Aksi';
        $gData['table']['data'] = 'no|nama|nrp|simpanan_wajib|simpanan_sukarela|created_at|flag|action';
        $gData['data'] = $data;
        $gData['no'] = true;
        $gData['action'] = 'edit|delete|reset|password';
        $gData['helper']['simpanan_wajib'] = ['formatDecimals', '{data}'];
        $gData['helper']['simpanan_sukarela'] = ['formatDecimals', '{data}'];
        $gData['url']['edit'] = 'master/anggota/form';
        $gData['url']['delete'] = 'users/admin_save';
        $gData['url']['reset'] = 'master/anggota/reset_saldo';
        $gData['url']['password'] = 'master/anggota/reset_password';
        $gData['helper']['created_at'] = ['formatTglIndo', '{data}'];
        $gData['helper']['flag'] = ['formatStatus', 'anggota|{data}'];
        $data = jsonGenrateTables('success', $gData);
        return $this->respond($data);
    }

    public function form($id = null)
    {  
        $data = [];
        if($id != null){
            $model = model('AnggotaModels');
            $data = $model->where('id',$id)->first();
        }else{
            $data['kode_anggota'] = 'AGT'.rand();
        }
        return view('anggota/_form',$data);
    }

    public function form_excel()
    {  
        $data = [];
        return view('anggota/form_excel',$data);
    }
    

    public function save(){
        $model = model('AnggotaModels');
        $validation =  \Config\Services::validation();
        $post = $this->request->getPost();

        if (!$validation->run($post, 'anggota')) {
            return $this->respond(jsonRes('validation_error', $validation->getErrors()));
        } else {
            if(!isset($post['id'])){
                $post['password'] = password_hash($post['password'], PASSWORD_BCRYPT);
                $post['flag'] = 1;
            }

            if(!empty($post['flag'])){
                $post['flag'] = 1;
            }else{
                $post['flag'] = 0;
            }
            
            $model->save($post);
            return $this->respond(jsonRes('success'));
        }
        
    }

    public function get_anggota(){
        $model = model('AnggotaModels');
        $cari = $this->request->getVar('search');
        $data = $model->get_anggota($cari)->getResultArray();
        $list = array();
        foreach($data as $row):
            $list[] = array('id' => $row['id'], 'text' => $row['nama']);
        endforeach;

        return json_encode($list);
        
    }

    public function reset_password($id_anggota){
        $model = model('AnggotaModels');

        $get_anggota = $model->where('id',$id_anggota)->first();

        $post['id'] = $id_anggota;
        $post['password'] = password_hash($get_anggota['nrp'], PASSWORD_BCRYPT);
        
        if($model->save($post)){
            return $this->respond(jsonRes('success'));
        }

    }

    public function reset_saldo($id_anggota)
    {
        $modelTr = model('TransaksiModel');
        $model = model('AnggotaModels');
        $ModAsset = model('MasterAsset');
        $id_asset =1;
        $asset = $ModAsset->where('id',$id_asset)->first();
        $anggota = $model->where('id',$id_anggota)->first();
        $total = $anggota['simpanan_wajib'] + $anggota['simpanan_sukarela'] + $anggota['simpanan_pokok'];
        
        if($total > 0)
        {

            $data['id'] = $id_anggota;
            $data['simpanan_wajib'] = 0;
            $data['simpanan_sukarela'] = 0;
            $data['simpanan_pokok'] = 0;
        
            if($model->save($data)){

                $add['id'] = $id_asset;
                $add['total'] = $asset['total'] + $total;
                
                $tr['kode_transaksi'] = 'AK'.rand();
                $tr['id_transaksi'] = 5;
                $tr['keterangan'] = "Reset saldo anggota koperasi ({$anggota['nama']})";
                $tr['total'] = $total;
                $tr['status_transaksi'] = 2;
                $tr['id_user'] = user()->id;
                
                $modelTr->save($tr);
                return $this->respond(jsonRes('success'));
            }
        }
    }

    public function prosesExcel(){
        $model = model('AnggotaModels');
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
                $reader->setReadDataOnly(true);

                $file = ROOTPATH . 'public/excel/master_anggota_koperasi.xlsx';
                
                $spreadsheet = $reader->load($file);
                
                $worksheet = $spreadsheet->getActiveSheet();
                $rows = $worksheet->toArray();
                
                unset($rows[0]);
                
                
                foreach ($rows as $key => $value) {

                    $nama = $value[0];
                    $pangkat = $value[1];
                    $nrp = $value[2];
                    $jabatan = $value[3];
                    $email = $value[4];
                    $alamat = $value[5];
                    
                    $model->insert([
                        'nama'      =>$nama,
                        'pangkat'   =>$pangkat,
                        'kode_anggota'=> 'AGT'.rand(),
                        'nrp'       =>$nrp,
                        'password'  =>password_hash($nrp, PASSWORD_BCRYPT),
                        'jabatan'   => $jabatan,
                        'email'     => $email,
                        'alamat'    => $alamat
                    ]);     
                    
                }

                return $this->respond(jsonRes('success'));
                
    }

    
}