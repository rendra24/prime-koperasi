<?php

namespace App\Controllers\Master;

use App\Controllers\BaseController;

class Asset extends BaseController
{
	public function index(){
        $data['menu'] = 'master';
        $data['submenu'] = 'asset';
        return view('asset/data', $data);
    }

	public function data()
    {
        $model = model('MasterAsset');
        $data = $model
            ->orderBy('created_at', 'desc')
            ->findAll();

        $gData['table']['title'] = 'No|Nama Asset|Total|Status|Aksi';
        $gData['table']['data'] = 'no|nama_asset|total|flag|action';
        $gData['data'] = $data;
        $gData['no'] = true;
        $gData['action'] = 'edit';
        $gData['url']['edit'] = 'master/asset/form';
        $gData['helper']['flag'] = ['formatStatus', 'anggota|{data}'];
        $gData['helper']['total'] = ['formatDecimals', '{data}'];
        $data = jsonGenrateTables('success', $gData);
        return $this->respond($data);
    }

	public function form($id = null)
    {  
        $data = [];
        if($id != null){
            $model = model('MasterAsset');
            $data = $model->where('id',$id)->first();
        }
        return view('asset/_form',$data);
    }

    public function save(){
        $model = model('MasterAsset');
        $validation =  \Config\Services::validation();
        $post = $this->request->getPost();

        if (!$validation->run($post, 'asset')) {
            return $this->respond(jsonRes('validation_error', $validation->getErrors()));
        } else {
			if($post['id_jenis_asset'] == '1'){
				$post['nama_jenis_asset'] = 'Cash';
			}else{
				$post['nama_jenis_asset'] = 'Bank';
			}
            $model->save($post);
            return $this->respond(jsonRes('success'));
        }
        
    }
}