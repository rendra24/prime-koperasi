<?php

namespace App\Controllers\Master;

use App\Controllers\BaseController;

class Pinjaman extends BaseController
{
	public function index(){
        $data['menu'] = 'master';
        $data['submenu'] = 'pinjaman';
        return view('pinjaman/data', $data);
    }

	public function data()
    {
        $model = model('Masterpinjaman');
        $data = $model
            ->orderBy('created_at', 'desc')
            ->findAll();

        $gData['table']['title'] = 'No|Nama Pinjaman|Bunga|Status|Aksi';
        $gData['table']['data'] = 'no|nama_pinjaman|bunga|flag|action';
        $gData['data'] = $data;
        $gData['no'] = true;
        $gData['action'] = 'edit|delete';
        $gData['url']['edit'] = 'master/pinjaman/form';
        $gData['url']['delete'] = 'angsuran/admin_save';
        $gData['helper']['flag'] = ['formatStatus', 'anggota|{data}'];
        $data = jsonGenrateTables('success', $gData);
        return $this->respond($data);
    }

	public function form($id = null)
    {  
        $data = [];
        if($id != null){
            $model = model('Masterpinjaman');
            $data = $model->where('id',$id)->first();
        }
        return view('pinjaman/_form',$data);
    }

    public function save(){
        $model = model('Masterpinjaman');
        $validation =  \Config\Services::validation();
        $post = $this->request->getPost();

      

        if (!$validation->run($post, 'pinjaman')) {
            return $this->respond(jsonRes('validation_error', $validation->getErrors()));
        } else {
            $model->save($post);
            return $this->respond(jsonRes('success'));
        }
        
    }

   
}