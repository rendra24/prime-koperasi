<?php

namespace App\Controllers\Master;

use App\Controllers\BaseController;

class Simpanan extends BaseController
{
	public function index(){
        $data['menu'] = 'master';
        $data['submenu'] = 'simpanan';
        return view('simpanan/data', $data);
    }

	public function data()
    {
        $model = model('Mastersimpanan');
        $data = $model
            ->orderBy('created_at', 'desc')
            ->findAll();

        $gData['table']['title'] = 'No|Nama|Besar Simpanan|Status|Aksi';
        $gData['table']['data'] = 'no|nama_simpanan|total|flag|action';
        $gData['data'] = $data;
        $gData['no'] = true;
        $gData['action'] = 'edit';
        $gData['url']['edit'] = 'master/simpanan/form';
        // $gData['url']['delete'] = 'simpanan/admin_save';
        $gData['helper']['flag'] = ['formatStatus', 'anggota|{data}'];
        $gData['helper']['total'] = ['formatDecimals','{data}'];
        $data = jsonGenrateTables('success', $gData);
        return $this->respond($data);
    }

	public function form($id = null)
    {  
        $data = [];
        if($id != null){
            $model = model('Mastersimpanan');
            $data = $model->where('id',$id)->first();
        }
        return view('simpanan/_form',$data);
    }

    public function save(){
        $model = model('Mastersimpanan');
        
        $validation =  \Config\Services::validation();
        $post = $this->request->getPost();

        if (!$validation->run($post, 'simpanan')) {
            return $this->respond(jsonRes('validation_error', $validation->getErrors()));
        } else {
            
            $model->save($post);
            return $this->respond(jsonRes('success'));
        }
        
    }
}