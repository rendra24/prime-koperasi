<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use CodeIgniter\HTTP\ResponseInterface;

class Transaksi extends BaseController
{
    public function index()
    {
        if($this->request->getVar()){
			$post = $this->request->getVar();
			$data['request'] = $post;
		}
		
		$data['menu'] = 'transaksi';
        $data['submenu'] = 'ppob';
        return view('transaksi/data_transaksi', $data);
    }

    public function data_transaksi()
	{
		$model = model('Transaksianggota');
		$post = $this->request->getVar();
		$data = $model->TransaksiAnggota($post)->getResultArray();
        
		
		$gData['table']['title'] = 'No|Nama Anggota|Nominal|Keterangan|Tanggal';
        $gData['table']['data'] = 'no|nama_anggota|nominal|keterangan|created_at';
        $gData['data'] = $data;
        $gData['no'] = true;
		$gData['helper']['nominal'] = ['formatDecimals', '{data}'];
        $gData['helper']['created_at'] = ['formatTglIndo', '{data}'];
        $data = jsonGenrateTables('success', $gData);
        return $this->respond($data);
	}

    public function form()
    {  
        return view('transaksi/form_transaksi');
    }

    public function save()
    {
        if($post = $this->request->getPost())
        {
            $modTran = model('TransaksiModel');
            $ModAnggota = model('AnggotaModels');
            $ModAsset = model('MasterAsset');
            $modTranAnggota = model('Transaksianggota');

            $db = \Config\Database::connect();

            $db->transStart();

            $anggota_id = $post['id_anggota'];
             //input transaksi
             $dataTr['kode_transaksi'] = 'TRA'.rand();
             $dataTr['id_anggota'] = $post['id_anggota'];
             $dataTr['id_asset'] = 1;
             $dataTr['total'] = $post['nominal'];
             $dataTr['keterangan'] = $post['keterangan'];
             $dataTr['id_transaksi'] = 8;
             $dataTr['status_transaksi'] = 2;
             $dataTr['id_user'] = user()->id;
             $modTran->save($dataTr);
     
             $last_id_transaksi = $modTran->getInsertID();
            // input transaksi anggota
            $data['id_transaksi'] = $last_id_transaksi;
            $data['id_anggota'] = $anggota_id;
            $data['nominal'] = $post['nominal'];
            $data['keterangan'] = $post['keterangan'];
            $data['id_user'] = user()->id;

            $modTranAnggota->save($data);

            $get = $modTran->getSaldoUser($anggota_id)->getRowArray();

            //update saldo user
            $dataSaldo['simpanan_sukarela'] = $get['simpanan_sukarela'] - $post['nominal'];
            $dataSaldo['id'] = $anggota_id;
            $ModAnggota->save($dataSaldo);

            //update asset
            $asset = $ModAsset->where('id', 1)->first();
            $add['total'] = $asset['total'] - $post['nominal'];
            $add['id'] = 1;
            $ModAsset->save($add);

            $db->transComplete();

            if ($db->transStatus() === false) {
                return $this->getResponse(
                    [
                        'message' => 'Membuat transaksi gagal coba kembali',
                    ],
                    ResponseInterface::HTTP_BAD_REQUEST);
            }

            return $this->getResponse(
                [
                    'message' => 'Transasi berhasil diproses',
                    'status' => true,
                ]
            );


        }
    }
}