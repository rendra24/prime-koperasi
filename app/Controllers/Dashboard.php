<?php

namespace App\Controllers;

use \DateTime;
use \DateInterval;

class Dashboard extends BaseController
{
	public function index()
	{

		$model = model('TransaksiModel');
		$simpanan = model('SimpananModel');
		$pinjaman = model('PinjamanModel');

		$last_transaksi = $model->getLastTransaksi()->getResultArray();
		$total_kas = $model->getTotalKas()->getRowArray();
		$total_simpanan = $simpanan->getSimpananAll()->getRowArray();
		$total_pinjaman = $pinjaman->getPinjamanAll([1,2])->getRowArray();
		$top_anggota = $simpanan->getTopAnggota()->getResultArray();
		
		$data['menu'] = 'dashboard';
		$data['transaksi'] = $last_transaksi;
		$data['total_kas'] = $total_kas['kas_masuk'] - $total_kas['kas_keluar'];
		$data['total_simpanan'] = $total_simpanan['total'];
		$data['total_pinjaman'] = $total_pinjaman['total'];
		$data['top_anggota'] = $top_anggota;
		return view('dashboard/index', $data);
	}

	function getCutoffDate($date) {
		$days = cal_days_in_month(CAL_GREGORIAN, $date->format('n'), $date->format('Y'));
		$date->add(new \DateInterval('P' . $days .'D'));
		return $date;
	}
	//--------------------------------------------------------------------


	public function flash_pendapatan()
	{
		$model = model('TransaksiModel');
		$get_data = [];
		$thn_berjalan = (int) date('Y');

		$model->select('month(created_at) as bulan, sum(total) as total');
		$model->groupBy('month(created_at)');
		$model->where('year(created_at)', $thn_berjalan);
		$model->where('status_transaksi','1');
		$get_data = $model->findAll();
		$get_data = formatChartCoreui($get_data);

		return $this->respond($get_data);
	}

	public function hitung_laporan()
	{
		$modelLPP = model('LaporanPendaftaranPelangganModel');
		$modelLPenjualan = model('LaporanPenjualanModel');
		$modelTrans	= model('TransaksiModel');
		$modelPelanggan	= model('PelangganModel');

		$modelPelanggan->select('month(created_at) as bulan, year(created_at) as tahun, count(id) as total');
		$modelPelanggan->groupBy('month(created_at), year(created_at)');
		$get_total_pendaftaran = $modelPelanggan->findAll();

		$modelTrans->select('date(tgl_transaksi) as tanggal, id_layanan, count(id) as total_transaksi, sum(total_harga) as nominal_total_transaksi');
		$modelTrans->groupBy('date(tgl_transaksi), id_layanan');
		$get_laporan_penjualan = $modelTrans->findAll();

		$modelLPP->truncate();
		foreach ($get_total_pendaftaran as $tp) {
			$modelLPP->save($tp);
		}

		$modelLPenjualan->truncate();
		foreach ($get_laporan_penjualan as $lp) {
			$modelLPenjualan->save($lp);
		}
	}
	public function test_chat()
	{
		$user_id = $this->request->getPost('user_id');
		$user_nama = $this->request->getPost('user_nama');
		$room_id = $this->request->getPost('room_id');
		$isi = $this->request->getPost('isi');

		$modelObrolan = model('ObrolanModel');

		$dataO['room_id'] = $room_id;
		$dataO['role'] = 1;
		$dataO['user_id'] = $user_id;
		$dataO['user_nama'] = $user_nama;
		$dataO['isi'] = $isi;
		$dataO['read'] = 0;

		$modelObrolan->save($dataO);
	}
}