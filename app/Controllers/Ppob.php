<?php

namespace App\Controllers;

use App\Controllers\BaseController;

class Ppob extends BaseController
{
	public function refund($id)
	{
        $db = \Config\Database::connect();
        $model = model('Ppob/TransaksiModel');
        $modAnggota = model('AnggotaModels');
        $modTran = model('TransaksiModel');
        $modAsset = model('MasterAsset');


        $db->transStart();

        $get = $model->where('id', $id)->where('status', '2')->first();
        $anggota = $modAnggota->where('id', $get['anggota_id'])->first();
        
        $jumlah_refund = $get['price_sell'];
        $sisa_saldo = $anggota['simpanan_sukarela'];
        
        $data['simpanan_sukarela'] = $jumlah_refund + $sisa_saldo;
        $data['id'] = $get['anggota_id'];
        $modAnggota->save($data);

        //Tambah Asset Saldo PPOB
        $asset = $modAsset->where('id', 4)->first();
        $dataAsset['total'] = $asset['total'] + $get['price'];
        $dataAsset['id'] = 4;
        $modAsset->save($dataAsset);

        //Tambah Refund pada transaksi untuk log 
        $dataTr['kode_transaksi'] = 'RPPOB'.rand();
        $dataTr['id_anggota'] = $get['anggota_id'];
        $dataTr['id_asset'] = 4;
        $dataTr['total'] = $get['price'];
        $dataTr['keterangan'] = 'Pengembalian Data PPOB sejumlah ' . $get['price'] .' atas nama ' . $anggota['nama'];
        $dataTr['id_transaksi'] = 7;
        $dataTr['status_transaksi'] = 1;
        $modTran->save($dataTr);

        //Ubah Status PPOB Menadi Refund
        $ppob['status'] = 3;
        $ppob['message'] = 'Refund Success';
        $ppob['id'] = $id;
        $model->save($ppob);

        $db->transComplete();

        if ($db->transStatus() === false) {
            return $this->getResponse(
                [
                    'message' => 'Order galgal',
                    'status' => false,
                ]
            );
            return false;
        }
        
        return $this->respond(jsonRes('success'));
        
    }
}