<?php

namespace App\Controllers;

use App\Controllers\BaseController;

class Laporan extends BaseController
{
	public function index()
	{
		$model = model('Ppob/TransaksiModel');
		$get_ppob = $model->findAll();
		
		foreach($get_ppob as $row)
		{
			$data['id'] = $row['id'];
			$data['price_sell'] = $row['price'] + (0.015 * $row['price']);
			$data['keuntungan'] = 0.015 * $row['price'];

			$model->save($data);
		}
	}

	public function anggota()
	{
		$data['menu'] = 'laporan';
        $data['submenu'] = 'anggota';
        return view('laporan/data_anggota', $data);
	}

	public function transaksi()
	{
		if($this->request->getVar()){
			$post = $this->request->getVar();
			$data['request'] = $post;
		}
		
		$data['menu'] = 'laporan';
        $data['submenu'] = 'transaksi';
        return view('laporan/data_transaksi', $data);
	}

	public function ppob()
	{
		if($this->request->getVar()){
			$post = $this->request->getVar();
			$data['request'] = $post;
		}
		
		$data['menu'] = 'laporan';
        $data['submenu'] = 'ppob';
        return view('laporan/data_ppob', $data);
	}

	public function data_anggota(){
		$model = model('LaporanModel');
		$data = $model->LaporanAnggota()->getResultArray();

        $gData['table']['title'] = 'No|Nama|Pangkat|Jabatan|Tanggal Daftar|Simpanan Wajib|Simpanan Pokok|Simpanan Sukarela';
        $gData['table']['data'] = 'no|nama|pangkat|jabatan|created_at|simpanan_wajib|simpanan_pokok|simpanan_sukarela';
        $gData['data'] = $data;
        $gData['no'] = true;
        $gData['helper']['created_at'] = ['formatTglIndo', '{data}'];
		$gData['helper']['simpanan_wajib'] = ['formatDecimals', '{data}'];
		$gData['helper']['simpanan_pokok'] = ['formatDecimals', '{data}'];
		$gData['helper']['simpanan_sukarela'] = ['formatDecimals', '{data}'];
        $data = jsonGenrateTables('success', $gData);
        return $this->respond($data);
	}

	public function data_transaksi(){
		$model = model('LaporanModel');
		$post = $this->request->getVar();
		$data = $model->LaporanTransaksi($post)->getResultArray();

		$gData['table']['title'] = 'No|Nama Anggota|Tanggal Transaksi|Total|Jenis Transaksi|Status';
        $gData['table']['data'] = 'no|nama|created_at|total|nama_transaksi|status_transaksi';
        $gData['data'] = $data;
        $gData['no'] = true;
        $gData['helper']['created_at'] = ['formatTglIndo', '{data}'];
		$gData['helper']['total'] = ['formatDecimals', '{data}'];
		$gData['helper']['status_transaksi'] = ['formatStatus', 'transaksi|{data}'];
        $data = jsonGenrateTables('success', $gData);
        return $this->respond($data);
	}

	public function data_ppob()
	{
		$model = model('Ppob/TransaksiModel');
		$post = $this->request->getVar();
		$data = $model->LaporanPpob($post)->getResultArray();
		
		$gData['table']['title'] = 'No|Nama Anggota|Produk|Jual|Beli|Keuntungan|Tanggal|Status|Action';
        $gData['table']['data'] = 'no|nama|product_code|price_sell|price|keuntungan|created_at|status|action';
        $gData['data'] = $data;
        $gData['no'] = true;
		$gData['action'] = 'refund';
		$gData['url']['refund'] = 'ppob/refund';
        $gData['helper']['created_at'] = ['formatTglIndo', '{data}'];
		$gData['helper']['price'] = ['formatDecimals', '{data}'];
		$gData['helper']['price_sell'] = ['formatDecimals', '{data}'];
		$gData['helper']['keuntungan'] = ['formatDecimals', '{data}'];
		$gData['helper']['status'] = ['formatStatus', 'ppob|{data}'];
        $data = jsonGenrateTables('success', $gData);
        return $this->respond($data);
	}
}