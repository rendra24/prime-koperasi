<?php

namespace App\Controllers\Import;

use App\Controllers\BaseController;

class Simpanan extends BaseController
{

    public function delete_excel($id)
    {
        $model = model('Simpananexcel');
        $model->where('id', $id)->delete();
        return $this->respond('success');
    }

    public function reset_excel()
    {
        $model = model('Simpananexcel');
        if ($model->truncate()) {
            return $this->respond(jsonRes('success'));
        }
    }

    public function move()
    {
        $db = \Config\Database::connect();
        $db->transStart();

        $model = model('SimpananModel');
        $transaksi = model('TransaksiModel');
        $anggota = model('AnggotaModels');
        $ModAsset = model('MasterAsset');
        $modelSimpanan = model('Simpananexcel');
        $data = $modelSimpanan->getSimpanan()->getResultArray();

        $total = 0;
        foreach ($data as $row) {

            if ($row['pokok'] != 0) {
                $post['id_simpanan'] = $id_simpanan = 3;
                $post['total_simpanan'] = $total_simpanan = $row['pokok'];
                $kode_transaksi = 'TR' . rand();
                $post['kode_transaksi'] = $kode_transaksi;
                $post['id_anggota'] = $row['id_anggota'];
                $post['nama_anggota'] = $row['nama'];
                $total += $row['pokok'];
                $model->save($post);

                $add['kode_transaksi'] = $kode_transaksi;
                $add['status_transaksi'] = 1;
                $add['id_transaksi'] = 1;
                $add['total'] = $post['total_simpanan'];
                $add['id_anggota'] = $row['id_anggota'];
                $add['id_user'] = user()->id;
                $add['created_at'] = date('Y-m-d H:i:s');
                $add['updated_at'] = date('Y-m-d H:i:s');
                $transaksi->saveTransaksi($add);

                $anggota->updaetSaldo($row['id_anggota'], $total_simpanan, $id_simpanan);
            }

            if ($row['wajib'] != 0) {
                $post['id_simpanan'] = $id_simpanan = 1;
                $post['total_simpanan'] = $total_simpanan = $row['wajib'];
                $kode_transaksi = 'TR' . rand();
                $post['kode_transaksi'] = $kode_transaksi;
                $post['id_anggota'] = $row['id_anggota'];
                $post['nama_anggota'] = $row['nama'];
                $total += $row['wajib'];
                $model->save($post);

                $add['kode_transaksi'] = $kode_transaksi;
                $add['status_transaksi'] = 1;
                $add['id_transaksi'] = 1;
                $add['total'] = $post['total_simpanan'];
                $add['id_anggota'] = $row['id_anggota'];
                $add['id_user'] = user()->id;
                $add['created_at'] = date('Y-m-d H:i:s');
                $add['updated_at'] = date('Y-m-d H:i:s');
                $transaksi->saveTransaksi($add);

                $anggota->updaetSaldo($row['id_anggota'], $total_simpanan, $id_simpanan);
            }

            if ($row['sukarela'] != 0) {
                $post['id_simpanan'] = $id_simpanan = 2;
                $post['total_simpanan'] = $total_simpanan = $row['sukarela'];
                $kode_transaksi = 'TR' . rand();
                $post['kode_transaksi'] = $kode_transaksi;
                $post['id_anggota'] = $row['id_anggota'];
                $post['nama_anggota'] = $row['nama'];
                $total += $row['sukarela'];
                $model->save($post);

                $add['kode_transaksi'] = $kode_transaksi;
                $add['status_transaksi'] = 1;
                $add['id_transaksi'] = 1;
                $add['total'] = $post['total_simpanan'];
                $add['id_anggota'] = $row['id_anggota'];
                $add['id_user'] = user()->id;
                $add['created_at'] = date('Y-m-d H:i:s');
                $add['updated_at'] = date('Y-m-d H:i:s');
                $transaksi->saveTransaksi($add);

                $anggota->updaetSaldo($row['id_anggota'], $total_simpanan, $id_simpanan);
            }

            if ($row['tabungan'] != 0) {
                $post['id_simpanan'] = $id_simpanan = 4;
                $post['total_simpanan'] = $total_simpanan = $row['tabungan'];
                $kode_transaksi = 'TR' . rand();
                $post['kode_transaksi'] = $kode_transaksi;
                $post['id_anggota'] = $row['id_anggota'];
                $post['nama_anggota'] = $row['nama'];
                $total += $row['tabungan'];
                $model->save($post);

                $add['kode_transaksi'] = $kode_transaksi;
                $add['status_transaksi'] = 1;
                $add['id_transaksi'] = 1;
                $add['total'] = $post['total_simpanan'];
                $add['id_anggota'] = $row['id_anggota'];
                $add['id_user'] = user()->id;
                $add['created_at'] = date('Y-m-d H:i:s');
                $add['updated_at'] = date('Y-m-d H:i:s');
                $transaksi->saveTransaksi($add);

                $anggota->updaetSaldo($row['id_anggota'], $total_simpanan, $id_simpanan);
            }
        }

        $modelSimpanan->truncate();

        $id_asset = 1;
        $asset = $ModAsset->where('id', 1)->first();

        $upp['total'] = $asset['total'] + $total;
        $upp['id'] = $id_asset;
        $ModAsset->save($upp);

        $db->transComplete();

        if ($db->transStatus() === false) {
            return $this->respond(jsonRes('error'));
            return false;
        }

        return $this->respond(jsonRes('success'));
    }

    public function prosesExcel()
    {

        $model = model('Simpananexcel');

        $dataBerkas = $this->request->getFile('file');
        $fileName = 'Soal_' . date('YmdHis') . '.xlsx';
        $dataBerkas->move('uploads/excel/', $fileName);


        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        $reader->setReadDataOnly(true);

        $file = 'uploads/excel/' . $fileName;

        $spreadsheet = $reader->load($file);

        $worksheet = $spreadsheet->getActiveSheet();
        $rows = $worksheet->toArray();

        unset($rows[0]);

        foreach ($rows as $key => $value) {
            if ($value[1] != null && $value[1] !== '') {
                $model->insert([
                    'nrp'                  => $value[1],
                    'pokok'                => str_replace(".", "", $value[3]),
                    'wajib'                => str_replace(".", "", $value[2]),
                    'sukarela'             => str_replace(".", "", $value[4]),
                    'tabungan'             => str_replace(".", "", $value[5]),
                ]);
            } else {
            }
        }

        return redirect()->to('/simpanan/form_excel');
    }
}
