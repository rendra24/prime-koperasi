<?php

namespace App\Controllers;

use Myth\Auth\Entities\User;

class Users extends BaseController
{
    private $loadModel = [];

    public function __construct()
    {
        $this->loadModel['users'] = model('UsersModel');
        $this->loadModel['vendor'] = model('VendorModel');
        $this->loadModel['pelanggan'] = model('PelangganModel');
        $this->config = config('Myth\Auth\Config\Auth');
    }

    public function index()
    {
        return redirect()->to(base_url('users/admin'));
    }

    /**
     * # ------------------------------------------------------------------ 
     * * MANAJEMEN PENGGUNA SUB HAK AKSES 
     *
     * Description
     * -
     */
    public function permission()
    {
        $data['menu'] = 'users';
        $data['submenu'] = 'permission';
        return view('users/permission', $data);
    }
    public function permission_data()
    {
        $model = model('AuthGroupModel');
        $data = $model
            ->orderBy('id', 'desc')
            ->findAll();

        $gData['table']['title'] = 'No|Nama|Keterangan|Aksi';
        $gData['table']['data'] = 'no|name|description|action';
        $gData['data'] = $data;
        $gData['no'] = true;
        $gData['action'] = 'detail|edit|delete';
        $gData['url']['detail'] = 'users/permision_detail';
        $gData['url']['edit'] = 'users/permission_form';
        $gData['url']['delete'] = 'users/permission_save';
        $data = jsonGenrateTables('success', $gData);
        return $this->respond($data);
    }
    public function permission_form($id = null)
    {
        $model = model('AuthGroupModel');
        $modelPermission = model('AuthPermissionModel');
        $data = [];
        $data['group_perm'] = [];
        if ($id != null) {
            $data['db'] = $model->where('id', $id)->first();
            $gp = $model->getGroupPermissions($id);
            foreach ($gp as $igp) {
                array_push($data['group_perm'], $igp['permission_id']);
            }
        }
        $data['permission'] = $modelPermission->findAll();
        return view('users/_permission_form', $data);
    }
    public function permission_save()
    {
        $model = model('AuthGroupModel');
        $validation =  \Config\Services::validation();
        $post = $this->request->getPost();

        if (!$validation->run($post, 'permission')) {
            return $this->respond(jsonRes('validation_error', $validation->getErrors()));
        } else {
            $model->save($post);
            if (@$post['id']) {
                $group_id = $post['id'];
            } else {
                $group_id = $model->insertID();
            }

            $model->cleanGroupPermission($group_id);

            foreach ($post['permissions'] as $item) {
                $data['group_id'] = $group_id;
                $data['permission_id'] = $item;
                $model->saveGroupPermissions($data);
            }
            return $this->respond(jsonRes('success'));
        }
    }
    public function permission_delete($id)
    {
        $model = model('AuthGroupModel');
        $model->cleanGroupPermission($id);
        $model->delete($id);
        return $this->respond(jsonRes('success'));
    }

    /**
     * # ------------------------------------------------------------------ 
     * * MANAJEMEN PENGGUNA SUB ADMIN
     *
     * Description
     * -
     */
    public function admin()
    {
        $data['menu'] = 'users';
        $data['submenu'] = 'admin';
        return view('users/admin', $data);
    }
    public function admin_data()
    {
        $model = $this->loadModel['users'];
        $data = $model
            ->orderBy('created_at', 'desc')
            ->findAll();

        $gData['table']['title'] = 'No|Nama|Email|Telepon|Aksi';
        $gData['table']['data'] = 'no|name|email|phone|action';
        $gData['data'] = $data;
        $gData['no'] = true;
        $gData['action'] = 'edit|delete';
        $gData['url']['edit'] = 'users/admin_form';
        $gData['url']['delete'] = 'users/admin_save';
        $data = jsonGenrateTables('success', $gData);
        return $this->respond($data);
    }
    public function admin_form($id = null)
    {
        $model = $this->loadModel['users'];
        $modelAuthGroup = model('AuthGroupModel');

        $modelGroup =
            $data = [];
        if ($id != null) {
            $data = $model
                ->join('auth_groups_users b', 'users.id = b.user_id', 'left')
                ->where('id', $id)
                ->first();
        }
        $data['hak_akses'] = $modelAuthGroup->findAll();
        return view('users/_admin_form', $data);
    }
    public function admin_save()
    {
        $model = $this->loadModel['users'];
        $validation =  \Config\Services::validation();
        $users = model('Myth\Auth\Models\UserModel');
        $post = $this->request->getPost();

        if (!$validation->run($post, 'admin')) {
            return $this->respond(jsonRes('validation_error', $validation->getErrors()));
        } else {
            if (@!$post['id']) {
                // Save the user
                $allowedPostFields = array_merge(['password'], $this->config->validFields, $this->config->personalFields);
                $user = new User($this->request->getPost($allowedPostFields));
                $user->activate();

                $users->save($user);
                $post['insertID'] = $users->insertID();
            } else {
                $model->save($post);
            }
            $model->saveGroup($post);

            return $this->respond(jsonRes('success'));
        }
    }
    public function admin_delete($id)
    {
        $model = $this->loadModel['users'];
        $model->delete($id);
        return $this->respond(jsonRes('success'));
    }


    /**
     * # ------------------------------------------------------------------ 
     * * MANAJEMEN PENGGUNA SUB VENDOR
     *
     * Description
     * -
     */
    public function vendor()
    {
        $data['menu'] = 'users';
        $data['submenu'] = 'vendor';
        return view('users/vendor', $data);
    }
    public function vendor_data()
    {
        $model = $this->loadModel['vendor'];
        $data = $model
            ->orderBy('created_at', 'desc')
            ->findAll();

        $gData['table']['title'] = 'No|Nama Perusahaan|Telp Perusahaan|Alamat Perusahaan|Nama Pemilik|Email Pemilik|Aksi';
        $gData['table']['data'] = 'no|nama_usaha|telp_usaha|alamat_usaha|nama_pemilik|email_pemilik|action';
        $gData['data'] = $data;
        $gData['no'] = true;
        $gData['action'] = 'edit|detail';
        $gData['url']['edit'] = 'users/vendor_form';
        $gData['url']['detail'] = 'users/vendor_detail';
        $data = jsonGenrateTables('success', $gData);
        return $this->respond($data);
    }
    public function vendor_form($id = null)
    {
        $model = $this->loadModel['vendor'];
        $data = [];
        if ($id != null) {
            $data['db'] = $model->where('id', $id)->first();
        }
        $data['bank'] = json_decode(file_get_contents('data/data_bank.json'), true);
        return view('users/_vendor_form', $data);
    }
    public function vendor_detail($id = null)
    {
        $model = $this->loadModel['vendor'];
        $data = [];
        if ($id != null) {
            $data['db'] = $model->where('id', $id)->first();
        }
        $data['bank'] = json_decode(file_get_contents('data/data_bank.json'), true);
        return view('users/_vendor_detail', $data);
    }
    public function vendor_save()
    {
        $model = $this->loadModel['vendor'];
        $validation =  \Config\Services::validation();
        $post = $this->request->getPost();

        if (!$validation->run($post, 'vendor')) {
            return $this->respond(jsonRes('validation_error', $validation->getErrors()));
        } else {
            $model->save($post);
            return $this->respond(jsonRes('success'));
        }
    }
    public function vendor_delete($id)
    {
        $model = $this->loadModel['vendor'];
        $model->delete($id);
        return $this->respond(jsonRes('success'));
    }


    /**
     * # ------------------------------------------------------------------ 
     * * MANAJEMEN PENGGUNA SUB CUSTOMMER
     *
     * Description
     * -
     */
    public function customer()
    {
        $data['menu'] = 'users';
        $data['submenu'] = 'customer';
        return view('users/customer', $data);
    }
    public function customer_data()
    {
        $model = $this->loadModel['pelanggan'];
        $data = $model->findAllPelanggan();

        $gData['table']['title'] = 'No|Nama|Email|Telp|Alamat|Tanggal Daftar|Status|Aksi';
        $gData['table']['data'] = 'no|nama|email|telp|alamat|created_at|flag|action';
        $gData['data'] = $data;
        $gData['no'] = true;
        $gData['action'] = 'detail';
        $gData['url']['detail'] = 'users/customer_detail';
        $gData['helper']['flag'] = ['formatFlag', 'pelanggan|{data}'];
        $gData['helper']['created_at'] = ['formatTglIndo', '{data}'];
        $data = jsonGenrateTables('success', $gData);
        return $this->respond($data);
    }
    public function customer_detail($id = null)
    {
        $model = $this->loadModel['pelanggan'];
        $data = $model->firstPelanggan($id);
        return view('users/_customer_detail', $data);
    }
    public function customer_save()
    {
        $model = $this->loadModel['pelanggan'];
        $validation =  \Config\Services::validation();
        $post = $this->request->getPost();

        if (!$validation->run($post, 'vendor')) {
            return $this->respond(jsonRes('validation_error', $validation->getErrors()));
        } else {
            $model->save($post);
            return $this->respond(jsonRes('success'));
        }
    }
}
