<?php

namespace App\Controllers\Api\Anggota;

use CodeIgniter\RESTful\ResourceController;
use CodeIgniter\API\ResponseTrait;
use App\Controllers\BaseController;


class Pinjaman extends BaseController
{
    use ResponseTrait;

    public function index($id_anggota)
    {
        $model = model('PinjamanModel');
		$data = $model->where('id_anggota',$id_anggota)->findAll();
        
        $jData= array();
        foreach($data as $row){
            $jData[] = array(
                'id' => $row['id'],
                'total_pinjaman' => formatRupiah($row['total_pinjaman']),
                'angsuran' => $row['angsuran'],
                'cicilan' => formatRupiah($row['cicilan']),
                'status_pinjaman' => $row['status_pinjaman']
            );
        }

        return $this->getResponse(
            [
                'message' => 'Data Riwayat Pinjaman successfully',
                'data' => $jData,
            ]
        );
    }

    public function riwayat($id)
    {
        $model = model('PinjamanModel');
        $data = $model->where('id',$id)->findAll();
		$detail = $model->get_detail($id)->getResultArray();

        $jData= array();
        foreach($detail as $row){
            $jData[] = array(
                'cicilan' => formatRupiah($row['cicilan']),
                'angsuran' => $row['angsuran'],
                'status_pembayaran' => $row['status_pembayaran']
            );
        }

        return $this->getResponse(
            [
                'message' => 'Data Riwayat Detail Pinjaman successfully',
                'data' => $jData,
            ]
        );
        
    }
}