<?php

namespace App\Controllers\Api\Anggota;

use CodeIgniter\RESTful\ResourceController;
use CodeIgniter\API\ResponseTrait;
use App\Controllers\BaseController;


class Transaksi extends BaseController
{
    use ResponseTrait;


    public function saldo()
    {
        $model = model('TransaksiModel');
        $post = $this->request->getPost();
        $get = $model->getSaldoUser($post['id_user'])->getRowArray();

        return $this->getResponse(
            [
                'message' => 'Saldo retrieved successfully',
                'saldo_pokok' => formatDecimals($get['simpanan_pokok']),
                'saldo_wajib' => formatDecimals($get['simpanan_wajib']),
                'saldo_sukarela' => formatDecimals($get['simpanan_sukarela'])
            ]
        );
    }

    public function riwayat()
    {
        $model = model('SimpananModel');
        $post = $this->request->getPost();
        $data = $model->getApiSimpanan($post['id_user'])->getResultArray();
        $jData = array();
        foreach ($data as $row) {
            $jData[] = array(
                'total_simpanan' => formatDecimals($row['total_simpanan']),
                'created_at' => date('d-m-Y H:i', strtotime($row['created_at'])),
                'nama_simpanan' => $row['nama_simpanan']
            );
        }
        return $this->getResponse(
            [
                'message' => 'Data Riwayat retrieved successfully',
                'data' => $jData,
            ]
        );
    }

    public function riwayat_ppob()
    {
        $model = model('Ppob/TransaksiModel');


        $post = $this->request->getPost();
        $data = $model->where('anggota_id', $post['id_user'])->findAll();

        $jData = array();
        foreach ($data as $row) {
            $jData[] = array(
                'id' => $row['id'],
                'ref_id' => $row['ref_id'],
                'nama_produk' => $row['product_code'],
                'harga' => formatDecimals(round($row['price'] + (0.015 * $row['price']))),
                'status' => $row['status'],
                'message' => $row['message']
            );
        }



        return $this->getResponse(
            [
                'message' => 'Data Riwayat purchase successfully',
                'data' => $jData,
            ]
        );
    }

    public function riwayat_pembelian()
    {
        $transaksiAnggota = model('TransaksiAnggota');
        $post = $this->request->getPost();
        $dataTransaksi =  $transaksiAnggota->where('id_anggota', $post['id_user'])->findAll();

        $jData = array();
        foreach ($dataTransaksi as $row) {
            $jData[] = array(
                'id' => $row['id'],
                'nominal' => formatDecimals($row['nominal']),
                'keterangan' => $row['keterangan'],
                'created_at' => strtotime('d-m-Y', $row['created_at'])
            );
        }

        return $this->getResponse(
            [
                'message' => 'Data Riwayat Pembelian successfully',
                'data' => $jData,
            ]
        );
    }

    public function pinjaman()
    {
        $model = model('MasterPinjaman');
        $angsuran = model('MasterAngsuran');
        $data['pinjaman'] = $model->findAll();
        $data['angsuran'] = $angsuran->findAll();

        return $this->getResponse(
            [
                'message' => 'Data Pinjaman retrieved successfully',
                'data' => $data,
            ]
        );
    }

    public function save_pinjaman()
    {
        $model = model('PinjamanModel');
        $validation =  \Config\Services::validation();
        $post = $this->request->getVar();
        $check = $model->where(['id_anggota' => $post['id_anggota'], 'status_pinjaman' => 0])->find();
        $post['total_pinjaman'] = str_replace(',', '', $post['total']);

        if (!$validation->run($post, 'pinjaman_mobile')) {
            return $this->respond(jsonRes('validation_error', $validation->getErrors()));
        } else {

            $check = $model->where(['id_anggota' => $post['id_anggota'], 'status_pinjaman' => 0])->find();

            if ($check) {
                return $this->getResponse(
                    [
                        'status' => false,
                        'message' =>
                        'Anda sudah mengajukan pinjaman. Kontak admin untuk info lebih lanjut', 'data' => [],
                    ]
                );
                return false;
            }

            $total_pinjaman = str_replace(',', '', $post['total']);
            $total_pinjaman = str_replace('.', '', $total_pinjaman);
            $post['total_pinjaman'] = $total_pinjaman;
            $post['id_asset'] = 1;
            $model->save($post);
            return $this->respond(jsonRes('success'));
        }
    }
}
