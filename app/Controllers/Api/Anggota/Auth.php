<?php 

namespace App\Controllers\Api\Anggota;
 
use \Firebase\JWT\JWT;
use App\Models\AuthAnggotaModel;
use App\Controllers\BaseController;
use CodeIgniter\HTTP\Response;
use CodeIgniter\HTTP\ResponseInterface;
use Exception;
use ReflectionException;
 
class Auth extends BaseController
{
    public function __construct()
    {
        $this->auth = new AuthAnggotaModel();
    }

    public function index(){
        return "test";
    }
 
    public function privateKey()
    {
        $privateKey = <<<EOD
            -----BEGIN RSA PRIVATE KEY-----
            MIICXAIBAAKBgQC8kGa1pSjbSYZVebtTRBLxBz5H4i2p/llLCrEeQhta5kaQu/Rn
            vuER4W8oDH3+3iuIYW4VQAzyqFpwuzjkDI+17t5t0tyazyZ8JXw+KgXTxldMPEL9
            5+qVhgXvwtihXC1c5oGbRlEDvDF6Sa53rcFVsYJ4ehde/zUxo6UvS7UrBQIDAQAB
            AoGAb/MXV46XxCFRxNuB8LyAtmLDgi/xRnTAlMHjSACddwkyKem8//8eZtw9fzxz
            bWZ/1/doQOuHBGYZU8aDzzj59FZ78dyzNFoF91hbvZKkg+6wGyd/LrGVEB+Xre0J
            Nil0GReM2AHDNZUYRv+HYJPIOrB0CRczLQsgFJ8K6aAD6F0CQQDzbpjYdx10qgK1
            cP59UHiHjPZYC0loEsk7s+hUmT3QHerAQJMZWC11Qrn2N+ybwwNblDKv+s5qgMQ5
            5tNoQ9IfAkEAxkyffU6ythpg/H0Ixe1I2rd0GbF05biIzO/i77Det3n4YsJVlDck
            ZkcvY3SK2iRIL4c9yY6hlIhs+K9wXTtGWwJBAO9Dskl48mO7woPR9uD22jDpNSwe
            k90OMepTjzSvlhjbfuPN1IdhqvSJTDychRwn1kIJ7LQZgQ8fVz9OCFZ/6qMCQGOb
            qaGwHmUK6xzpUbbacnYrIM6nLSkXgOAwv7XXCojvY614ILTK3iXiLBOxPu5Eu13k
            eUz9sHyD6vkgZzjtxXECQAkp4Xerf5TGfQXGXhxIX52yH+N2LtujCdkQZjXAsGdm
            B2zNzvrlgRmgBrklMTrMYgm1NPcW+bRLGcwgW2PTvNM=
            -----END RSA PRIVATE KEY-----
            EOD;
        return $privateKey;
    }


    public function login()
    {

        $input = $this->getRequestInput($this->request);
        $model = model('AnggotaModels');
        $user = $model->where('nrp', $input['email'])->first();

        if(empty($user)) {
            return $this
        ->getResponse(
            [
                'error' => 'NRP atau Password salah',
            ],
            ResponseInterface::HTTP_BAD_REQUEST
        );
        }

        $verify = password_verify($input['password'], $user['password']);
        if(!$verify) return $this
        ->getResponse(
            [
                'error' => 'NRP atau Password salah',
            ],
            ResponseInterface::HTTP_BAD_REQUEST
        );

       return $this->getJWTForUser($input['email'], $input['player_id']);

       
    }

    public function change_password()
    {
        $rules = [
            'email' => 'required|min_length[6]|max_length[50]',
            'password' => 'required|max_length[255]|validateUser[email, password]'
        ];

        $errors = [
            'password' => [
                'validateUser' => 'Password atau Email salah'
            ]
        ];
        $model = model('AnggotaModels');
        $input = $this->getRequestInput($this->request);
        
        $user = $model->where('id',$input['id_anggota'])->first();

        $verify = password_verify($input['password'], $user['password']);
        if(!$verify) return $this
        ->getResponse(
            [
                'error' => 'NRP atau Password salah',
            ],
            ResponseInterface::HTTP_BAD_REQUEST
        );
        

        $dataUp['id'] = $user['id'];
        $dataUp['updated_at'] = date('Y-m-d H:i:s');
        $dataUp['password'] = password_hash($input['password_baru'], PASSWORD_BCRYPT);
        if($model->update($user['id'], $dataUp)){
            return $this
                ->getResponse(
                    [
                        'status' => 'true',
                        'message' => 'Password berhasil diubah',
                    ],
                    ResponseInterface::HTTP_OK
                );
        }
    }
    
    private function getJWTForUser(
        string $emailAddress, string $player_id ,
        int $responseCode = ResponseInterface::HTTP_OK
    )
    {
        try {
            $user = $this->auth->findUserByEmailAddress($emailAddress);
            
            unset($user['password']);

            helper('jwt');
            $model = model('AnggotaModels');
            $dataUp['onesignal_token'] = $player_id;
            $dataUp['id'] = $user['id'];
            $dataUp['updated_at'] = date('Y-m-d H:i:s');
            $model->update($user['id'], $dataUp);

            return $this
                ->getResponse(
                    [
                        'message' => 'User authenticated successfully',
                        'user' => $user,
                        'access_token' => getSignedJWTForUser($emailAddress)
                    ]
                );
        } catch (Exception $exception) {
            return $this
                ->getResponse(
                    [
                        'error' => $exception->getMessage(),
                    ],
                    $responseCode
                );
        }
    }

  
 
}