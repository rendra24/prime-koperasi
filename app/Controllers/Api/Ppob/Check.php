<?php

namespace App\Controllers\Api\Ppob;

use CodeIgniter\RESTful\ResourceController;
use CodeIgniter\API\ResponseTrait;
use App\Controllers\BaseController;
use CodeIgniter\HTTP\ResponseInterface;
use App\Models\MasterAsset;


class Check extends BaseController
{
    use ResponseTrait;

    public function index()
    {
        $model = Model('Ppob');

        $get_ppob = $model->where('flag', 1)->findAll();
        return $this->getResponse(
            [
                'message' => 'Ppob retrieved successfully',
                'data' => $get_ppob,
            ]
        );

    }

    public function operator_prefix()
    {
        $url = $_ENV['URL_PPOB'].'check-operator';
        $data = array(
            "username" => $_ENV['PPOB_USERNAME'],
            "sign" => $this->sign('op'),
            "customer_id" => $this->request->getPost('no_tlp'),
        );

        $result = curl_json($url, 'POST', json_encode($data));
        return $this->getResponse(
            [
                'message' => 'Operator retrieved successfully',
                'operator' => $result['data']['operator'],
            ]
        );
    }

    public function token_pln()
    {
        $url = $_ENV['URL_PPOB'].'inquiry-pln';
        $customer_id = $this->request->getPost('customer_id');
        // $customer_id = '123456789012';
        $data = array(
            "username" => $_ENV['PPOB_USERNAME'],
            "sign" => $this->sign($customer_id),
            "customer_id" => $customer_id,
        );

        $result = curl_json($url, 'POST', json_encode($data));
        
        if($result['data']['status'] == 2){
            return $this->getResponse(
                [
                    'message' => 'Nomor Pelanggan Tidak Ada',
                    'status' => false,
                ]
            );
            return false;
        }
        $dataJson = [
            'customer_id' => $result['data']['customer_id'],
            'meter_no' => $result['data']['meter_no'],
            'name' => $result['data']['name'],
            'segment_power' => $result['data']['segment_power'],
        ];
        return $this->getResponse(
            [
                'message' => 'Operator retrieved successfully',
                'data' => $dataJson,
                'status' => true,
            ]
        );
    }

    public function price_list()
    {
        $type = $this->request->getPost('type');
        $operator = $this->request->getPost('operator');

        $url = $_ENV['URL_PPOB']."pricelist/{$type}/{$operator}";
        $data = array(
            "username" => $_ENV['PPOB_USERNAME'],
            "sign" => $this->sign('pl'),
            "status" => 'all',
        );

        $result = curl_json($url, 'POST', json_encode($data));
        
        $jsonData = [];
        foreach($result['data']['pricelist'] as $row){
            $jsonData[] = array(
               "product_code" => $row['product_code'],
               "product_description" => $row['product_description'],
               "product_nominal" => $row['product_nominal'],
               "product_details" => $row['product_details'],
               "product_price_text" => formatRupiah($row['product_price'] + (0.015 * $row['product_price'])),
               "product_price_value" => $row['product_price'] + (0.015 * $row['product_price']),
               "product_type" => $row['product_type'],
               "active_period" => $row['active_period'],
               "status" => $row['status'],
               "icon_url" => $row['icon_url'],
            );
        }
        
        return $this->getResponse(
            [
                'message' => 'Operator retrieved successfully',
                'data' => $jsonData,
            ]
        );
    }


    // ini adalah produk pascabayar
    public function inquery_pln()
    {
        
        if($customer_id = $this->request->getVar('customer_id')){
        
        $refid = rand();
        $url = $_ENV['URL_POSTPAID'];
        
        $data = array(
            "commands" => "inq-pasca",
            "code" => "PLNPOSTPAID",
            "username" => $_ENV['PPOB_USERNAME'],
            "sign" => $this->sign($refid),
            "ref_id" => $refid,
            "hp" => $customer_id,
        );

        $result = curl_json($url.'v1/bill/check', 'POST', json_encode($data));
        
        if($result['data']['response_code'] == '01'){
            return $this->getResponse(
                [
                    'message' => $result['data']['message'],
                    'data' => [],
                ]
            );die;
        }

        $jsonData = [];
        if(isset($result['data']['nominal'])){
            $jsonData = array(
                "tr_id" => $result['data']['tr_id'],
                "code" => $result['data']['code'],
                "hp" => $result['data']['hp'],
                "tr_name" => $result['data']['tr_name'],
                "period" => $result['data']['period'],
                "nominal" => $result['data']['nominal'],
                "admin" => $result['data']['admin'],
                "ref_id" => $result['data']['ref_id'],
                "message" => $result['data']['message'],
                "price" => $result['data']['price'],
                "selling_price" => $result['data']['selling_price'],
                "total" => $result['data']['desc']['tagihan']['detail'][0]['total'],
            );
        } 
        
        $dataTemp['tr_id'] = $jsonData['tr_id'];
        $dataTemp['anggota_id'] = $this->request->getPost('id_anggota');
        $dataTemp['ref_id'] = $jsonData['ref_id'];
        $dataTemp['customer_id'] = $customer_id;
        $dataTemp['product_code'] = $jsonData['code'];
        $dataTemp['price'] = $jsonData['price'];
        $dataTemp['price_sell'] = $jsonData['total'];
        $dataTemp['status'] = 0;
        $dataTemp['message'] = $jsonData['message'];
        $dataTemp['created_at'] = date('Y-m-d H:i:s');

        $model = model('ppob/TransaksiPpobTemp');
        $model->save($dataTemp);

        $jsonData['price'] = formatdecimals($jsonData['nominal']);
        $jsonData['admin'] = formatdecimals($jsonData['admin']);
        $jsonData['selling_price'] = formatdecimals($jsonData['selling_price']);
        $jsonData['nominal'] = formatdecimals($jsonData['nominal']);
        $jsonData['total'] = formatdecimals($jsonData['total']);
        
        return $this->getResponse(
            [
                'message' => $result['data']['message'],
                'data' => $jsonData,
            ]
        );

        }else{
            return $this->getResponse(
                [
                    'message' => 'Customer not found',
                ],
                ResponseInterface::HTTP_BAD_REQUEST);
        }
    }

    public function status()
    {
        $model = model('Ppob/TransaksiModel');
        $ref_id = $this->request->getVar('ref_id');

        $url = $_ENV['URL_PPOB'].'check-status';
        $data = array(
            "username" => $_ENV['PPOB_USERNAME'],
            "sign" => $this->sign($ref_id),
            "ref_id" => $ref_id,
        );

        $result = curl_json($url, 'POST', json_encode($data));
        
        $get_trransaksi = $model->where('ref_id', $ref_id)->first();

        if($result['data']['rc'] == '00'){
            if($get_trransaksi){
                $up['sn'] = $result['data']['sn'];
                $up['id'] = $get_trransaksi['id'];
                $model->save($up);
            }
        }

        $jsonData = [
            'ref_id' => $get_trransaksi['ref_id'],
            'product_code' => $get_trransaksi['product_code'],
            'price' => $get_trransaksi['price_sell'],
            'message' => $get_trransaksi['message'],
            'created_at' => $get_trransaksi['created_at'],
        ];

        if(isset($result['data']['sn'])){
            $code = explode('/' , $result['data']['sn']);
            $jsonData['sn'] = $code[0];
            $jsonData['message'] = $result['data']['message'];

        }
        
        return $this->getResponse(
            [
                'message' => 'Check successful',
                'data' => $jsonData,
            ]
        );
    }



}