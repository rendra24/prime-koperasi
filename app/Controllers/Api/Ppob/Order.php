<?php

namespace App\Controllers\Api\Ppob;

use CodeIgniter\RESTful\ResourceController;
use CodeIgniter\API\ResponseTrait;
use App\Controllers\BaseController;
use CodeIgniter\HTTP\ResponseInterface;


class Order extends BaseController
{
    use ResponseTrait;

    public function topup(){
        $db = \Config\Database::connect();
        $model = model('Ppob/TransaksiModel');
        $modTran = model('TransaksiModel');
        $ModAsset = model('MasterAsset');
        $ModAnggota = model('AnggotaModels');
        // $customer_id = '085933008404';
        // $product_code = 'xld25000';
        // $anggota_id = 58;

        $rules = [
			"customer_id" => "required",
			"product_code" => "required",
			"price" => "required",
			"anggota_id" => "required",
		];

		$messages = [
			"customer_id" => [
				"required" => "Customer is required"
			],
			"product_code" => [
				"required" => "Product is required",
			],
			"price" => [
				"required" => "Price is required"
			],
            "anggota_id" => [
				"required" => "Anggota is required"
			],
		];

		if (!$this->validate($rules, $messages)) {
            return $this->getResponse(
                [
                    'message' => $this->validator->getErrors(),
                ],
                ResponseInterface::HTTP_BAD_REQUEST);
                die;
        }

        $db->transStart();

        $customer_id = $this->request->getPost('customer_id');
        $product_code = $this->request->getPost('product_code');
        $anggota_id = $this->request->getPost('anggota_id');
        $price = $this->request->getPost('price');
        $transaction_code = 'TOPUP'.rand();

        
        $get = $modTran->getSaldoUser($anggota_id)->getRowArray();

        
        

        if($get['simpanan_sukarela'] <= $price){
            return $this->getResponse(
                [
                    'message' => 'Saldo tidak cukup',
                    'status' => false,
                ]
            );
            return false;
        }

        //input transaksi
        $dataTr['kode_transaksi'] = 'PPOB'.rand();
        $dataTr['id_anggota'] = $anggota_id;
        $dataTr['id_asset'] = 4;
        $dataTr['total'] = $price;
        $dataTr['keterangan'] = 'Pembelian Produk PPOB';
        $dataTr['id_transaksi'] = 6;
        $dataTr['status_transaksi'] = 2;
        $modTran->save($dataTr);

        $last_id_transaksi = $modTran->getInsertID();
        
        //update saldo user
        $dataSaldo['simpanan_sukarela'] = $get['simpanan_sukarela'] - $price;
        $dataSaldo['id'] = $anggota_id;
        
        $ModAnggota->save($dataSaldo);



        $url = $_ENV['URL_PPOB']."top-up";
        $data = array(
            "username" => $_ENV['PPOB_USERNAME'],
            "ref_id" => $transaction_code,
            "customer_id" => $customer_id,
            "product_code" => $product_code,
            "sign" => $this->sign($transaction_code)
        );

        $result = curl_json($url, 'POST', json_encode($data));

        //input transaksi ppob
        $formatData = array(
            "id_transaksi" => $last_id_transaksi,
            "anggota_id" => $anggota_id,
            "ref_id" => $transaction_code,
            "customer_id" => $customer_id,
            "product_code" => $product_code,
            "price" => $result['data']['price'],
            "price_sell" => $result['data']['price'] + (0.015 * $result['data']['price']),
            "keuntungan" => 0.015 * $result['data']['price'],
            "status" => $result['data']['status'],
            "message" => $result['data']['message'],
            "created_at" => date('Y-m-d H:i:s'),
        );

        $model->save($formatData);

        //update pengurangan kas 
        $asset = $ModAsset->where('id', 4)->first();
        $add['total'] = $asset['total'] - $price;
        $add['id'] = 4;
        $ModAsset->save($add);

        $db->transComplete();


        if ($db->transStatus() === false) {
            return $this->getResponse(
                [
                    'message' => 'Order galgal',
                    'status' => false,
                ]
            );
            return false;
        }
        
        return $this->getResponse(
            [
                'message' => 'Order dalam proses',
                'status' => true,
            ]
        );

        // dd($result);
    }
    public function payment_inquery() 
    {
        
        if($tr_id = $this->request->getVar('tr_id')){
        
            $anggota_id = $this->request->getVar('anggota_id');

            $modelTemp = model('Ppob/TransaksiPpobTemp');
            $modTran = model('TransaksiModel');

            $get_tr = $modelTemp->where('tr_id', $tr_id)->where('anggota_id', $anggota_id)->first();
            $get_agt = $modTran->getSaldoUser($anggota_id)->getRowArray();

            if(empty($get_tr) || empty($get_agt)){
                return $this->getResponse(
                    [
                        'message' => 'Data tidak ditemukan',
                    ],
                    ResponseInterface::HTTP_BAD_REQUEST);
            }

            if($get_agt['simpanan_sukarela'] <= $get_tr['price']){
                return $this->getResponse(
                    [
                        'message' => 'Maaf saldo kamu tidak cukup',
                    ],
                    );
            }

            $url = $_ENV['URL_POSTPAID'];
            $data = array(
                "commands" => "pay-pasca",
                "username" => $_ENV['PPOB_USERNAME'],
                "tr_id" => $get_tr['tr_id'],
                "sign" => $this->sign($tr_id),
            );

            $result = curl_json($url, 'POST', json_encode($data));
            echo json_encode($result);die;

            $responOk = array('00');
            if (!in_array($result['data']['response_code'], $responOk))
            {
                return $this->getResponseError(
                     $result['data']['message'],
                    ResponseInterface::HTTP_BAD_REQUEST);die;
            }

            $db = \Config\Database::connect();
            $model = model('Ppob/TransaksiModel');
            
            $ModAsset = model('MasterAsset');
            $ModAnggota = model('AnggotaModels');


            $db->transStart();

            //input transaksi
            $dataTr['kode_transaksi'] = 'PPOB'.rand();
            $dataTr['id_anggota'] = $anggota_id;
            $dataTr['id_asset'] = 4;
            $dataTr['total'] = $result['data']['price'];
            $dataTr['keterangan'] = 'Pembelian Produk PPOB';
            $dataTr['id_transaksi'] = 6;
            $dataTr['status_transaksi'] = 2;
            $modTran->save($dataTr);
    
            $last_id_transaksi = $modTran->getInsertID();


            //update saldo user
            $dataSaldo['simpanan_sukarela'] = $get_agt['simpanan_sukarela'] - $result['data']['price'];
            $dataSaldo['id'] = $anggota_id;
            
            $ModAnggota->save($dataSaldo); 
        
            //input transaksi ppob
            $formatData = array(
                "id_transaksi" => $last_id_transaksi,
                "anggota_id" => $anggota_id,
                "ref_id" => $result['data']['ref_id'],
                "customer_id" => $result['data']['hp'],
                "product_code" => $result['data']['code'],
                "price" => $result['data']['price'],
                "price_sell" => $result['data']['price'],
                // "keuntungan" => 0.015 * $result['data']['price'],
                "status" => 1,
                "message" => $result['data']['message'],
                "created_at" => date('Y-m-d H:i:s'),
            );

            $model->save($formatData);

            $id_ppob_transaksi = $model->getInsertID();

            //input transaksi pln
            if($result['data']['code'] == 'PLNPOSTPAID')
            {
                $modelPln = model('Ppob/TransaksiPln');

                $dataPln['id_ppob_transaksi'] = $id_ppob_transaksi;
                $dataPln['tr_name'] = $result['data']['tr_name'];
                $dataPln['period'] = $result['data']['period'];
                $dataPln['nominal'] = $result['data']['nominal'];
                $dataPln['admin'] = $result['data']['admin'];
                $dataPln['message'] = $result['data']['message'];
                $dataPln['price'] = $result['data']['price'];
                $dataPln['selling_price'] = $result['data']['selling_price'];
                $dataPln['balance'] = $result['data']['balance'];
                $dataPln['lembar_tagihan'] = $result['data']['desc']['lembar_tagihan'];
                $dataPln['meter_awal'] = $result['data']['desc']['tagihan']['detail'][0]['meter_awal'];
                $dataPln['meter_akhir'] = $result['data']['desc']['tagihan']['detail'][0]['meter_akhir'];
                $dataPln['denda'] = $result['data']['desc']['tagihan']['detail'][0]['denda'];
                $dataPln['total'] = $result['data']['desc']['tagihan']['detail'][0]['total'];

                $modelPln->save($dataPln);

            }

            //update pengurangan kas 
            $asset = $ModAsset->where('id', 4)->first();
            $add['total'] = $asset['total'] - $result['data']['price'];
            $add['id'] = 4;
            $ModAsset->save($add);
            
            $db->transComplete();


            if ($db->transStatus() === false) {
                return $this->getResponse(
                    [
                        'message' => 'Order gagal ulangi order kembali',
                    ],
                    ResponseInterface::HTTP_BAD_REQUEST);
            }

            return $this->getResponse(
                [
                    'message' => 'Order berhasil diproses',
                    'status' => true,
                ]
            );



        }else{
            return $this->getResponse(
                [
                    'message' => 'Customer not found',
                ],
                ResponseInterface::HTTP_BAD_REQUEST);
        }
    }
    public function riwayat(){
        $model = model('Ppob/TransaksiModel');
        
    }
}