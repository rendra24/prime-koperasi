<?php

namespace App\Controllers\Api\Ppob;

use CodeIgniter\RESTful\ResourceController;
use CodeIgniter\API\ResponseTrait;
use App\Controllers\BaseController;


class Callback extends BaseController
{
    use ResponseTrait;

    public function index(){
        $model = model('Ppob/TransaksiModel');
        $modAnggota = model('AnggotaModels');
        $json = file_get_contents('php://input');

        $data = json_decode($json);

        $formatData = array(
            "message" => $data->data->message,
            "status" => $data->data->status
        );

        $get = $model->where('ref_id', $data->data->ref_id)->first();
        if($get){
            $model->update($get['id'],$formatData);
            $getAnggota = $modAnggota->where('id', $get['anggota_id'])->first();
            send_notif_mobile("Pembelian {$get['product_code']} {strtolower($data->data->message)}", $getAnggota['onesignal_token'],  'notif' , $get['id']);
        }




    }
}