<?php

namespace App\Validation;

use CodeIgniter\HTTP\RequestInterface;
use Config\Services;

/**
 * File validation rules
 */
class CustomImage
{

    /**
     * Request instance. So we can get access to the files.
     *
     * @var \CodeIgniter\HTTP\RequestInterface
     */
    protected $request;

    //--------------------------------------------------------------------

    /**
     * Constructor.
     *
     * @param RequestInterface $request
     */
    public function __construct(RequestInterface $request = null)
    {
        if (is_null($request)) {
            $request = Services::request();
        }

        $this->request = $request;
    }

    //--------------------------------------------------------------------

    /**
     * Checks an uploaded file to verify that the dimensions are within
     * a specified allowable dimension.
     *
     * @param string|null $blank
     * @param string      $params
     *
     * @return boolean
     */
    public function img_scale(string $blank = null, string $params): bool
    {
        // Grab the file name off the top of the $params
        // after we split it.
        $params = explode(',', $params);
        $name   = array_shift($params);

        if (!($files = $this->request->getFileMultiple($name))) {
            $files = [$this->request->getFile($name)];
        }

        foreach ($files as $file) {
            if (is_null($file)) {
                return false;
            }

            if ($file->getError() === UPLOAD_ERR_NO_FILE) {
                return true;
            }

            $scale = explode(':', $params[0]);

            // Get uploaded image size
            $info       = getimagesize($file->getTempName());
            $fileWidth  = $info[0];
            $fileHeight = $info[1];

            if (($fileWidth / $fileHeight) != ($scale[0] / $scale[1])) {
                return false;
            }
        }

        return true;
    }

    /**
     * Checks an uploaded file to verify that the dimensions are within
     * a specified allowable dimension.
     *
     * @param string|null $blank
     * @param string      $params
     *
     * @return boolean
     */
    public function img_size(string $blank = null, string $params): bool
    {
        // Grab the file name off the top of the $params
        // after we split it.
        $params = explode(',', $params);
        $name   = array_shift($params);

        if (!($files = $this->request->getFileMultiple($name))) {
            $files = [$this->request->getFile($name)];
        }

        foreach ($files as $file) {
            if (is_null($file)) {
                return false;
            }

            if ($file->getError() === UPLOAD_ERR_NO_FILE) {
                return true;
            }

            $size = explode(':', $params[0]);

            // Get uploaded image size
            $info       = getimagesize($file->getTempName());
            $fileWidth  = $info[0];
            $fileHeight = $info[1];

            if (($fileWidth != $size[0]) || ($fileHeight != $size[1])) {
                return false;
            }
        }

        return true;
    }

    //--------------------------------------------------------------------
}
