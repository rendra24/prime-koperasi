<?php

namespace App\Validation;

use App\Models\AuthAnggotaModel;
use App\Models\Masterasset;
use Exception;

class UserRules
{
    public function validateUser(string $str, string $fields, array $data): bool
    {
        try {
            $model = new AuthAnggotaModel();
            $user = $model->findUserByEmailAddress($data['email']);
            return password_verify($data['password'], $user['password_hash']);
        } catch (Exception $e) {
            return false;
        }
    }

    public function CekTotalAsset(string $str, string $fields, array $data): bool
    {
        try {
            if($data['status_transaksi'] != '1'){
                $model = new Masterasset();
                $asset = $model->getsaldo($data['id_asset'])->getRowArray();
                if((int)$data['total'] <= (int)$asset['total']){
                    return true;
                }else{
                    return false;
                }
            }
            return true;
        }catch (Exception $e){
            return false;
        }
    }

}