    <style>
.text-label {
    font-size 13px;
    color: #666;
    font-weight: 600;
    margin: 0;
    margin-bottom: 5px;
}

.text-value {
    font-size 13px;
    color: #333;
    font-weight: 800;
    margin: 0;
    margin-bottom: 10px;
}
    </style>
    <div class="modal-header">
        <h4 class="modal-title"><?= (!@$id ? 'Tambah' : '') ?> Data Pinjaman</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-6">
                <p class="text-label">Nama Anggota</p>
                <p class="text-value"><?= $nama_anggota; ?></p>

                <p class="text-label">Total Pinjaman</p>
                <p class="text-value"><?= formatdecimals($total_pinjaman); ?></p>

                <p class="text-label">Angsuran</p>
                <p class="text-value"><?= "{$angsuran} Bulan"; ?></p>
            </div>
            <div class="col-md-6">
                <p class="text-label">Bunga</p>
                <p class="text-value"><?= "{$bunga} %"; ?></p>

                <p class="text-label">Tanggal Pengajuan</p>
                <p class="text-value"><?=formatTglIndo($created_at); ?></p>
            </div>

            <?php if($status_pinjaman == 1): ?>
            <div class="col-md-12">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Cicilan</th>
                            <th style="text-align:right;">Total Cicilan</th>
                            <th style="text-align:right;">Bunga</th>
                            <th style="width:15%;">Status</th>
                            <th style="width:18%;">Action</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($detail as $row): ?>
                        <tr>
                            <td>Bulan <?= $row['angsuran']; ?></td>
                            <td style="text-align:right;font-weight: 600;">
                                <?= formatdecimals($row['cicilan']); ?>
                                <div class="form-group">
                                    <input type="number" name="cicilan<?= $row['id']; ?>" class="form-control"
                                        value="<?= $row['cicilan']; ?>" style="display:none">
                                </div>
                            </td>
                            <td style="text-align:right;font-weight: 600;">
                                <?= formatdecimals($row['bunga']); ?>
                                <div class="form-group">
                                    <input type="number" name="bunga<?= $row['id']; ?>" class="form-control"
                                        value="<?= $row['bunga']; ?>" style="display:none">
                                </div>
                            </td>
                            <td style="text-align:center;">
                                <?php if($row['status_pembayaran'] == 0){ ?>
                                <h4><span class="badge badge-danger">Unpaid</span></h4>
                                <?php }else{ ?>
                                <h4><span class="badge badge-success">Paid</span></h4>
                                <?php } ?>
                            </td>
                            <td>
                                <?php if($row['status_pembayaran'] == 0){ ?>
                                <button type="button" class="btn btn-sm btn-primary btn-cicil"
                                    data-url="<?= base_url('pinjaman/angsur'); ?>" key="<?= $row['id']; ?>"><i
                                        class="cil-input"></i>Angsur</button>
                                <button class="btn btn-warning btn-sm btn-edit" key="<?= $row['id']; ?>"><i
                                        class="cil-pencil"></i>Edit</button>
                                <?php }else{ ?>
                                <h4><span class="badge badge-success">Lunas</span></h4>
                                <?php } ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <?php endif; ?>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn btn-danger" type="button" data-dismiss="modal">Close</button>
        <?php if($status_pinjaman != 1 && $status_pinjaman != 3): ?>
        <div class="float-right">
            <button class="btn btn-primary btn-acc" type="button" data-url="<?= base_url('pinjaman/verifikasi'); ?>"
                data-status="1" key="<?= $id; ?>">Setujui
                Pinjaman</button>
            <button class="btn btn-warning btn-acc" data-status="3" type="button"
                data-url="<?= base_url('pinjaman/tolak'); ?>" key="<?= $id; ?>">Tolak Pinjaman</button>
        </div>
        <?php endif; ?>
    </div>

    <script>
$(function() {

    $('.btn-acc').click(function() {
        var url = $(this).attr('data-url');
        var id = $(this).attr('key');
        var status_pinjaman = $(this).attr('data-status');
        var status = (status_pinjaman == 1) ? 'Setujui' : 'Tolak';


        swal({
                title: "Apakah Kamu Yakin?",
                text: `Pengajuan pijaman akan di ${status} !`,
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: {
                            id: id,
                            status_pinjaman: status_pinjaman
                        },
                        success: function(data) {
                            swal("Data berhasil di Ubah !", {
                                icon: "success",
                            });
                            loadDatatables(".tableAjax");
                            $("#modalSide").modal('hide');
                        },
                        error: function() {
                            swal({
                                title: "Gagal memverifikasi data",
                                text: "Periksa kembali koneksi anda !",
                                icon: "error",
                            });
                        },
                    });
                }
            });
    });
    $('.btn-edit').click(function() {
        var id = $(this).attr('key');
        $(`input[name=cicilan${id}]`).toggle();
        $(`input[name=bunga${id}]`).toggle();
    });
    $('.btn-cicil').click(function() {
        var url = $(this).attr('data-url');
        var id = $(this).attr('key');
        var status_pembayaran = 1;
        var id_pinjaman = '<?= $id; ?>';
        var cicilan = $(`input[name=cicilan${id}]`).val();
        var bunga = $(`input[name=bunga${id}]`).val();
        swal({
                title: "Apakah Kamu Yakin?",
                text: "Mengubah angsuran sudah di bayaran!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: {
                            id: id,
                            status_pembayaran: status_pembayaran,
                            id_pinjaman: id_pinjaman,
                            cicilan: cicilan,
                            bunga: bunga
                        },
                        success: function(data) {
                            swal("Angsuran berhasil di bayarkan !", {
                                icon: "success",
                            });
                            loadDatatables(".tableAjax");
                            $("#modalSide").modal('hide');
                        },
                        error: function() {
                            swal({
                                title: "Gagal memverifikasi data",
                                text: "Periksa kembali koneksi anda !",
                                icon: "error",
                            });
                        },
                    });
                }
            });
    });

});
    </script>