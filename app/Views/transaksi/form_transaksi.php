<style>
.select2-selection {
    width: 100%;
    height: 35px !important;
    border: 1px solid #DDD !important;
    padding: 2px 5px !important;
}
</style>
<form action="<?= base_url('transaksi/save') ?>" class="ajax-multipart modal-content" data-respond="reload">
    <?php if (@$id) : ?>
    <input type="hidden" name="id" value="<?= $id ?>">
    <?php endif; ?>"
    <input type="hidden" name="nama_anggota" id="nama_anggota">
    <input type="hidden" name="id_anggota" id="id_anggota">
    <div class="modal-header">
        <h4 class="modal-title"><?= (!@$id ? 'Tambah' : '') ?> Transaksi Baru</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <label for="id_pinjaman" class="mb-1">Nama Anggota</label>
                <select class="form-control select2" data-placeholder="Masukkan Nama Anggota"
                    select-url="<?= base_url('master/anggota/get_anggota') ?>">

                </select>
                <div class="invalid-feedback"></div>
            </div>
            <div class="col-md-12">
                <label>Nominal</label>
                <input type="number" name="nominal" id="nominal" class="form-control">
            </div>
            <div class="col-md-12">
                <label>Keterangan</label>
                <textarea name="keterangan" id="keterangan" class="form-control" cols="30" rows="10"></textarea>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn btn-danger" type="button" data-dismiss="modal">Close</button>
        <button class="btn btn-success float-right" type="submit">Simpan</button>
    </div>
</form>

<script>
$(function() {
    var url = $('.select2').attr('select-url');
    var placeholder = $('.select2').attr('data-placeholder');
    $('.select2').select2({
        minimumInputLength: 2,
        allowClear: true,
        placeholder: placeholder,
        selectOnClose: true,
        dropdownParent: $("#modalSide"),
        ajax: {
            dataType: 'json',
            url: url,
            delay: 250,
            data: function(params) {
                return {
                    search: params.term
                }
            },
            processResults: function(data, page) {
                return {
                    results: $.map(data, function(item) {
                        return {
                            id: item.id,
                            text: item.text
                        }
                    })
                }
            },

        },
    }).on('select2:select', function(evt) {

        var data = $(".select2 option:selected").text();
        var value = $(".select2 option:selected").val();
        $('#nama_anggota').val(data);
        $('#id_anggota').val(value);
    });

});
</script>