<?= $this->extend('general_layout'); ?>
<?= $this->section('breadcrumb'); ?>
<ol class="breadcrumb border-0 m-0">
    <li class="breadcrumb-item active">Tabungan/Simpanan Excel</li>
</ol>
<?= $this->endSection('breadcrumb'); ?>
<?= $this->section('main'); ?>
<div class="container-fluid">
    <div class="fade-in">
        <div class="card">
            <div class="card-header">
                <h5 class="card-title mb-0">Import Soal Excel</h5>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="alert alert-warning" role="alert">
                            <p class="m-0">Gunakan Excel yang sesuai dengan format berikut. Download format excel <a
                                    href="<?= base_url('excel/simpanan_template.xlsx'); ?>" download>Disini</a>. <br>
                            </p>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <?= form_open_multipart('import/simpanan/prosesExcel');?>
                        <div class="form-group">
                            <label for="file" class="mb-1">File Excel</label>
                            <input type="file" name="file" id="file" class="form-control">
                            <div class="invalid-feedback"></div>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary" type="submit">Import</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-5">
                        <h5 class="card-title mb-0">Preview Import Excel</h5>
                        <div class="alert alert-warning mt-2" role="alert">
                            <p class="m-0">Data yang di tampilan masih bersifat temporary. Data akan di pindah ke tabel
                                asli
                                apabila sudah <b>Simpan</b></p>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <?php if($total > 0): ?>
                        <button class="btn btn-danger mb-2 float-right btn-action"
                            data-url="<?= base_url('import/simpanan/reset_excel') ?>"
                            data-title="Apakah anda yakin akan mereset soal excel ?"
                            data-pesan="Data Import excel akan terhapus !" data-success="Reset data berhasil"
                            data-reload="true"><i class="cil cil-trash"></i> Reset
                            Data</button>
                        <button class="btn btn-success mb-2 float-right btn-action mr-2" type="button"
                            data-url="<?= base_url('import/simpanan/move'); ?>"
                            data-title="Apakah anda yakin akan menyimpan soal ?"
                            data-pesan="Pastikan semua soal terisi !"
                            data-success="Soal berhasil di masukkan">Simpan</button>

                        <?php endif; ?>
                    </div>
                </div>

            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="tableAjax" class="tableAjax table table-bordered table-striped w-100"
                        data-source="<?= base_url('simpanan/data_excel'); ?>">
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection('main'); ?>