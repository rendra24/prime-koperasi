<?= $this->extend('general_layout'); ?>
<?= $this->section('breadcrumb'); ?>
<ol class="breadcrumb border-0 m-0">
    <li class="breadcrumb-item active">Tabungan/Simpanan</li>
</ol>
<?= $this->endSection('breadcrumb'); ?>
<?= $this->section('main'); ?>
<div class="container-fluid">
    <div class="fade-in">
        <div class="card">
            <div class="card-header">
                <h5 class="card-title mb-0">Simpanan</h5>
            </div>
            <div class="card-body">
                <button class="btn btn-success mb-2 float-right" data-toggle="modal" data-target="#modalSide"
                    data-page="simpanan/form">
                    <div class="c-icon mr-1 cil-plus"></div>
                    <span>Tambah Simpanan</span>
                </button>
                <a href="<?= base_url('simpanan/form_excel') ?>" class="btn btn-success mb-2 mr-2 float-right">
                    <div class="c-icon mr-1 cil-file"></div>
                    <span>Tambah Excel</span>
                </a>
                <div class="table-responsive">
                    <table id="tableAjax" class="tableAjax table table-bordered table-striped w-100"
                        data-source="<?= base_url('simpanan/data'); ?>">
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection('main'); ?>