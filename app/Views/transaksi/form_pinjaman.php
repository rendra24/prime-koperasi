<style>
.select2-selection {
    width: 100%;
    height: 35px !important;
    border: 1px solid #DDD !important;
    padding: 2px 5px !important;
}
</style>
<form action="<?= base_url('pinjaman/save') ?>" class="form-pinjam modal-content" data-respond="reload">
    <?php if (@$id) : ?>
    <input type="hidden" name="id" value="<?= $id ?>">
    <?php endif; ?>"
    <input type="hidden" name="nama_anggota" id="nama_anggota">
    <input type="hidden" name="id_anggota" id="id_anggota">
    <div class="modal-header">
        <h4 class="modal-title"><?= (!@$id ? 'Tambah' : '') ?> Data Pinjaman</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <label for="id_pinjaman" class="mb-1">Nama Anggota</label>
                <select class="form-control select2" data-placeholder="Masukkan Nama Anggota"
                    select-url="<?= base_url('master/anggota/get_anggota') ?>">

                </select>
                <div class="invalid-feedback"></div>
            </div>
            <div class="col-md-12">
                <label for="id_asset" class="mb-1">Asset</label>
                <select name="id_asset" id="id_asset" class="form-control">
                    <option>Pilih Asset</option>
                    <?php foreach($asset as $row): ?>
                    <option value="<?=$row['id'];?>"><?= $row['nama_asset']; ?>
                        (<?= formatDecimals($row['total']); ?>)</option>
                    <?php endforeach; ?>
                </select>
                <div class="invalid-feedback"></div>
            </div>
            <div class="col-md-12">
                <label for="bunga" class="mb-1">Jenis Pinjaman</label>
                <select name="id_pinjaman" id="id_pinjaman" class="form-control">
                    <option>Pilih Jenis Pinjaman</option>
                    <?php foreach($pinjaman as $row): ?>
                    <option value="<?=$row['id'];?>" data-bunga="<?=$row['bunga'];?>"><?= $row['nama_pinjaman']; ?>
                    </option>
                    <?php endforeach; ?>
                </select>
                <div class="invalid-feedback"></div>
            </div>
            <div class="col-md-12">
                <label for="angsuran" class="mb-1">Total Angsuran</label>
                <select name="angsuran" id="angsuran" class="form-control">
                    <option>Pilih Angsuran</option>
                    <?php foreach($angsuran as $row): ?>
                    <option value="<?=$row['angsuran'];?>"><?= $row['nama_angsuran']; ?>
                    </option>
                    <?php endforeach; ?>
                </select>
                <div class="invalid-feedback"></div>
            </div>
            <div class="col-md-12">
                <label>Bunga</label>
                <input type="text" name="bunga" id="bunga" class="form-control" readonly>
                <div class="invalid-feedback"></div>
            </div>
            <div class="col-md-12">
                <label>Cicilan Perbulan</label>
                <input type="text" name="cicilan" id="cicilan" class="form-control" readonly>
                <div class="invalid-feedback"></div>
            </div>
            <div class="col-md-12">
                <label>Total Pinjaman</label>
                <input type="number" name="total" id="total_pinjaman" class="form-control">
                <div class="invalid-feedback"></div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn btn-danger" type="button" data-dismiss="modal">Close</button>
        <button class="btn btn-success float-right" type="submit">Simpan</button>
    </div>
</form>

<script>
function hitung_cicilan() {
    var total_pinjaman = $('#total_pinjaman').val();
    var bunga = $('#bunga').val();
    var angsuran = $('#angsuran').val();

    var cicilan = parseInt(total_pinjaman) / parseInt(angsuran);
    var total_bunga = bunga / 100 * parseInt(total_pinjaman);
    var hasil = parseInt(cicilan) + parseInt(total_bunga);
    $('#cicilan').val(hasil);
}

$(function() {

    $('.form-pinjam').submit(function(event) {
        event.preventDefault();
        $.ajax({
            url: $(this).attr('action'),
            data: $(this).serialize(),
            dataType: 'json',
            method: 'POST',
            cache: false,
            success: function(data) {
                if (data.response == 'error') {
                    swal("Maaf", data.data.msg, "error");
                } else {
                    swal("Berhasil", "Data berasil di tambahkan", "success");
                    location.reload();
                }
            }
        });
    });


    var url = $('.select2').attr('select-url');
    var placeholder = $('.select2').attr('data-placeholder');
    $('.select2').select2({
        minimumInputLength: 2,
        allowClear: true,
        placeholder: placeholder,
        selectOnClose: true,
        dropdownParent: $("#modalSide"),
        ajax: {
            dataType: 'json',
            url: url,
            delay: 250,
            data: function(params) {
                return {
                    search: params.term
                }
            },
            processResults: function(data, page) {
                return {
                    results: $.map(data, function(item) {
                        return {
                            id: item.id,
                            text: item.text
                        }
                    })
                }
            },

        },
    }).on('select2:select', function(evt) {

        var data = $(".select2 option:selected").text();
        var value = $(".select2 option:selected").val();
        $('#nama_anggota').val(data);
        $('#id_anggota').val(value);
    });

    $('#id_pinjaman').change(function() {
        var bunga = $('option:selected', this).attr('data-bunga');
        $("#bunga").val(bunga);
    });

    $('#total_pinjaman').on('blur', function() {
        hitung_cicilan();

    });
});
</script>