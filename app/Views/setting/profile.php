<?= $this->extend('general_layout'); ?>
<?= $this->section('breadcrumb'); ?>
<ol class="breadcrumb border-0 m-0">
    <li class="breadcrumb-item">Pengaturan</li>
    <li class="breadcrumb-item active">Profil</li>
</ol>
<?= $this->endSection('breadcrumb'); ?>
<?= $this->section('main'); ?>
<div class="container-fluid">
    <div class="fade-in row">
        <div class="col-md-7">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title mb-0">Profile</h5>
                </div>
                <div class="card-body">
                    <form action="<?= base_url('setting/profile') ?>" class="row ajax-multipart" data-respond="reload">
                        <input type="hidden" name="id" value="<?= $profile['id'] ?>">
                        <div class="col-md-4 mb-2 pr-0">
                            <label for="photo" class="mb-1">Foto</label>
                            <?php if (@$profile['photo']) : ?>
                                <img src="<?= base_url('uploads/' . $profile['photo']); ?>" class="img-thumbnail w-100 mb-2">
                            <?php else : ?>
                                <img src="<?= base_url('assets/img/default-avatar.png'); ?>" class="img-thumbnail w-100 mb-2">
                            <?php endif; ?>
                            <input type="file" class="form-control-file" id="photo" name="photo" aria-describedby="fotoHelp">
                            <div class="invalid-feedback"></div>
                            <small id="fotoHelp" class="form-text text-muted">
                                Foto harus berukuran 1:1. <br> Cth. 500px x 500px
                            </small>
                            <input type="hidden" name="old_phto" value="<?= @$photo ?>">
                        </div>
                        <div class="col-md-8 mb-2">
                            <div class="row">
                                <div class="col-md-12 mb-2">
                                    <label for="name" class="mb-1">Nama Lengkap</label>
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Nama Lengkap" value="<?= @$profile['name'] ?>">
                                    <div class="invalid-feedback"></div>
                                </div>
                                <div class="col-md-12 mb-2">
                                    <label for="username" class="mb-1">Username</label>
                                    <input type="hidden" name="old_username" value="<?= @$profile['username'] ?>">
                                    <input type="text" class="form-control" id="username" name="username" placeholder="Username" value="<?= @$profile['username'] ?>">
                                    <div class="invalid-feedback"></div>
                                </div>
                                <div class="col-md-12 mb-2">
                                    <label for="phone" class="mb-1">No. Telp</label>
                                    <input type="text" class="form-control" id="phone" name="phone" placeholder="No. Telp" value="<?= @$profile['phone'] ?>">
                                    <div class="invalid-feedback"></div>
                                </div>
                                <div class="col-md-12 mb-2">
                                    <label for="email" class="mb-1">Email</label>
                                    <input type="text" class="form-control" id="email" name="email" placeholder="Email" value="<?= @$profile['email'] ?>">
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="col-md-12 mb-2">
                            <label for="password" class="mb-1">Password</label>
                            <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                            <div class="invalid-feedback"></div>
                            <small id="passwordHelp" class="form-text text-muted">
                                Silahkan memasukkan password anda untuk mengubah profile.
                            </small>
                        </div>
                        <div class="col-md-12 mb-2">
                            <button type="submit" class="btn btn-primary float-right">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-5 pl-1">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title mb-0">Password</h5>
                </div>
                <div class="card-body">
                    <form action="<?= base_url('setting/password') ?>" class="row ajax" data-respond="reload">
                        <input type="hidden" name="username" value="<?= @$profile['username'] ?>">
                        <div class="col-md-12 mb-2">
                            <label for="old_password" class="mb-1">Password Lama</label>
                            <input type="password" class="form-control" id="old_password" name="old_password" placeholder="Password Lama">
                            <div class="invalid-feedback"></div>
                        </div>
                        <div class="col-md-12 mb-2">
                            <label for="new_password" class="mb-1">Password Baru</label>
                            <input type="password" class="form-control" id="new_password" name="new_password" placeholder="Password Baru">
                            <div class="invalid-feedback"></div>
                        </div>
                        <div class="col-md-12 mb-2">
                            <label for="confirm_new_password" class="mb-1">Konfirmasi Password Baru</label>
                            <input type="password" class="form-control" id="confirm_new_password" name="confirm_new_password" placeholder="Konfirmasi Password Baru">
                            <div class="invalid-feedback"></div>
                        </div>
                        <div class="col-md-12 mb-2">
                            <button type="submit" class="btn btn-primary float-right">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection('main'); ?>