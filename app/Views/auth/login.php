<?= $this->extend('auth/layout'); ?>

<?= $this->section('content'); ?>
<form action="<?= route_to('login') ?>" method="post" class="row justify-content-center">
    <?= csrf_field() ?>
    <div class="col-md-4">
        <div class="card-group">
            <div class="card p-4" style="background:#ffffffd8">
                <div class="card-body">
                    <center><img src="assets/img/logo.png" alt="" style="height:100px;width:auto;">
                        <h2>Primer Koperasi</h2>
                        <p class="text-muted">Masuk untuk menggunakan sistem</p>
                    </center>
                    <?= view('auth/_message_block') ?>

                    <?php if ($config->validFields === ['email']) : ?>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend"><span class="input-group-text">
                                <svg class="c-icon">
                                    <use xlink:href="vendors/@coreui/icons/svg/free.svg#cil-user"></use>
                                </svg></span>
                        </div>
                        <input name="login"
                            class="form-control <?php if (session('errors.login')) : ?>is-invalid<?php endif ?>"
                            type="email" placeholder="<?= lang('Auth.email') ?>" value="<?= old('email') ?>">
                        <div class="invalid-feedback">
                            <?= session('errors.login') ?>
                        </div>
                    </div>
                    <?php else : ?>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend"><span class="input-group-text">
                                <svg class="c-icon">
                                    <use xlink:href="vendors/@coreui/icons/svg/free.svg#cil-user"></use>
                                </svg></span>
                        </div>
                        <input name="login"
                            class="form-control <?php if (session('errors.login')) : ?>is-invalid<?php endif ?>"
                            type="text" placeholder="<?= lang('Auth.emailOrUsername') ?>" value="<?= old('login') ?>">
                        <div class="invalid-feedback">
                            <?= session('errors.login') ?>
                        </div>
                    </div>
                    <?php endif; ?>
                    <div class="input-group mb-2">
                        <div class="input-group-prepend"><span class="input-group-text">
                                <svg class="c-icon">
                                    <use xlink:href="vendors/@coreui/icons/svg/free.svg#cil-lock-locked"></use>
                                </svg></span>
                        </div>
                        <input name="password"
                            class="form-control <?php if (session('errors.password')) : ?>is-invalid<?php endif ?>"
                            type="password" placeholder="<?= lang('Auth.password') ?>" value="<?= old('password') ?>">
                        <div class="invalid-feedback">
                            <?= session('errors.password') ?>
                        </div>
                    </div>
                    <?php if ($config->allowRemembering) : ?>
                    <div class="form-check mb-2">
                        <label class="form-check-label">
                            <input type="checkbox" name="remember" class="form-check-input"
                                <?php if (old('remember')) : ?> checked <?php endif ?>>
                            <?= lang('Auth.rememberMe') ?>
                        </label>
                    </div>
                    <?php endif; ?>
                    <div class="row">
                        <div class="col-6">
                            <button class="btn btn-primary px-4" type="submit"><?= lang('Auth.loginAction') ?></button>
                        </div>
                        <div class="col-6 text-right">
                            <a href="forgot" class="btn btn-link px-0"
                                type="button"><?= lang('Auth.forgotYourPassword') ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<?= $this->endSection('content'); ?>