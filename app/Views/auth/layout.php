<!DOCTYPE html>
<html lang="en">

<head>
    <base href="<?= base_url(); ?>/">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Login | Primer Koperasi</title>
    <link rel="icon" type="image/png" href="favicon.ico">
    <link rel="manifest" href="assets/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="assets/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#000">
    <!-- Main styles for this application-->
    <link href="css/style.css" rel="stylesheet">
</head>

<body class="c-app flex-row align-items-center" id="line-bg">
    <div class="container">
        <?= $this->renderSection('content'); ?>
    </div>
    <!-- CoreUI and necessary plugins-->
    <script src="vendors/@coreui/coreui/js/coreui.bundle.min.js"></script>
    <!--[if IE]><!-->
    <script src="vendors/@coreui/icons/js/svgxuse.min.js"></script>
    <!--<![endif]-->
</body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/three.js/r121/three.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vanta@latest/dist/vanta.net.min.js"></script>
<script>
VANTA.NET({
    el: "#line-bg",
    mouseControls: true,
    touchControls: true,
    gyroControls: false,
    minHeight: 400.00,
    minWidth: 400.00,
    scale: 1.00,
    scaleMobile: 1.00,
    color: 0x41f2ef,
    backgroundColor: 0x2b1d45,
    points: 6.00,
    maxDistance: 18.00,
    spacing: 12.00
})
</script>

</html>