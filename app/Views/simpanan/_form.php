<form action="<?= base_url('master/simpanan/save') ?>" class="ajax-multipart modal-content" data-respond="reload">
    <?php if (@$id) : ?>
    <input type="hidden" name="id" value="<?= $id ?>">
    <?php endif; ?>
    <div class="modal-header">
        <h4 class="modal-title"><?= (!@$id ? 'Tambah' : '') ?> Data Simpanan</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <label for="nama_simpanan" class="mb-1">Nama Simpanan</label>
                <input type="text" name="nama_simpanan" id="nama_simpanan" class="form-control"
                    value="<?= @$nama_simpanan ?>">
                <div class="invalid-feedback"></div>
            </div>
            <div class="col-md-12">
                <label for="total" class="mb-1">Total Besar Simpanan</label>
                <input type="number" name="total" id="total" class="form-control" value="<?= @$total ?>">
                <div class="invalid-feedback"></div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn btn-danger" type="button" data-dismiss="modal">Close</button>
        <button class="btn btn-success float-right" type="submit">Simpan</button>
    </div>
</form>