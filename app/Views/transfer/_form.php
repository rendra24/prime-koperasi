<form action="<?= base_url('transfer/save') ?>" class="ajax-multipart modal-content" data-respond="reload">
    <div class="modal-header">
        <h4 class="modal-title"><?= (!@$id ? 'Tambah' : '') ?> Transfer Kas</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <label for="id_asset_dari" class="mb-1">Dari Asset</label>
                <select name="id_asset_dari" id="id_asset_dari" class="form-control">
                    <option value="">Pilih Asset</option>
                    <?php foreach($asset as $row): ?>
                    <option value="<?= $row['id']; ?>"><?= $row['nama_asset']; ?>
                        (<?= FormatDecimals($row['total']); ?>)</option>
                    <?php endforeach; ?>
                </select>
                <div class="invalid-feedback"></div>
            </div>

            <div class="col-md-12">
                <label for="id_asset_ke" class="mb-1">Ke Asset</label>
                <select name="id_asset_ke" id="id_asset_ke" class="form-control">
                    <option value="">Pilih Asset</option>
                    <?php foreach($asset as $row): ?>
                    <option value="<?= $row['id']; ?>"><?= $row['nama_asset']; ?>
                        (<?= FormatDecimals($row['total']); ?>)</option>
                    <?php endforeach; ?>
                </select>
                <div class="invalid-feedback"></div>
            </div>
            <div class="col-md-12">
                <label>Total Besaran</label>
                <input type="number" class="form-control" id="total" name="total">
            </div>
            <div class="col-md-12">
                <label>Keterangan</label>
                <textarea name="keterangan" id="keterangan" cols="20" rows="10" class="form-control"></textarea>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn btn-danger" type="button" data-dismiss="modal">Close</button>
        <button class="btn btn-success float-right" type="submit">Simpan</button>
    </div>
</form>