<form action="<?= base_url('master/asset/save') ?>" class="ajax-multipart modal-content" data-respond="reload">
    <?php if (@$id) : ?>
    <input type="hidden" name="id" value="<?= $id ?>">
    <?php endif; ?>
    <input type="hidden" name="flag" value="1">
    <div class="modal-header">
        <h4 class="modal-title"><?= (!@$id ? 'Tambah' : '') ?> Data Asset</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <label for="nama_asset" class="mb-1">Nama Asset</label>
                <input type="text" name="nama_asset" id="nama_asset" class="form-control" value="<?= @$nama_asset ?>">
                <div class="invalid-feedback"></div>
            </div>
            <div class="col-md-12">
                <label for="id_jenis_asset" class="mb-1">Jenis Asset</label>
                <select name="id_jenis_asset" id="id_jenis_asset" class="form-control">
                    <option value="">Pilih Jenis Asset</option>
                    <option value="1">Cash</option>
                    <option value="2">Bank</option>
                </select>
                <div class="invalid-feedback"></div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn btn-danger" type="button" data-dismiss="modal">Close</button>
        <button class="btn btn-success float-right" type="submit">Simpan</button>
    </div>
</form>