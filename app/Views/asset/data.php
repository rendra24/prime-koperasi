<?= $this->extend('general_layout'); ?>
<?= $this->section('breadcrumb'); ?>
<ol class="breadcrumb border-0 m-0">
    <li class="breadcrumb-item active">Asset</li>
</ol>
<?= $this->endSection('breadcrumb'); ?>
<?= $this->section('main'); ?>
<div class="container-fluid">
    <div class="fade-in">
        <div class="card">
            <div class="card-header">
                <h5 class="card-title mb-0">Data Asset</h5>
            </div>
            <div class="card-body mb-2">
                <div class="float-right mb-2">
                    <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modalSide"
                        data-page="master/asset/form">
                        <div class="c-icon mr-1 cil-plus"></div>
                        <span>Tambah Asset</span>
                    </button>
                </div>
                <div class="table-responsive">
                    <table id="tableAjax" class="tableAjax table table-bordered table-striped w-100"
                        data-source="<?= base_url('master/asset/data'); ?>">
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection('main'); ?>