<li class="c-sidebar-nav-item">
    <a class="c-sidebar-nav-link <?= ($menu == 'dashboard' ? 'c-active' : '') ?>" href="<?= base_url('dashboard'); ?>">
        <div class="c-sidebar-nav-icon">
            <i class="c-icon cil-speedometer"></i>
        </div>
        Dashboard
    </a>
</li>
<?php if (has_permission([1, 3, 5, 7])) : ?>
<li class="c-sidebar-nav-item c-sidebar-nav-dropdown <?= ($menu == 'users' ? 'c-show' : '') ?>">
    <a class="c-sidebar-nav-link c-sidebar-nav-dropdown-toggle <?= ($menu == 'users' ? 'c-active' : '') ?>"
        href="#users">
        <div class="c-sidebar-nav-icon">
            <i class="c-icon cil-user"></i>
        </div>
        Manajemen User/Petugas
    </a>
    <ul class="c-sidebar-nav-dropdown-items">
        <?php if (has_permission(1)) : ?>
        <li class="c-sidebar-nav-item"><a
                class="c-sidebar-nav-link <?= (@$submenu == 'permission' ? 'c-active' : '') ?>"
                href="<?= base_url('users/permission'); ?>"> Hak Akses</a></li>
        <?php endif; ?>
        <?php if (has_permission(3)) : ?>
        <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link <?= (@$submenu == 'admin' ? 'c-active' : '') ?>"
                href="<?= base_url('users/admin'); ?>"> Admin</a></li>
        <?php endif; ?>
        <?php if (has_permission(5)) : ?>
        <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link <?= (@$submenu == 'vendor' ? 'c-active' : '') ?>"
                href="<?= base_url('users/vendor'); ?>"> Penyedia / Vendor</a></li>
        <?php endif; ?>
        <?php if (has_permission(7)) : ?>
        <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link <?= (@$submenu == 'custommer' ? 'c-active' : '') ?>"
                href="<?= base_url('users/customer'); ?>"> Member</a></li>
        <?php endif; ?>
    </ul>
</li>
<?php endif; ?>
<li class="c-sidebar-nav-item c-sidebar-nav-dropdown <?= ($menu == 'master' ? 'c-show' : '') ?>">
    <a class="c-sidebar-nav-link c-sidebar-nav-dropdown-toggle <?= ($menu == 'master' ? 'c-active' : '') ?>"
        href="#users">
        <div class="c-sidebar-nav-icon">
            <i class="c-icon cil-user"></i>
        </div>
        Master Data
    </a>
    <ul class="c-sidebar-nav-dropdown-items">
        <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link <?= (@$submenu == 'anggota' ? 'c-active' : '') ?>"
                href="<?= base_url('master/anggota'); ?>"> Anggota</a>
        </li>
        <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link <?= (@$submenu == 'simpanan' ? 'c-active' : '') ?>"
                href="<?= base_url('master/simpanan'); ?>"> Jenis Simpanan</a>
        </li>
        <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link <?= (@$submenu == 'pinjaman' ? 'c-active' : '') ?>"
                href="<?= base_url('master/pinjaman'); ?>"> Jenis Pinjaman</a>
        </li>
        <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link <?= (@$submenu == 'angsuran' ? 'c-active' : '') ?>"
                href="<?= base_url('master/angsuran'); ?>"> Lama Angsuran</a>
        </li>
        <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link <?= (@$submenu == 'asset' ? 'c-active' : '') ?>"
                href="<?= base_url('master/asset'); ?>"> Asset</a>
        </li>

    </ul>
</li>
<li class="c-sidebar-nav-item">
    <a class="c-sidebar-nav-link <?= ($menu == 'pinjaman' ? 'c-active' : '') ?>" href="<?= base_url('pinjaman'); ?>">
        <div class="c-sidebar-nav-icon">
            <i class="c-icon cil-bank"></i>
        </div>
        Pinjaman
    </a>
</li>
<li class="c-sidebar-nav-item">
    <a class="c-sidebar-nav-link <?= ($menu == 'simpanan' ? 'c-active' : '') ?>" href="<?= base_url('simpanan'); ?>">
        <div class="c-sidebar-nav-icon">
            <i class="c-icon cil-money"></i>
        </div>
        Simpanan
    </a>
</li>
<li class="c-sidebar-nav-item">
    <a class="c-sidebar-nav-link <?= ($menu == 'aruskas' ? 'c-active' : '') ?>" href="<?= base_url('aruskas'); ?>">
        <div class="c-sidebar-nav-icon">
            <i class="c-icon cil-bar-chart"></i>
        </div>
        Arus Kas
    </a>
</li>
<li class="c-sidebar-nav-item">
    <a class="c-sidebar-nav-link <?= ($menu == 'transfer' ? 'c-active' : '') ?>" href="<?= base_url('transfer'); ?>">
        <div class="c-sidebar-nav-icon">
            <i class="c-icon cil-transfer"></i>
        </div>
        Transfer Kas
    </a>
</li>
<li class="c-sidebar-nav-item">
    <a class="c-sidebar-nav-link <?= ($menu == 'transaksi' ? 'c-active' : '') ?>" href="<?= base_url('transaksi'); ?>">
        <div class="c-sidebar-nav-icon">
            <i class="c-icon cil-cart"></i>
        </div>
        Transaksi
    </a>
</li>
<?php if (has_permission(9)) : ?>
<li class="c-sidebar-nav-item">
    <a class="c-sidebar-nav-link <?= ($menu == 'chat' ? 'c-active' : '') ?>" href="<?= base_url('chat'); ?>">
        <div class="c-sidebar-nav-icon">
            <i class="c-icon cil-chat-bubble"></i>
        </div>
        Obrolan
        <span class="badge badge-info notif-chatmenu"><?= countChat() ?></span>
    </a>
</li>
<?php endif; ?>
<?php //if (has_permission([21, 22])) : ?>
<li class="c-sidebar-nav-item c-sidebar-nav-dropdown <?= ($menu == 'report' ? 'c-show' : '') ?>">
    <a class="c-sidebar-nav-link c-sidebar-nav-dropdown-toggle <?= ($menu == 'report' ? 'c-active' : '') ?>"
        href="#report">
        <div class="c-sidebar-nav-icon">
            <i class="c-icon cil-spreadsheet"></i>
        </div>
        Laporan
    </a>
    <ul class="c-sidebar-nav-dropdown-items">
        <?php //if (has_permission(21)) : ?>
        <li class="c-sidebar-nav-item"><a
                class="c-sidebar-nav-link <?= (@$submenu == 'transaction' ? 'c-active' : '') ?>"
                href="<?= base_url('laporan/transaksi'); ?>"> Transaksi</a></li>
        <?php //endif; ?>
        <?php //if (has_permission(22)) : ?>
        <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link <?= (@$submenu == 'income' ? 'c-active' : '') ?>"
                href="<?= base_url('laporan/anggota'); ?>"> Anggota</a></li>
        <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link <?= (@$submenu == 'ppob' ? 'c-active' : '') ?>"
                href="<?= base_url('laporan/ppob'); ?>"> PPOB</a></li>
        <?php //endif; ?>
    </ul>
</li>
<?php //endif; ?>