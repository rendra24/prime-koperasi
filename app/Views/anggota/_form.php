<form action="<?= base_url('master/anggota/save') ?>" class="ajax-multipart modal-content" data-respond="reload">
    <?php if (@$id) : ?>
    <input type="hidden" name="id" value="<?= $id ?>">
    <?php endif; ?>
    <div class="modal-header">
        <h4 class="modal-title"><?= (!@$id ? 'Tambah' : '') ?> Data Menu</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <label for="nama" class="mb-1">Kode Anggota</label>
                <input type="text" name="kode_anggota" id="kode_anggota" class="form-control"
                    value="<?= @$kode_anggota ?>" readonly>
                <div class="invalid-feedback"></div>
            </div>
            <div class="col-md-12">
                <label for="nama" class="mb-1">NRP/NIP</label>
                <input type="text" name="nrp" id="nrp" class="form-control" value="<?= @$nrp ?>">
                <div class="invalid-feedback"></div>
            </div>
            <div class="col-md-12">
                <label for="nama" class="mb-1">Nama</label>
                <input type="text" name="nama" id="nama" class="form-control" value="<?= @$nama ?>">
                <div class="invalid-feedback"></div>
            </div>
            <div class="col-md-12">
                <label for="pangkat" class="mb-1">Pangkat</label>
                <input type="text" name="pangkat" id="pangkat" class="form-control" value="<?= @$pangkat ?>"
                    style="text-transform: uppercase;">
                <div class="invalid-feedback"></div>
            </div>
            <div class="col-md-12">
                <label for="jabatan" class="mb-1">Jabatan</label>
                <input type="text" name="jabatan" id="jabatan" class="form-control" value="<?= @$jabatan ?>">
                <div class="invalid-feedback"></div>
            </div>
            <div class="col-md-12">
                <label for="jabatan" class="mb-1">Fungsi</label>
                <input type="text" name="fungsi" id="fungsi" class="form-control" value="<?= @$fungsi ?>">
                <div class="invalid-feedback"></div>
            </div>
            <div class="col-md-12">
                <label for="nama" class="mb-1">Email</label>
                <input type="text" name="email" id="email" class="form-control" value="<?= @$email ?>">
                <div class="invalid-feedback"></div>
            </div>
            <?php if(!isset($id)): ?>
            <div class="col-md-12">
                <label for="nama" class="mb-1">Password</label>
                <input type="password" name="password" id="password" class="form-control" value="">
                <div class="invalid-feedback"></div>
            </div>
            <?php endif; ?>
            <div class="col-md-12">
                <label for="nama" class="mb-1">alamat</label>
                <textarea name="alamat" id="alamat" class="form-control"><?= @$alamat ?></textarea>
                <div class="invalid-feedback"></div>
            </div>
            <div class="col-md-12">
                <label>Publish</label> <br>
                <label class="c-switch c-switch-pill c-switch-lg c-switch-success">
                    <input name="flag" class="c-switch-input" type="checkbox" value="1"
                        <?php if(isset($flag)){ if($flag == 1){ echo "checked";} } ?>><span
                        class="c-switch-slider"></span>
                </label>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn btn-danger" type="button" data-dismiss="modal">Close</button>
        <button class="btn btn-success float-right" type="submit">Simpan</button>
    </div>
</form>