<div class="modal-header">
    <h4 class="modal-title">Import Data Anggota Excel</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
            <form method="post" action="<?= base_url('master/anggota/prosesExcel') ?>"
                class="ajax-multipart modal-content" data-respond="reload">
                <div class="form-group">
                    <label>File Excel</label>
                    <input type="file" name="fileexcel" class="form-control" id="file" required accept=".xls, .xlsx" />
                    </p>
                </div>
                <div class="form-group">
                    <button class="btn btn-primary" type="submit">Upload</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button class="btn btn-danger" type="button" data-dismiss="modal">Close</button>
</div>