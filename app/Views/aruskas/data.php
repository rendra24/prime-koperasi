<?= $this->extend('general_layout'); ?>
<?= $this->section('breadcrumb'); ?>
<ol class="breadcrumb border-0 m-0">
    <li class="breadcrumb-item active">Aruskas</li>
</ol>
<?= $this->endSection('breadcrumb'); ?>
<?= $this->section('main'); ?>
<div class="container-fluid">
    <div class="fade-in">
        <div class="card">
            <div class="card-header">
                <h5 class="card-title mb-0">Data Aruskas</h5>
            </div>
            <div class="card-body mb-2">
                <form action="" method="GET" class="mb-3">
                    <div class="row">
                        <div class="col-md-3">
                            <label for="">Tanggal Awal</label>
                            <input type="date" class="form-control" name="start_date"
                                value="<?= (isset($request['start_date'])) ? $request['start_date'] : ''; ?>">
                        </div>
                        <div class="col-md-3">
                            <label for="">Tanggal Akhir</label>
                            <input type="date" class="form-control" name="end_date"
                                value="<?= (isset($request['end_date'])) ? $request['end_date'] : ''; ?>">
                        </div>
                        <div class="col-md-2">
                            <br>
                            <button type="submit" class="btn btn-primary mt-2 btn-block">Cari</button>
                        </div>
                    </div>
                </form>
                <hr>

                <div class="float-right mb-2">
                    <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modalSide"
                        data-page="aruskas/form">
                        <div class="c-icon mr-1 cil-plus"></div>
                        <span>Tambah Transaksi</span>
                    </button>
                </div>
                <div class="table-responsive">
                    <table id="tableAjax" class="tableAjax table table-bordered table-striped w-100"
                        data-source="<?= (!isset($request)) ? base_url('aruskas/data') : base_url("aruskas/data?start_date={$request['start_date']}&end_date={$request['end_date']}"); ?>">
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection('main'); ?>