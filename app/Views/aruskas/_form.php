<form action="<?= base_url('aruskas/save') ?>" class="ajax-multipart modal-content" data-respond="reload">
    <?php if (@$id) : ?>
    <input type="hidden" name="id" value="<?= $id ?>">
    <?php endif; ?>
    <input type="hidden" name="flag" value="1">
    <div class="modal-header">
        <h4 class="modal-title"><?= (!@$id ? 'Tambah' : '') ?> Data Kas</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <label for="status_transaksi" class="mb-1">Jenis Kas</label>
                <select name="status_transaksi" id="status_transaksi" class="form-control">
                    <option value="">Pilih Jenis Kas</option>
                    <option value="1">Masuk</option>
                    <option value="2">Keluar</option>
                </select>
                <div class="invalid-feedback"></div>
            </div>
            <div class="col-md-12">
                <label for="id_asset" class="mb-1">Jenis Asset</label>
                <select name="id_asset" id="id_asset" class="form-control">
                    <option value="">Pilih Asset</option>
                    <?php foreach($asset as $row): ?>
                    <option value="<?= $row['id']; ?>"><?= $row['nama_asset']; ?>
                        (<?= formatDecimals($row['total']); ?>)</option>
                    <?php endforeach; ?>
                </select>
                <div class="invalid-feedback"></div>
            </div>
            <div class="col-md-12">
                <label>Total Besaran</label>
                <input type="number" class="form-control" id="total" name="total">
                <div class="invalid-feedback"></div>
            </div>
            <div class="col-md-12">
                <label>Keterangan</label>
                <textarea name="keterangan" id="keterangan" cols="20" rows="10" class="form-control"></textarea>
                <div class="invalid-feedback"></div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn btn-danger" type="button" data-dismiss="modal">Close</button>
        <button class="btn btn-success float-right" type="submit">Simpan</button>
    </div>
</form>