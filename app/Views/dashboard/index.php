<?= $this->extend('general_layout'); ?>
<?= $this->section('breadcrumb'); ?>
<ol class="breadcrumb border-0 m-0">
    <li class="breadcrumb-item">Home</li>
    <li class="breadcrumb-item active">Dashboard</li>
</ol>
<?= $this->endSection('breadcrumb'); ?>
<?= $this->section('main'); ?>
<div class="container-fluid">
    <div class="fade-in">
        <div class="row">
            <div class="col-12 col-md-9">
                <div class="row">
                    <div class="col-12 col-md-4">
                        <div class="card text-white bg-primary">
                            <div class="card-body card-body pb-0 d-flex justify-content-between align-items-start">
                                <div>
                                    <div class="text-value-lg"><?= formatRupiah($total_kas); ?></div>
                                    <div>Total Kas</div>
                                </div>
                            </div>
                            <div class="c-chart-wrapper mt-3 mx-3" style="height:70px;">
                                <canvas class="chart" id="card-chart1" height="70"></canvas>
                            </div>
                        </div>
                    </div>
                    <!-- /.col-->
                    <div class="col-12 col-md-4">
                        <div class="card text-white bg-info">
                            <div class="card-body card-body pb-0 d-flex justify-content-between align-items-start">
                                <div>
                                    <div class="text-value-lg"><?= formatRupiah($total_simpanan); ?></div>
                                    <div>Tabungan</div>
                                </div>
                            </div>
                            <div class="c-chart-wrapper mt-3 mx-3" style="height:70px;">
                                <canvas class="chart" id="card-chart2" height="70"></canvas>
                            </div>
                        </div>
                    </div>
                    <!-- /.col-->
                    <div class="col-12 col-md-4">
                        <div class="card text-white bg-warning">
                            <div class="card-body card-body pb-0 d-flex justify-content-between align-items-start">
                                <div>
                                    <div class="text-value-lg"><?= formatRupiah($total_pinjaman); ?></div>
                                    <div>Pinjaman</div>
                                </div>
                            </div>
                            <div class="c-chart-wrapper mt-3 mx-3" style="height:70px;">
                                <!-- <div class="loading-center" height="70">
									<div class="spinner-border" role="status">
										<span class="sr-only">Loading...</span>
									</div>
								</div> -->
                                <canvas class="chart" id="card-chart3" height="70"></canvas>
                            </div>
                        </div>
                    </div>
                    <!-- /.col-->
                </div>
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-between">
                            <div>
                                <h4 class="card-title mb-0">Grafik Pendapatan</h4>
                                <div class="small text-muted">Tahun <?= date('Y') ?></div>
                            </div>
                        </div>
                        <div class="c-chart-wrapper" style="height:300px;margin-top:40px;">
                            <canvas class="chart" id="chart-pendapatan" height="300"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-3">
                <div class="card">
                    <div class="card-body">
                        <h3 class="mb-0">Transaksi</h3>
                        <p><b>Transaksi</b> terbaru</p>
                        <hr>
                        <div class="row">
                            <?php foreach($transaksi as $row): ?>
                            <div class="col-12 mb-2">
                                <div
                                    class="text-uppercase font-weight-bold  <?= ($row['status_transaksi'] == '1') ? 'text-success' : 'text-danger'; ?>">
                                    <?= formatdecimals($row['total']); ?></div>
                                <div class="text-uppercase text-muted small"><?= formatTglIndo($row['created_at']); ?>
                                </div>
                            </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <h3 class="mb-0">5 Top Anggota</h3>
                        <p><b>Anggota</b> bulan ini</p>
                        <hr>
                        <div class="row">
                            <?php foreach($top_anggota as $row): ?>
                            <div class="col-12 mb-2">
                                <div class="text-uppercase font-weight-bold"><?= $row['nama']; ?></div>
                                <div class="text-uppercase small text-success">
                                    <?= formatRupiah($row['total']); ?></div>
                            </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row-->
        </div>
    </div>
    <?= $this->endSection('main'); ?>