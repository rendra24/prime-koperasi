<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <style>
    body {
        width: 100%;
        height: 100%;
        margin: 0;
        padding: 0;
        background-color: #FAFAFA;
        font: 12pt "arial";
    }

    * {
        box-sizing: border-box;
        -moz-box-sizing: border-box;
    }

    .page {
        width: 210mm;
        min-height: 297mm;
        padding: 20mm;
        margin: 10mm auto;
        border: 1px #D3D3D3 solid;
        border-radius: 5px;
        background: white;
        box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
    }

    .subpage {
        padding: 1mm;
        border: 5px white solid;
        height: 257mm;
        outline: 2cm white solid;
    }

    @page {
        size: A4;
        margin: 0;
    }

    @media print {

        html,
        body {
            width: 210mm;
            height: 297mm;
        }

        .page {
            margin: 0;
            border: initial;
            border-radius: initial;
            width: initial;
            min-height: initial;
            box-shadow: initial;
            background: initial;
            page-break-after: always;
        }

        button {
            display: none;
        }
    }
    </style>
    <title>Perjanjian Pinjaman</title>
</head>

<body style="background:#DDD;" onafterprint="myFunction()">
    <div class="book">
        <div class="page">
            <div class="subpage">

                <table style="width:100%;border-bottom:2px solid #000;">
                    <tr>
                        <td><img src="<?= base_url('assets/img/logo-koperasi.png') ?>" alt="" style="width:2.7cm;"></td>
                        <td style="text-align:center;">
                            <strong>
                                PRIMER KOPERASI KEPOLISIAN RESOR BATU <br>
                                (PRIMKOPPOL) <br>
                            </strong>
                            <p style="margin:0;font-size:12px;">
                                BH. NOMOR: 518/BH/XVI,38/422.402/2007 TANGGAL 24 SEPTEMBER 2007 <br>
                                Jl. Trunojoyo No. 9D Junrejo Kota Batu 65321 <br>
                            </p>
                        </td>
                    </tr>
                </table>

                <center>
                    <h5>FORMULIR PERMOHONAN PEMINJAMAN</h5>
                </center>

                <table style="width:100%;font-size:12px;">
                    <tr>
                        <td colspan="3">Dengan Hormat, <br> Yang bertanda tangan di bawa ini : <br><br></td>
                    </tr>
                    <tr>
                        <td>Nama</td>
                        <td>:</td>
                        <td><?= $nama_anggota; ?></td>
                    </tr>
                    <tr>
                        <td>Pangkat/ NRP/NIP</td>
                        <td>:</td>
                        <td><?= $anggota['pangkat']; ?> / <?= $anggota['nrp']; ?></td>
                    </tr>
                    <tr>
                        <td>Jabatan</td>
                        <td>:</td>
                        <td><?= $anggota['jabatan']; ?></td>
                    </tr>
                    <tr>
                        <td>Fungsi</td>
                        <td>:</td>
                        <td><?= $anggota['fungsi']; ?></td>
                    </tr>
                    <tr>
                        <td>No. HP/ Telp</td>
                        <td>:</td>
                        <td><?= $anggota['telp']; ?></td>
                    </tr>

                    <tr>
                        <td colspan="3"><br>Dengan ini mengajukan permohonan peminjaman uang/ barang di Primkoppol Resor
                            Batu <br><br></td>
                    </tr>

                </table>

                <table border="1" style="border-color:#000;width:100%;border-collapse:collapse;font-size:12px;">
                    <tr>
                        <td>1.</td>
                        <td>Besar Pinjaman</td>
                        <td><?= formatRupiah($total_pinjaman); ?></td>
                    </tr>
                    <td>2.</td>
                    <td>Berupa uang / barang</td>
                    <td><?= formatRupiah($total_pinjaman); ?></td>
                    </tr>
                    <td>3.</td>
                    <td>Lamanya angsuran <?= $angsuran; ?>X ( Bulan )</td>
                    <td>Besar jasa pinjaman <?= $bunga; ?> %</td>
                    </tr>
                    <td>4.</td>
                    <td>Sampai dengan Bulan :</td>
                    <td><?= FullFormatTgl(formatTanggalCount($created_at, $angsuran, 'month')); ?></td>
                    </tr>
                    <td>5.</td>
                    <td>Besar jumlah angsuran/ bulan</td>
                    <td><?= formatRupiah($cicilan); ?></td>
                    </tr>
                </table>
                <br>
                <table style="font-size:12px;">
                    <tr>
                        <td>
                            Demikian formulir permohonan pinjaman uang/barang ini saya isi dengan sebenar-benarnya,
                            serta menyatakan bahwa angsuran pinjaman perbulan bersedia di potong oleh bendahara gaji (
                            SIKEU ) Polres Batu dan apabila ada kesalahan didalam mengisi formulir peminjaman ini saya
                            bersedia menerima konsekuensi dari pengurus PRIMKOPPOLRES BATU.
                        </td>
                    </tr>
                </table>


                <br><br>
                <p style="text-align:right;font-size:13px;">Batu,<?= date('d-m-Y') ?></p>
                <table style="width:100%;font-size:11px;border-collapse: collapse;" border="1">
                    <tr>
                        <td colspan="6" height="20" style=" text-align:center;">KOLOM PERSETUJUAN</td>
                    </tr>
                    <tr>
                        <td style="text-align:center;width:20%;">PEMOHONN</td>
                        <td style="text-align:center;width:20%;">ISTRI/SUAMI</td>
                        <td style="text-align:center;width:20%;">KASI KEU</td>
                        <td style="text-align:center;width:20%;">WAKAPOLRES</td>
                        <td style="text-align:center;width:20%;">KAPRIM</td>
                        <td style="text-align:center;width:20%;">BENDAHARA</td>

                    </tr>
                    <tr>
                        <td height="100"></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>

                </table>
                <br><br>
                <p style="font-size:12px;">
                    Catatan : <br>
                    1. Menyertakan foto copy KTP 2 lembar <br>
                    2.Menyertakan foto copy No Rek buku tabungan gaji terakir <br>
                    3.Tidak mempunyai tanggungan ditempat lain melebihi 40% dari gaji <br>
                    4.Info lebih lanjut hubungi Sekertaris Primkoopol Telp. 082210184801
                </p>

            </div>
        </div>

        <!-- <div class="page">
            <div class="subpage">Page 2/2</div>
        </div> -->

    </div>

    <div style="position:fixed;bottom:30px;right:15px;">
        <button type="button" onclick="window.print();"
            style="width:200px;height:45px;border:none;background:#28a745;color:#FFF;font-weight:bold;text-align:center;border-radius:5px;padding: 10px 15px;margin-bottom:20px;cursor:pointer;">Cetak
            Dokumen</button><br>
        <button type="button" onclick="window.location.href='<?= base_url('pinjaman'); ?>';"
            style="width:200px;height:44px;background:#FFF;color:#3D405B;font-weight:bold;text-decoration:none;text-align:center;border-radius:5px;padding: 10px 15px;border:2px solid #3D405B;cursor:pointer;">Batal</button>
    </div>
</body>
<script>
function myFunction() {
    window.location.href = '<?= base_url('pinjaman') ?>';
}
</script>

</html>
<!-- <script type="text/javascript">window.print();</script> -->