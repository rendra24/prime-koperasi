<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <style>
    body {
        width: 100%;
        height: 100%;
        margin: 0;
        padding: 0;
        background-color: #FAFAFA;
        font: 12pt "arial";
    }

    * {
        box-sizing: border-box;
        -moz-box-sizing: border-box;
    }

    .page {
        width: 210mm;
        min-height: 297mm;
        padding: 20mm;
        margin: 10mm auto;
        border: 1px #D3D3D3 solid;
        border-radius: 5px;
        background: white;
        box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
    }

    .subpage {
        padding: 1mm;
        border: 5px white solid;
        height: 257mm;
        outline: 2cm white solid;
    }

    @page {
        size: A4;
        margin: 0;
    }

    @media print {

        html,
        body {
            width: 210mm;
            height: 297mm;
        }

        .page {
            margin: 0;
            border: initial;
            border-radius: initial;
            width: initial;
            min-height: initial;
            box-shadow: initial;
            background: initial;
            page-break-after: always;
        }

        button {
            display: none;
        }
    }
    </style>
    <title>Perjanjian Pinjaman</title>
</head>

<body style="background:#DDD;" onafterprint="myFunction()">
    <div class="book">
        <div class="page">
            <div class="subpage">

                <center>
                    <h2>SURAT PERJANJIAN PINJAMAN</h2>
                </center>

                <table>
                    <tr>
                        <td colspan="3">Yang bertanda tangan di bawah ini : <br><br></td>
                    </tr>
                    <tr>
                        <td>Nama</td>
                        <td>:</td>
                        <td><?= $nama_anggota; ?></td>
                    </tr>
                    <tr>
                        <td>Umur</td>
                        <td>:</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Pekerjaan</td>
                        <td>:</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Nomor KTP</td>
                        <td>:</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Alamat</td>
                        <td>:</td>
                        <td> </td>
                    </tr>

                    <tr>
                        <td colspan="3"><br>Untuk Selanjutnya di sebut dengan Pihak Pertama <br><br></td>
                    </tr>

                    <tr>
                        <td>Nama</td>
                        <td>:</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Umur</td>
                        <td>:</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Pekerjaan</td>
                        <td>:</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Nomor KTP</td>
                        <td>:</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Alamat</td>
                        <td>:</td>
                        <td></td>
                    </tr>

                    <tr>
                        <td colspan="3"><br>Yang mana disebut Pihak Kedua<br><br></td>
                    </tr>
                    <tr>
                        <td colspan="3">Yang mana kedua belah pihak sepakat untuk mengadakan hutang piutang. Maka
                            melalui surat perjanjian ini merupakan bukti bahwa kedua belah pihak mengadakan hutang
                            piutang dan juga telah menyetujui ketentuan - ketentuan sebagai berikut :</td>
                    </tr>
                    <tr>
                        <td>1.</td>
                        <td colspan="2">Pihak Pertama meminjamkan uang tunai kepada Pihak Kedua sebesar
                            <?= formatRupiah($total_pinjaman); ?>
                            (<?= terbilang($total_pinjaman); ?>)</td>
                    </tr>
                    <tr>
                        <td>2.</td>
                        <td colspan="2">Pihak Kedua akan menggembalikan uang kepada Pihak Pertama menggunakan bunga
                            sebesar 0,9% dengan tenggang waktu selama 10 bulan terhitung dari penandatanganan Surat
                            Perjanjian ini.</td>
                    </tr>
                    <tr>
                        <td colspan="3">Surat ini dibuat rangkap dua yang ditandatangani oleh kedua belah pihak diatas
                            materai yang berkekuatan cukup.</td>
                    </tr>
                    <tr>
                        <td colspan="3">Demikian surat perjanjian hutang piutang ini dibuat oleh kedua belah pihak tanpa
                            ada paksaan dari pihak manapun.</td>
                    </tr>
                    <tr>
                        <td colspan="3" style="text-align:right;"><br><br>Malang, <?= date('d-m-Y'); ?></td>
                    </tr>
                </table>
                <br><br>
                <table style="width:100%;">
                    <tr>
                        <td style="text-align:center;">Pihak Pertama</td>
                        <td style="text-align:center;">Pihak Kedua</td>
                    </tr>
                    <tr>
                        <td style="text-align:center;"></td>
                        <td style="text-align:center;"><br><br><br><br><br><strong><?= $nama_anggota; ?></strong></td>
                    </tr>
                </table>

            </div>
        </div>

        <!-- <div class="page">
            <div class="subpage">Page 2/2</div>
        </div> -->

    </div>

    <div style="position:fixed;bottom:30px;right:15px;">
        <button type="button" onclick="window.print();"
            style="width:200px;height:45px;border:none;background:#28a745;color:#FFF;font-weight:bold;text-align:center;border-radius:5px;padding: 10px 15px;margin-bottom:20px;cursor:pointer;">Cetak
            Dokumen</button><br>
        <button type="button" onclick="window.location.href='<?= base_url('pinjaman'); ?>';"
            style="width:200px;height:44px;background:#FFF;color:#3D405B;font-weight:bold;text-decoration:none;text-align:center;border-radius:5px;padding: 10px 15px;border:2px solid #3D405B;cursor:pointer;">Batal</button>
    </div>
</body>
<script>
function myFunction() {
    window.location.href = '<?= base_url('pinjaman') ?>';
}
</script>

</html>
<!-- <script type="text/javascript">window.print();</script> -->