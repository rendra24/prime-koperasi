<form action="<?= base_url('master/pinjaman/save') ?>" class="ajax-multipart modal-content" data-respond="reload">
    <?php if (@$id) : ?>
    <input type="hidden" name="id" value="<?= $id ?>">
    <?php endif; ?>"
    <input type="hidden" name="flag" value="1">
    <div class="modal-header">
        <h4 class="modal-title"><?= (!@$id ? 'Tambah' : '') ?> Data Pinjaman</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <label for="nama_pinjaman" class="mb-1">Nama Pinjaman</label>
                <input type="text" name="nama_pinjaman" id="nama_pinjaman" class="form-control"
                    value="<?= @$nama_pinjaman ?>">
                <div class="invalid-feedback"></div>
            </div>
            <div class="col-md-12">
                <label for="bunga" class="mb-1">Total Bunga</label>
                <input type="number" name="bunga" step=0.01 id="bunga" class="form-control" value="<?= @$bunga ?>">
                <div class="invalid-feedback"></div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn btn-danger" type="button" data-dismiss="modal">Close</button>
        <button class="btn btn-success float-right" type="submit">Simpan</button>
    </div>
</form>