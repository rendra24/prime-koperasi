<form action="<?= base_url('master/angsuran/save') ?>" class="ajax-multipart modal-content" data-respond="reload">
    <?php if (@$id) : ?>
    <input type="hidden" name="id" value="<?= $id ?>">
    <?php endif; ?>
    <input type="hidden" name="flag" value="1">
    <div class="modal-header">
        <h4 class="modal-title"><?= (!@$id ? 'Tambah' : '') ?> Data Angsuran</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <label for="nama_angsuran" class="mb-1">Nama Angsuran</label>
                <input type="text" name="nama_angsuran" id="nama_angsuran" class="form-control"
                    value="<?= @$nama_angsuran ?>">
                <div class="invalid-feedback"></div>
            </div>
            <div class="col-md-12">
                <label for="angsuran" class="mb-1">Total Lama Angsuran</label>
                <input type="number" name="angsuran" id="angsuran" class="form-control" value="<?= @$angsuran ?>">
                <div class="invalid-feedback"></div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn btn-danger" type="button" data-dismiss="modal">Close</button>
        <button class="btn btn-success float-right" type="submit">Simpan</button>
    </div>
</form>