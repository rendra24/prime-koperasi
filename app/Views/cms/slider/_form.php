<form action="<?= base_url('cms/slider/save') ?>" class="ajax-multipart modal-content">
    <?php if (@$id) : ?>
        <input type="hidden" name="id" value="<?= $id ?>">
    <?php endif; ?>
    <input type="hidden" name="old_img" value="<?= @$img ?>">

    <div class="modal-header">
        <h4 class="modal-title"><?= (!@$id ? 'Tambah' : '') ?> Data Slider</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <label for="judul" class="mb-1">Judul</label>
                <input type="text" name="judul" id="judul" class="form-control" value="<?= @$judul ?>">
                <div class="invalid-feedback"></div>
            </div>
            <div class="col-md-12 mt-2">
                <label for="slider" class="mb-1">Slider</label>
                <input type="file" name="img" id="slider" class="form-control-file">
                <div class="invalid-feedback"></div>
                <small id="gambarHelp" class="form-text text-muted">
                    Slider harus berukuran 2000px x 851px
                </small>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn btn-danger" type="button" data-dismiss="modal">Close</button>
        <button class="btn btn-success float-right" type="submit">Simpan</button>
    </div>
</form>