<form action="<?= base_url('cms/newsblog/save') ?>" class="ajax-multipart modal-content">
    <?php if (@$id) : ?>
        <input type="hidden" name="id" value="<?= $id ?>">
    <?php endif; ?>
    <input type="hidden" name="id_menu" value="4">
    <input type="hidden" name="old_file" value="<?= @$file ?>">

    <div class="modal-header">
        <h4 class="modal-title"><?= (!@$id ? 'Tambah' : '') ?> Data Berita dan Blog</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <label for="judul" class="mb-1">Judul</label>
                <input type="text" name="judul" id="judul" class="form-control" value="<?= @$judul ?>">
                <div class="invalid-feedback"></div>
            </div>
            <div class="col-md-12 mt-2">
                <label for="banner" class="mb-1">Banner</label>
                <input type="file" name="file" id="banner" class="form-control-file">
                <div class="invalid-feedback"></div>
            </div>
            <div class="col-md-12 mt-2">
                <label for="editor" class="mb-1">Konten</label>
                <textarea name="keterangan" class="form-control editor"><?= @$keterangan ?></textarea>
                <div class="invalid-feedback"></div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn btn-danger" type="button" data-dismiss="modal">Close</button>
        <button class="btn btn-success float-right" type="submit">Simpan</button>
    </div>
</form>