<form action="<?= base_url('cms/faq/save') ?>" class="ajax-multipart modal-content">
    <?php if (@$id) : ?>
        <input type="hidden" name="id" value="<?= $id ?>">
    <?php endif; ?>
    <input type="hidden" name="id_menu" value="4">
    <input type="hidden" name="old_file" value="<?= @$file ?>">

    <div class="modal-header">
        <h4 class="modal-title"><?= (!@$id ? 'Tambah' : '') ?> Data FAQ</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <label for="pertanyaan" class="mb-1">Pertanyaan</label>
                <textarea name="pertanyaan" id="pertanyaan" rows="2" class="form-control"><?= @$pertanyaan ?></textarea>
                <div class="invalid-feedback"></div>
            </div>
            <div class="col-md-12 mt-2">
                <label for="editor" class="mb-1">Jawaban</label>
                <textarea name="jawaban" class="form-control editor"><?= @$jawaban ?></textarea>
                <div class="invalid-feedback"></div>
            </div>
            <div class="col-md-12 mb-2">
                <label for="flag" class="mb-1 d-block">Status</label>
                <label class="c-switch c-switch-pill c-switch-lg c-switch-success">
                    <input name="flag" class="c-switch-input" type="checkbox" <?= (@$flag == '1' ? 'checked' : '') ?> value="1"><span class="c-switch-slider"></span>
                </label>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn btn-danger" type="button" data-dismiss="modal">Close</button>
        <button class="btn btn-success float-right" type="submit">Simpan</button>
    </div>
</form>