<?= $this->extend('general_layout'); ?>
<?= $this->section('breadcrumb'); ?>
<ol class="breadcrumb border-0 m-0">
    <li class="breadcrumb-item">Website CMS</li>
    <li class="breadcrumb-item active">Tentang Kami</li>
</ol>
<?= $this->endSection('breadcrumb'); ?>
<?= $this->section('main'); ?>
<div class="container-fluid">
    <div class="fade-in">
        <div class="card">
            <div class="card-header">
                <h5 class="card-title mb-0">Tentang Kami</h5>
            </div>
            <div class="card-body">
                <form action="<?= base_url('cms/about/save'); ?>" class="row ajax-multipart" data-respond="reload">
                    <input type="hidden" name="id" value="<?= $data['id'] ?>">
                    <div class="col-md-12">
                        <label for="judul" class="mb-1">Judul</label>
                        <input type="text" name="judul" id="judul" class="form-control" value="<?= $data['judul'] ?>">
                        <div class="invalid-feedback"></div>
                    </div>
                    <div class="col-md-12 mt-2">
                        <label for="banner" class="mb-1">Banner</label>
                        <input type="file" name="file" id="banner" class="form-control-file">
                        <div class="invalid-feedback"></div>
                        <img src="<?= base_url('uploads/logo.png'); ?>" class="img-thumbnail mt-2 w-25">
                    </div>
                    <div class="col-md-12 mt-2">
                        <label for="editor" class="mb-1">Konten</label>
                        <textarea name="keterangan" class="form-control editor"><?= $data['keterangan'] ?></textarea>
                        <div class="invalid-feedback"></div>
                    </div>
                    <div class="col-md-12 mt-2">
                        <button type="submit" class="btn btn-success">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection('main'); ?>