<form action="<?= base_url('cms/partner/save') ?>" class="ajax-multipart modal-content">
    <?php if (@$id) : ?>
        <input type="hidden" name="id" value="<?= $id ?>">
    <?php endif; ?>
    <input type="hidden" name="old_img" value="<?= @$img ?>">

    <div class="modal-header">
        <h4 class="modal-title"><?= (!@$id ? 'Tambah' : '') ?> Data Partner</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <label for="judul" class="mb-1">Judul</label>
                <input type="text" name="judul" id="judul" class="form-control" value="<?= @$judul ?>">
                <div class="invalid-feedback"></div>
            </div>
            <div class="col-md-12 mt-2">
                <label for="gambar" class="mb-1">Gambar</label>
                <input type="file" name="img" id="gambar" class="form-control-file">
                <div class="invalid-feedback"></div>
                <small id="gambarHelp" class="form-text text-muted">
                    Gambar harus berukuran 1:1. <br> Cth. 500px x 500px
                </small>
            </div>
            <div class="col-md-12 mt-2">
                <label for="keterangan" class="mb-1">Keteranagan</label>
                <textarea name="keterangan" id="keterangan" class="form-control"><?= @$keterangan ?></textarea>
                <div class="invalid-feedback"></div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn btn-danger" type="button" data-dismiss="modal">Close</button>
        <button class="btn btn-success float-right" type="submit">Simpan</button>
    </div>
</form>