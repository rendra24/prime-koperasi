<form action="<?= base_url('cms/testimoni/save') ?>" class="ajax-multipart modal-content">
    <?php if (@$id) : ?>
        <input type="hidden" name="id" value="<?= $id ?>">
    <?php endif; ?>
    <input type="hidden" name="old_testimoni" value="<?= @$old_testimoni ?>">
    <input type="hidden" name="old_foto_user" value="<?= @$foto_user ?>">

    <div class="modal-header">
        <h4 class="modal-title"><?= (!@$id ? 'Tambah' : '') ?> Data Testimoni</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <label for="nama" class="mb-1">Nama Pelanggan</label>
                <input type="text" name="nama" id="nama" class="form-control" value="<?= @$nama ?>">
                <div class="invalid-feedback"></div>
            </div>
            <div class="col-md-12 mt-2">
                <label for="foto_user" class="mb-1">Foto Pelanggan</label>
                <input type="file" name="foto_user" id="foto_user" class="form-control-file">
                <div class="invalid-feedback"></div>
                <small id="gambarHelp" class="form-text text-muted">
                    Foto Pelanggan harus berukuran 1:1. <br> Cth. 500px x 500px
                </small>
            </div>
            <div class="col-md-12 mt-2">
                <label for="keterangan" class="mb-1">Tipe Testimoni</label>
                <select name="tipe_testimoni" id="tipe_testimoni" class="form-control">
                    <option value="foto" <?= (@$tipe_testimoni == 'foto' ? 'selected' : false) ?>>Foto</option>
                    <option value="video" <?= (@$tipe_testimoni == 'video' ? 'selected' : false) ?>>Video</option>
                    <option value="text" <?= (@$tipe_testimoni == 'text' ? 'selected' : false) ?>>Text</option>
                </select>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn btn-danger" type="button" data-dismiss="modal">Close</button>
        <button class="btn btn-success float-right" type="submit">Simpan</button>
    </div>
</form>
<div id="hidden-place" style="display: none;">
    <div class="col-md-12 mt-2 place_testi" id="place_testi_foto">
        <label for="foto_testimoni" class="mb-1">Foto Testimoni</label>
        <input type="file" name="testimoni" id="foto_testimoni" class="form-control-file">
        <div class="invalid-feedback"></div>
    </div>
    <div class="col-md-12 mt-2 place_testi" id="place_testi_video">
        <label for="video_testimoni" class="mb-1">Link Video Testimoni</label>
        <input type="text" name="testimoni" id="video_testimoni" class="form-control" value="<?= @substr($testimoni, 30) ?>">
        <div class="invalid-feedback"></div>
        <small id="gambarHelp" class="form-text text-muted">
            Masukkan link video testimoni yang sudah diupload di YouTube (Cth: fqBWpr_60Oo).
        </small>
    </div>
    <div class="col-md-12 mt-2 place_testi" id="place_testi_text">
        <label for="text_testimoni" class="mb-1">Testimoni</label>
        <textarea name="testimoni" id="text_testimoni" class="form-control"><?= @$testimoni ?></textarea>
        <div class="invalid-feedback"></div>
    </div>
</div>
<script>
    testimoni_func();

    function testimoni_func() {
        data = $('#hiden-place');
        tipe = $('#tipe_testimoni').val();
        $('.modal-body .row').find('.place_testi').remove();
        $('.modal-body .row').append($('#place_testi_' + tipe).clone());
    }

    $('#tipe_testimoni').change(function() {
        testimoni_func();
    });
</script>