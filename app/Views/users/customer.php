<?= $this->extend('general_layout'); ?>
<?= $this->section('breadcrumb'); ?>
<ol class="breadcrumb border-0 m-0">
    <li class="breadcrumb-item">Manajemen Pengguna</li>
    <li class="breadcrumb-item active">Member</li>
</ol>
<?= $this->endSection('breadcrumb'); ?>
<?= $this->section('main'); ?>
<div class="container-fluid">
    <div class="fade-in">
        <div class="card">
            <div class="card-header">
                <h5 class="card-title mb-0">Data Member</h5>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="tableAjax table table-bordered table-striped w-100" data-source="<?= base_url('users/customer_data'); ?>">
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection('main'); ?>