<div class="modal-header">
    <h4 class="modal-title"><strong>Detail</strong> Penyedia / Vendor</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-6">
            <h5 class="m-0">Data Perusahaan</h5>
            <hr class="my-1">
            <div class="mb-1">
                <label class="mb-0 d-block">Nama Perushaaan</label>
                <strong><?= $db['nama_usaha'] ?></strong>
            </div>
            <div class="mb-1">
                <label class="mb-0 d-block">Alamat Perushaaan</label>
                <strong><?= $db['alamat_usaha'] ?></strong>
            </div>
            <div class="mb-1">
                <label class="mb-0 d-block">Telp Perushaaan</label>
                <strong><?= $db['telp_usaha'] ?></strong>
            </div>
            <div class="mb-1">
                <label class="mb-0 d-block">Tanggal Pendaftaran</label>
                <strong><?= formatTglIndo($db['created_at']) ?></strong>
            </div>
        </div>
        <div class="col-md-6">
            <h5 class="m-0">Data Pemilik</h5>
            <hr class="my-1">
            <div class="mb-1">
                <label class="mb-0 d-block">Nama Pemilik</label>
                <strong><?= $db['nama_pemilik'] ?></strong>
            </div>
            <div class="mb-1">
                <label class="mb-0 d-block">Telp Pemilik</label>
                <strong><?= $db['telp_pemilik'] ?></strong>
            </div>
            <div class="mb-1">
                <label class="mb-0 d-block">Email Pemilik</label>
                <strong><?= $db['email_pemilik'] ?></strong>
            </div>
            <div class="mb-1">
                <label class="mb-0 d-block">NIK Pemilik</label>
                <strong><?= $db['nik_pemilik'] ?></strong>
            </div>
            <div class="mb-1">
                <label class="mb-0 d-block">Foto KTP</label>
                <img src="<?= base_url('uploads_vendor/' . $db['upload_ktp_pemilik']); ?>" alt="Foto KTP" class="img-thumbnail w-100">
            </div>
        </div>
    </div>
    <h5 class="m-0 mt-3">Data Bank</h5>
    <hr class="my-1">
    <div class="row">
        <div class="col-md-4">
            <label class="mb-0">Bank</label>
        </div>
        <div class="col-md-8">
            : <strong>
                <?php
                foreach ($bank as $item) {
                    if ($item['val'] == $db['bank_rekening']) {
                        echo $item['text'];
                        break;
                    }
                }
                ?>
            </strong>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <label class="mb-0">Nama</label>
        </div>
        <div class="col-md-8">
            : <strong><?= $db['nama_rekening'] ?></strong>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <label class="mb-0">Nomor Rekening</label>
        </div>
        <div class="col-md-8">
            : <strong><?= $db['no_rekening'] ?></strong>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <label class="mb-0">Foto Buku Rekening</label>
        </div>
        <div class="col-md-8">
            <img src="<?= base_url('uploads_vendor/' . $db['upload_buku_rekening']); ?>" alt="Foto Buku Rekening" class="img-thumbnail w-100">
        </div>
    </div>
    <h5 class="m-0 mt-3">Data Lainnya</h5>
    <hr class="my-1">
    <div class="row">
        <div class="col-md-6">
            <label class="mb-0 d-block">Dokumen Kelengkapan</label>
            <?php if ($db['upload_kelengkapan'] != '') : ?>
                <a href="<?= base_url('uploads_vendor/' . $db['upload_kelengkapan']); ?>" target="_blank">Download</a>
            <?php else : ?>
                <strong>-</strong>
            <?php endif; ?>
        </div>
        <div class="col-md-6">
            <label class="mb-0 d-block">NPWP</label>
            <strong><?= $db['nomor_npwp'] ?></strong>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button class="btn btn-danger" type="button" data-dismiss="modal">Close</button>
</div>