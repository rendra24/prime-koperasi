<form action="<?= base_url('users/vendor_save') ?>" class="ajax modal-content">
    <input type="hidden" name="id" value="<?= $db['id'] ?>">
    <div class="modal-header">
        <h4 class="modal-title">Ubah Data Penyedia atau Vendor</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-6 mb-3 pr-0">
                <h5 class="m-0">Data Perusahaan</h5>
                <hr class="my-1">
                <div class="mb-2">
                    <label for="nama_usaha" class="mb-1">Nama Perusahaan</label>
                    <input type="text" class="form-control" id="nama_usaha" name="nama_usaha" placeholder="Nama Perusahaan" value="<?= @$db['nama_usaha'] ?>">
                    <div class="invalid-feedback"></div>
                </div>
                <div class="mb-2">
                    <label for="alamat_usaha" class="mb-1">Alamat Perushaaan</label>
                    <textarea class="form-control" id="alamat_usaha" name="alamat_usaha" placeholder="Alamat Perushaaan"><?= @$db['alamat_usaha'] ?></textarea>
                    <div class="invalid-feedback"></div>
                </div>
                <div class="mb-2">
                    <label for="telp_usaha" class="mb-1">Telp Perusahaan</label>
                    <input type="text" class="form-control" id="telp_usaha" name="telp_usaha" placeholder="Telp Perusahaan" value="<?= @$db['telp_usaha'] ?>">
                    <div class="invalid-feedback"></div>
                </div>
            </div>
            <div class="col-md-6 mb-3">
                <h5 class="m-0">Data Pemilik</h5>
                <hr class="my-1">
                <div class="mb-2">
                    <label for="nama_pemilik" class="mb-1">Nama Pemilik</label>
                    <input type="text" class="form-control" id="nama_pemilik" name="nama_pemilik" placeholder="Nama Pemilik" value="<?= @$db['nama_pemilik'] ?>">
                    <div class="invalid-feedback"></div>
                </div>
                <div class="mb-2">
                    <label for="telp_pemilik" class="mb-1">Telp Pemilik</label>
                    <input type="text" class="form-control" id="telp_pemilik" name="telp_pemilik" placeholder="Telp Pemilik" value="<?= @$db['telp_pemilik'] ?>">
                    <div class="invalid-feedback"></div>
                </div>
            </div>
            <div class="col-md-12 mb-3">
                <h5 class="m-0">Data Bank</h5>
                <hr class="my-1">
                <div class="mb-2">
                    <label for="nama_rekening" class="mb-1">Nama</label>
                    <input type="text" class="form-control" id="nama_rekening" name="nama_rekening" placeholder="Nama" value="<?= @$db['nama_rekening'] ?>">
                    <div class="invalid-feedback"></div>
                </div>
                <div class="mb-2">
                    <label for="no_rekening" class="mb-1">Nomor Rekening</label>
                    <input type="text" class="form-control" id="no_rekening" name="no_rekening" placeholder="Nomor Rekening" value="<?= @$db['no_rekening'] ?>">
                    <div class="invalid-feedback"></div>
                </div>
                <div class="mb-2">
                    <label for="bank_rekening" class="mb-1">Bank</label>
                    <select name="bank_rekening" id="bank_rekening" class="form-control">
                        <option value="">-- PILIH --</option>
                        <?php foreach ($bank as $item) : ?>
                            <?php if ($item['val'] == $db['bank_rekening']) : ?>
                                <option value="<?= $item['val'] ?>" selected><?= $item['text'] ?></option>
                            <?php else : ?>
                                <option value="<?= $item['val'] ?>"><?= $item['text'] ?></option>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </select>
                    <div class="invalid-feedback"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn btn-danger" type="button" data-dismiss="modal">Close</button>
        <button class="btn btn-success float-right" type="submit">Simpan</button>
    </div>
</form>