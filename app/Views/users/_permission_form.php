<form action="<?= base_url('users/permission_save') ?>" class="ajax modal-content">
    <?php if (@$db['id']) : ?>
        <input type="hidden" name="id" value="<?= $db['id'] ?>">
    <?php endif; ?>
    <div class="modal-header">
        <h4 class="modal-title"><?= (!@$id ? 'Tambah' : '') ?> Data Hak Akses</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12 mb-2">
                <label for="name" class="mb-1">Nama</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Nama" value="<?= @$db['name'] ?>">
                <div class="invalid-feedback"></div>
            </div>
            <div class="col-md-12 mb-2">
                <label for="description" class="mb-1">Desc</label>
                <textarea name="description" id="description" rows="5" class="form-control"><?= @$db['description'] ?></textarea>
            </div>
            <div class="col-md-12 mb-2">
                <label for="phone" class="mb-1">Hak Akses</label>
                <?php foreach ($permission as $item) : ?>
                    <div class="form-check checkbox">
                        <input name="permissions[]" class="form-check-input" id="check<?= $item['id'] ?>" type="checkbox" value="<?= $item['id'] ?>" <?= (in_array($item['id'], @$group_perm) ? 'checked' : false) ?>>
                        <label class="form-check-label" for="check<?= $item['id'] ?>"><?= $item['name'] ?></label>
                    </div>
                <?php endforeach; ?>
                <div class="invalid-feedback"></div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn btn-danger" type="button" data-dismiss="modal">Close</button>
        <button class="btn btn-success float-right" type="submit">Simpan</button>
    </div>
</form>