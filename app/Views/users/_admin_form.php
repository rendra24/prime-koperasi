<form action="<?= base_url('users/admin_save') ?>" class="ajax modal-content">
    <?php if (@$id) : ?>
        <input type="hidden" name="id" value="<?= $id ?>">
    <?php endif; ?>
    <div class="modal-header">
        <h4 class="modal-title"><?= (!@$id ? 'Tambah' : '') ?> Data Admin</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-6 mb-2 pr-0">
                <label for="name" class="mb-1">Nama</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Nama" value="<?= @$name ?>">
                <div class="invalid-feedback"></div>
            </div>
            <div class="col-md-6 mb-2">
                <label for="phone" class="mb-1">Telepon</label>
                <input type="text" class="form-control" id="phone" name="phone" placeholder="Telepon" value="<?= @$phone ?>">
                <div class="invalid-feedback"></div>
            </div>
            <div class="col-md-6 mb-2 pr-0">
                <label for="username" class="mb-1">Username</label>
                <input type="text" class="form-control" id="username" name="username" placeholder="Username" value="<?= @$username ?>">
                <div class="invalid-feedback"></div>
            </div>
            <div class="col-md-6 mb-2">
                <label for="email" class="mb-1">Email</label>
                <input type="text" class="form-control" id="email" name="email" placeholder="Email" value="<?= @$email ?>">
                <div class="invalid-feedback"></div>
            </div>
            <?php if (@!$id) : ?>
                <div class="col-md-12 mb-2">
                    <label for="password" class="mb-1">Password</label>
                    <input type="hidden" name="password" value="123#">
                    <input type="text" class="form-control" id="password" name="email" disabled value="123#">
                    <div class="invalid-feedback"></div>
                </div>
            <?php endif; ?>
            <div class="col-md-12 mb-2">
                <label for="group_id" class="mb-1">Hak Akses</label>
                <select name="group_id" class="form-control" id="group_id">
                    <option value="">-- PILIH --</option>
                    <?php foreach ($hak_akses as $item) : ?>
                        <?php if (@$group_id == $item['id']) : ?>
                            <option value="<?= $item['id'] ?>" selected><?= $item['name'] ?></option>
                        <?php else : ?>
                            <option value="<?= $item['id'] ?>"><?= $item['name'] ?></option>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </select>
                <div class="invalid-feedback"></div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn btn-danger" type="button" data-dismiss="modal">Close</button>
        <button class="btn btn-success float-right" type="submit">Simpan</button>
    </div>
</form>