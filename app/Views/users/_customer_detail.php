<div class="modal-header">
    <h4 class="modal-title">Data Pelanggan</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-6">
            <div class="mt-2">
                <label class="mb-0">ID</label><br>
                <strong>#<?= $id ?></strong>
            </div>
            <div class="mt-2">
                <label class="mb-0">Nama</label><br>
                <strong><?= $nama ?></strong>
            </div>
            <div class="mt-2">
                <label class="mb-0">Email</label><br>
                <strong><?= $email ?></strong>
            </div>
            <div class="mt-2">
                <label class="mb-0">Telepon</label><br>
                <strong><?= $telp ?></strong>
            </div>
            <div class="mt-2">
                <label class="mb-0">Alamat</label><br>
                <strong><?= $alamat ?></strong>
            </div>
            <div class="mt-2">
                <label class="mb-0">Status</label><br>
                <strong><?= formatFlag('pelanggan', $flag) ?></strong>
            </div>
        </div>
        <div class="col-md-6">
            Foto
        </div>
    </div>
</div>
<div class="modal-footer">
    <button class="btn btn-danger" type="button" data-dismiss="modal">Close</button>
</div>