<?php

namespace Config;

class Validation
{
	//--------------------------------------------------------------------
	// Setup
	//--------------------------------------------------------------------

	/**
	 * Stores the classes that contain the
	 * rules that are available.
	 *
	 * @var array
	 */
	public $ruleSets = [
		\CodeIgniter\Validation\Rules::class,
		\CodeIgniter\Validation\FormatRules::class,
		\CodeIgniter\Validation\FileRules::class,
		\CodeIgniter\Validation\CreditCardRules::class,
		\Myth\Auth\Authentication\Passwords\ValidationRules::class,
		\App\Validation\CustomImage::class,
		 \App\Validation\UserRules::class,
	];

	/**
	 * Specifies the views that are used to display the
	 * errors.
	 *
	 * @var array
	 */
	public $templates = [
		'list' => 'CodeIgniter\Validation\Views\list',
		'single' => 'CodeIgniter\Validation\Views\single',
	];

	//--------------------------------------------------------------------
	// Rules
	//--------------------------------------------------------------------
	public $anggota = [
		'nama' => ['label' => 'Nama Lengkap', 'rules' => 'required'],
		'email' => ['label' => 'Email', 'rules' => 'required|valid_email|is_unique[anggota.email, id, {id}]'],
		'nrp' => ['label' => 'NRP', 'rules' => 'required|numeric|is_unique[anggota.nrp, id, {id}]'],
		'alamat' => ['label' => 'Alamat', 'rules' => 'required'],
	];

	public $simpanan = [
		'nama_simpanan' => ['label' => 'Nama Simpanan', 'rules' => 'required'],
		'total' => ['label' => 'Total besar simpanan', 'rules' => 'required'],
	];

	public $angsuran = [
		'nama_angsuran' => ['label' => 'Nama Simpanan', 'rules' => 'required'],
		'angsuran' => ['label' => 'Total Lama Angsuran', 'rules' => 'required'],
	];

	public $pinjaman = [
		'nama_pinjaman' => ['label' => 'Nama Pinjaman', 'rules' => 'required'],
		'bunga' => ['label' => 'Total Bunga', 'rules' => 'required'],
	];

	public $asset = [
		'nama_asset' => ['label' => 'Nama Asset', 'rules' => 'required'],
		'id_jenis_asset' => ['label' => 'Jenis Asset', 'rules' => 'required'],
	];

	public $aruskas = [
		'status_transaksi' => ['label' => 'Jenis Kas', 'rules' => 'required'],
		'id_asset' => ['label' => 'Jenis Asset', 'rules' => 'required'],
		'total' => ['label' => 'Total Kas', 'rules' => 'required'],
		'keterangan' => ['label' => 'Keterangan', 'rules' => 'required'],
	];

	public $transfer = [
		'id_asset_dari' => ['label' => 'Dari Asset Kas', 'rules' => 'required'],
		'id_asset_ke' => ['label' => 'Ke Asset Kas', 'rules' => 'required'],
		'total' => ['label' => 'Total Kas', 'rules' => 'required'],
		'keterangan' => ['label' => 'Keterangan', 'rules' => 'required'],
	];

	public $pinjaman_mobile = [
		'nama_anggota' => ['label' => 'Nama Anggota', 'rules' => 'required'],
		'bunga' => ['label' => 'Bunga', 'rules' => 'required'],
		'id_pinjaman' => ['label' => 'Pinjaman', 'rules' => 'required'],
		'total' => ['label' => 'Total Pinjaman', 'rules' => 'required'],
		'angsuran' => ['label' => 'Angsuran', 'rules' => 'required'],
		'total_pinjaman' => ['label' => 'Total Pinjaman', 'rules' => 'required|min_length[6]'],
	];

	public $transaksi_pinjaman = [
		'nama_anggota' => ['label' => 'Nama Anggota', 'rules' => 'required'],
		'bunga' => ['label' => 'Bunga', 'rules' => 'required'],
		'id_pinjaman' => ['label' => 'Pinjaman', 'rules' => 'required'],
		'id_asset' => ['label' => 'Jenis Asset harus di pilih', 'rules' => 'required'],
		'total' => ['label' => 'Total Pinjaman', 'rules' => 'required'],
		'angsuran' => ['label' => 'Angsuran', 'rules' => 'required'],
	];
	
	public $transaksi_simpanan = [
		'nama_anggota' => ['label' => 'Nama Anggota', 'rules' => 'required'],
		'id_simpanan' => ['label' => 'Simpananan atau Tabungan', 'rules' => 'required'],
		'id_asset' => ['label' => 'Asset', 'rules' => 'required'],
		'total_simpanan' => ['label' => 'Total Simpanan', 'rules' => 'required'],
	];
	
	public $setting_password = [
		'old_password' => ['label' => 'Password Lama', 'rules' => 'required'],
		'new_password' => ['label' => 'Password Baru', 'rules' => 'required'],
		'confirm_new_password' => ['label' => 'Konfirmasi Password Baru', 'rules' => 'required|matches[new_password]'],
	];

	public $permission = [
		'name' => ['label' => 'Nama', 'rules' => 'required'],
	];

	public $admin = [
		'name' => ['label' => 'Nama Lengkap', 'rules' => 'required'],
		'phone' => ['label' => 'Telepon', 'rules' => 'required|numeric'],
		'username' => ['label' => 'Username', 'rules' => 'required|alpha_dash|is_unique[users.username, id, {id}]'],
		'email' => ['label' => 'Email', 'rules' => 'required|valid_email|is_unique[users.email, id, {id}]'],
	];

	public $vendor = [
		'nama_usaha' => ['label' => 'Nama Perusahaan', 'rules' => 'required'],
		'alamat_usaha' => ['label' => 'Alamat Perusahaan', 'rules' => 'required'],
		'telp_usaha' => ['label' => 'Telp ', 'rules' => 'required|numeric'],
		'nama_pemilik' => ['label' => 'Nama Pemilik', 'rules' => 'required'],
		'telp_pemilik' => ['label' => 'Telp Pemilik', 'rules' => 'required|numeric'],
		'nama_rekening' => ['label' => 'Nama', 'rules' => 'required'],
		'no_rekening' => ['label' => 'Nomor Rekening', 'rules' => 'required|numeric'],
		'bank_rekening' => ['label' => 'Bank', 'rules' => 'required'],
	];

	public $area = [
		'nama' => ['label' => 'Area', 'rules' => 'required|is_unique[layanan_area.nama, id, {id}]'],
		'alamat' => ['label' => 'Alamat', 'rules' => 'required'],
		'latitude' => ['label' => 'Latitude', 'rules' => 'required'],
		'longitude' => ['label' => 'Longtude', 'rules' => 'required'],
	];

	public $product = [
		'id_layanan_sub' => ['label' => 'Sub Layanan', 'rules' => 'required'],
		'nama' => ['label' => 'Nama Produk', 'rules' => 'required'],
		'harga' => ['label' => 'Harga', 'rules' => 'required'],
		'harga_vendor' => ['label' => 'Harga Vendor', 'rules' => 'required'],
		'harga_owner' => ['label' => 'Harga Owner', 'rules' => 'required'],
		'foto' => ['label' => 'Foto', 'rules' => 'mime_in[foto,image/jpg,image/jpeg,image/png]|img_scale[foto,1:1]'],
	];

	public $kategori = [
		'nama' => ['label' => 'Nama Kategori', 'rules' => 'required'],
	];

	public $sub = [
		'nama' => ['label' => 'Nama', 'rules' => 'required'],
	];

	public $hour = [
		'jam' => ['label' => 'Jam', 'rules' => 'required'],
	];

	public $budget = [
		'anggaran' => ['label' => 'anggaran', 'rules' => 'required'],
	];

	public $offering = [
		'lampiran_penawaran' => ['label' => 'Lampiran Penawaran', 'rules' => 'uploaded[lampiran_penawaran]|mime_in[lampiran_penawaran,application/pdf,application/msword],max_size[lampiran_penawaran, 10240]'],
		'tgl_pengerjaan' => ['label' => 'Tanggal Pengerjaan', 'rules' => 'required'],
		'jam_pengerjaan' => ['label' => 'Jam Pengerjaan', 'rules' => 'required'],
		'total_harga' => ['label' => 'Total Biaya', 'rules' => 'required'],
	];

	public $voucher = [
		'kode_voucher' => ['label' => 'Kode Voucher', 'rules' => 'required|is_unique[voucher.kode_voucher, id, {id}]'],
		'nama' => ['label' => 'Nama', 'rules' => 'required'],
		'diskon' => ['label' => 'Diskon', 'rules' => 'required|numeric'],
		'foto' => ['label' => 'Foto', 'rules' => 'mime_in[foto,image/jpg,image/jpeg,image/png]|max_size[foto, 3072]'],
	];

	public $cms_slider = [
		'judul' => ['label' => 'Judul', 'rules' => 'required|is_unique[cms_slider.judul, id, {id}]'],
		'img' => ['label' => 'Slider', 'rules' => 'mime_in[img,image/jpg,image/jpeg,image/png]|max_size[img, 2048]|img_scale[img,2000:851]'],
	];

	public $cms_partner = [
		'judul' => ['label' => 'Judul', 'rules' => 'required|is_unique[cms_partner.judul, id, {id}]'],
		'img' => ['label' => 'Gambar', 'rules' => 'mime_in[img,image/jpg,image/jpeg,image/png]|max_size[img, 2048]|img_scale[img,1:1]'],
		'keterangan' => ['label' => 'Keterangan', 'rules' => 'required'],
	];

	public $cms_testimoni_foto = [
		'nama' => ['label' => 'Nama', 'rules' => 'required'],
		'foto_user' => ['label' => 'Foto Pelanggan', 'rules' => 'mime_in[foto_user,image/jpg,image/jpeg,image/png]|max_size[foto_user, 2048]|img_scale[foto_user,1:1]'],
		'testimoni' => ['label' => 'Foto', 'rules' => 'mime_in[testimoni,image/jpg,image/jpeg,image/png]|max_size[testimoni, 2048]'],
	];

	public $cms_testimoni_video = [
		'nama' => ['label' => 'Nama', 'rules' => 'required'],
		'foto_user' => ['label' => 'Foto Pelanggan', 'rules' => 'mime_in[foto_user,image/jpg,image/jpeg,image/png]|max_size[foto_user, 2048]|img_scale[foto_user,1:1]'],
		'testimoni' => ['label' => 'Link Video', 'rules' => 'required'],
	];

	public $cms_testimoni_text = [
		'nama' => ['label' => 'Nama', 'rules' => 'required'],
		'foto_user' => ['label' => 'Foto Pelanggan', 'rules' => 'mime_in[foto_user,image/jpg,image/jpeg,image/png]|max_size[foto_user, 2048]|img_scale[foto_user,1:1]'],
		'testimoni' => ['label' => 'Testimoni', 'rules' => 'required'],
	];

	public $cms_konten = [
		'judul' => ['label' => 'Judul', 'rules' => 'required|is_unique[cms_konten.judul, id, {id}]'],
		'file' => ['label' => 'Banner', 'rules' => 'mime_in[file,image/jpg,image/jpeg,image/png],max_size[file, 2048]'],
		'keterangan' => ['label' => 'Konten', 'rules' => 'required'],
	];

	public $cms_menu = [
		'nama' => ['label' => 'Nama', 'rules' => 'required|is_unique[cms_menu.nama, id, {id}]'],
		'img' => ['label' => 'Banner', 'rules' => 'mime_in[img,image/jpg,image/jpeg,image/png],max_size[img, 2048]'],
	];

	public $cms_faq = [
		'pertanyaan' => ['label' => 'Pertanyaan', 'rules' => 'required|is_unique[faq.pertanyaan, id, {id}]'],
		'jawaban' => ['label' => 'Jawaban', 'rules' => 'required'],
	];

	public $ckeditor = [
		'img' => ['label' => 'Gambar', 'rules' => 'mime_in[img,image/jpg,image/jpeg,image/png],max_size[img, 2048]'],
	];
}