<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Dashboard');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/**
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Dashboard::index');
$routes->get('verifikasi_email', 'Api\Verifikasi::verif_email');
$routes->group('setting', function ($routes) {
	$routes->post('profile', 'Setting::profile_save');
	$routes->post('password', 'Setting::password_save');
});
$routes->group('users', function ($routes) {
	$routes->delete('permission_save/(:num)', 'Users::permission_delete/$1');
	$routes->delete('admin_save/(:num)', 'Users::admin_delete/$1');
	$routes->delete('vendor_save/(:num)', 'Users::vendor_delete/$1');
	$routes->delete('customer_save/(:num)', 'Users::pelanggan_delete/$1');
});
$routes->group('order', function ($routes) {
	$routes->get('soft_cleaning/(:num)', 'Order\Soft_cleaning::detail/$1');
	$routes->get('renovation/(:num)', 'Order\Renovation::detail/$1');
	$routes->get('carwash/(:num)', 'Order\Carwash::detail/$1');
});
$routes->group('service', function ($routes) {
	$routes->delete('soft_cleaning/hour_save/(:num)', 'Service\Soft_cleaning::hour_delete/$1');
	$routes->delete('renovation/hour_save/(:num)', 'Service\Renovation::hour_delete/$1');
	$routes->delete('renovation/sub_save/(:num)', 'Service\Renovation::sub_delete/$1');
	$routes->delete('renovation/budget_save/(:num)', 'Service\Renovation::budget_delete/$1');
	$routes->delete('carwash/hour_save/(:num)', 'Service\Carwash::hour_delete/$1');
});
$routes->group('cms', function ($routes) {
	$routes->delete('faq/save/(:num)', 'Cms\Faq::delete/$1');
});

/**
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
