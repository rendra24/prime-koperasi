<?php
function jsonRes($response, $data = [])
{
    $data = array(
        'response' => $response,
        'data' => $data,
    );
    if ($response == 'validation_error') {
        $data = array(
            'response' => $response,
            'validation' => $data,
        );
    }
    return $data;
}
function jsonGenrateTables($response, $args)
{
    if (!@$args['table']) {
    }
    if (!@$args['data']) {
    }
    if (!@$args['no']) {
        $args['no'] = false;
    }
    if (!@$args['action']) {
        $args['action'] = '';
    }
    if (!@$args['url']) {
        $args['url'] = [];
    }
    if (!@$args['helper']) {
        $args['helper'] = [];
    }

    $fColumns = [];
    $button = '';
    $type_action = explode('|', $args['action']);
    $number = 1;
    $tableTitle = explode("|", $args['table']['title']);
    $tableData = explode("|", $args['table']['data']);
    $dataReturn = [];

    // genrate format column Datatable
    foreach ($tableTitle as $keyTitle => $itemTitle) {
        array_push($fColumns, ['title' => $itemTitle, 'data' => $tableData[$keyTitle]]);
    }

    foreach ($args['data'] as $key => $item) {
        // Cut Array
        foreach ($tableData as $itemData) {
            if ($itemData != 'no' && $itemData != 'action') {
                $dataReturn[$key][$itemData] = $item[$itemData];
            }
        }

        // genrate number 
        if ($args['no'] == true) {
            $dataReturn[$key]['no'] = $number++;
        }

        // genrate action button
        if (count($type_action) != 0) {
            $button = '<div class="btn-group btn-group-sm" role="group" aria-label="Small button group">';
            if (in_array('detail', $type_action)) {
                $button .= '<button class="btn btn-info" data-toggle="modal" data-target="#modalSide" data-page="' . $args['url']['detail'] . '" key="' . $item['id'] . '">';
                $button .= '<i class="c-icon-c-s mr-1 cil-search"></i>';
                $button .= '<span>Detail</span>';
                $button .= '</button>';
            }
            if (in_array('edit', $type_action)) {
                $button .= '<button class="btn btn-warning" data-toggle="modal" data-target="#modalSide" data-page="' . $args['url']['edit'] . '" key="' . $item['id'] . '">';
                $button .= '<i class="c-icon-c-s mr-1 cil-pencil"></i>';
                $button .= '<span>Ubah</span>';
                $button .= '</button>';
            }
            if (in_array('delete', $type_action)) {
                $button .= '<button class="btn btn-danger ajax-del" data-url="' . base_url($args['url']['delete'] . '/' . $item['id']) . '">';
                $button .= '<i class="c-icon-c-s mr-1 cil-trash"></i>';
                $button .= '<span>Hapus</span>';
                $button .= '</button>';
            }
            if (in_array('reset', $type_action)) {
                $button .= '<button class="btn btn-primary ajax-reset" data-url="' . base_url($args['url']['reset'] . '/' . $item['id']) . '">';
                $button .= '<i class="c-icon-c-s mr-1 cil-reload"></i>';
                $button .= '<span>Reset Saldo</span>';
                $button .= '</button>';
            }
            if (in_array('refund', $type_action)) {
                if($item['status'] == 2){
                $button .= '<button class="btn btn-primary ajax-refund" data-url="' . base_url($args['url']['refund'] . '/' . $item['id']) . '">';
                $button .= '<i class="c-icon-c-s mr-1 cil-reload"></i>';
                $button .= '<span>Refund</span>';
                $button .= '</button>';
                }
            }
            if (in_array('password', $type_action)) {
                $button .= '<button class="btn btn-dark ajax-reset-pass" data-url="' . base_url($args['url']['password'] . '/' . $item['id']) . '">';
                $button .= '<i class="c-icon-c-s mr-1 cil-lock-locked"></i>';
                $button .= '<span>Reset Password</span>';
                $button .= '</button>';
            }
            if (in_array('penawaran', $type_action)) {
                $button .= '<button class="btn btn-primary" data-toggle="modal" data-target="#modalSide" data-page="' . $args['url']['penawaran'] . '" key="' . $item['id'] . '">';
                $button .= '<i class="c-icon-c-s mr-1 cil-send"></i>';
                $button .= '<span>Input Penawaran</span>';
                $button .= '</button>';
            }

             if (in_array('print', $type_action)) {
                $button .= '<a class="btn btn-success" href="' . base_url('pinjaman/perjanjian') . '/' . $item['id'] . '" key="' . $item['id'] . '">';
                $button .= '<i class="c-icon-c-s mr-1 cil-print"></i>';
                $button .= '<span>Surat Perjanjian</span>';
                $button .= '</a>';
            }
            $button .= '</div>';
            $dataReturn[$key]['action'] = $button;
        }

        // genrate call helper
        foreach ($args['helper'] as $keyFunct => $itemFunct) {
            $get_data_key = $args['data'][$key][$keyFunct];

            $args_func_replace = str_replace('{data}', $get_data_key, $itemFunct[1]);

            //mencari parameter yang ada di array data
            foreach (array_keys($item) as $itemKeyHelp) {
                $get_value = $args['data'][$key][$itemKeyHelp];
                $get_value = str_replace('|', '', $get_value);
                $args_func_replace = str_replace('{' . $itemKeyHelp . '}', $get_value, $args_func_replace);
            }

            $dataReturn[$key][$keyFunct] = call_user_func_array($itemFunct[0], explode('|', $args_func_replace));
        }
    }

    $return = array(
        'response' => $response,
        'data' => $dataReturn,
        'columns' => $fColumns,
    );
    return $return;
}