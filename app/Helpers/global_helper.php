<?php

function formatUang($value)
{
    if ($value != 0 || $value != '') {
        if (isset($value)) {
            return number_format($value, 0, ",", ".");
        }
    } else {
        return 0;
    }
}

function send_notif_mobile($title, $player_id, $page, $notification_id = '')
{

    $content      = array(
        "en" =>  $title,
        "headings" => $title
    );

    if ($player_id != '') {
        $fields = array(
            'app_id' => "77bb684e-7b88-47e0-af22-4ea4f709bc1d",
            'include_player_ids' => array($player_id),
            'data' => array(
                "page" => $page,
                "notification_id" => $notification_id,
            ),
            'contents' => $content
        );
    } else {
        $fields = array(
            'app_id' => "77bb684e-7b88-47e0-af22-4ea4f709bc1d",
            'included_segments' => array(
                'Subscribed Users'
            ),
            'data' => array(
                "foo" => "bar"
            ),
            'contents' => $content
        );
    }



    $fields = json_encode($fields);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json; charset=utf-8',
        'Authorization: Basic ZGI0ZGU5M2EtOTZlMC00NDk5LTk4OGItMTA1NGI1NjE1Y2Mw'
    ));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

    $response = curl_exec($ch);
    curl_close($ch);

    return $response;
}

function status_transaksi($id_status)
{
    if ($id_status == 0) {
        $status = '<span class="badge color-red">CANCEL</span>';
    } else if ($id_status == 1) {
        $status = '<span class="badge color-blue">ORDER</span>';
    } else if ($id_status == 2) {
        $status = '<span class="badge color-green">PAID</span>';
    } else if ($id_status == 3) {
        $status = '<span class="badge color-yellow">PROSES</span>';
    } else if ($id_status == 4) {
        $status = '<span class="badge color-green">SELESAI</span>';
    } else if ($id_status == 5) {
        $status = '<span class="badge color-blue">PENAWARAN</span>';
    } else {
        $status = '<span class="badge color-gray">NON</span>';
    }

    return $status;
}

function send_sms($phone, $msg)
{
    $curl = curl_init();
    $token = $_SERVER['APIKEY_SMS'];
    $data = [
        'phone' => $phone,
        'message' => $msg,
    ];

    curl_setopt(
        $curl,
        CURLOPT_HTTPHEADER,
        array(
            "Authorization: $token",
        )
    );
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
    curl_setopt($curl, CURLOPT_URL, $_SERVER['URLSMS']);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
    $result = curl_exec($curl);
    curl_close($curl);

    return $result;
}

function load_curl($url, $method='GET' , $data=''){
    $curl = service('curlrequest');

    if($data != ''){
        $posts_data = $curl->request($method, $url, [
            "headers" => [
                "Accept" => "application/json"
            ],
            'form_params' => $data,
        ]);
    }else{
        $posts_data = $curl->request($method, $url, [
            "headers" => [
                "Accept" => "application/json"
            ],
        ]);
    }
    

    $data = $posts_data->getBody();

    return json_decode($data, true);
}

function curl_json($url, $method, $data){

    $curl = curl_init();

    curl_setopt_array($curl, [
    CURLOPT_URL => $url,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => $method,
    CURLOPT_POSTFIELDS => $data,
    CURLOPT_HTTPHEADER => [
        "Content-Type: application/json"
    ],
    ]);

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
    echo "cURL Error #:" . $err;
    } else {
        return json_decode($response, true);

    }
}