<?php

function countChat()
{
    $modelObrolan = new App\Models\ObrolanRoomModel();
    $data = $modelObrolan->countChatAdmin();

    if ($data['notif'] == 0) {
        return false;
    }
    return $data['notif'];
}
