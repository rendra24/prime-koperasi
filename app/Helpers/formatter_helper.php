<?php

/**
 * memngubah tanggal sistem menjadi tanggal indo 
 * @param string $date
 * @return string date
 */

function FullFormatTgl($tanggal, $cetak_hari = false)
{
	$hari = array ( 1 =>    'Senin',
				'Selasa',
				'Rabu',
				'Kamis',
				'Jumat',
				'Sabtu',
				'Minggu'
			);
			
	$bulan = array (1 =>   'Januari',
				'Februari',
				'Maret',
				'April',
				'Mei',
				'Juni',
				'Juli',
				'Agustus',
				'September',
				'Oktober',
				'November',
				'Desember'
			);
	$split 	  = explode('-', $tanggal);
	$tgl_indo = $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
	
	if ($cetak_hari) {
		$num = date('N', strtotime($tanggal));
		return $hari[$num] . ', ' . $tgl_indo;
	}
	return $tgl_indo;
}

function formatTglIndo($date)
{
    $dateOnly = date('Y-m-d', strtotime($date));
    $bulan = array(
        1 =>   'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
    );

    $split = explode('-', $dateOnly);
    return $split[2] . ' ' . $bulan[(int)$split[1]] . ' ' . $split[0];
}

/**
 * memngubah tanggal sistem menjadi tanggal indo 
 * @param int $int
 * @return string 
 */
function formatBlnIndo($int)
{
    $bulan = array(
        1 =>   'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
    );

    return $bulan[(int)$int];
}

/**
 * memngubah tanggal sistem menjadi tanggal string d/m/Y 
 * @param string $date
 * @return string date 
 */
function formatTglString($date)
{
    return date('d/m/Y', strtotime($date));
}

/**
 * memngubah tanggal sistem menjadi tanggal string d/m/Y 
 * @param string $time
 * @return string time 
 */
function formatJam($time)
{
    return date('H:i', strtotime($time));
}

/**
 * mengubah angka sistem menjadi format rupiah 
 * @param integer $number
 * @return string 
 */
function formatRupiah($number)
{
    $result = "Rp. " . number_format($number, 0, '', '.');
    return $result;
}

function penyebut($nilai) {
    $nilai = abs($nilai);
    $huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
    $temp = "";
    if ($nilai < 12) {
        $temp = " ". $huruf[$nilai];
    } else if ($nilai <20) {
        $temp = penyebut($nilai - 10). " belas";
    } else if ($nilai < 100) {
        $temp = penyebut($nilai/10)." puluh". penyebut($nilai % 10);
    } else if ($nilai < 200) {
        $temp = " seratus" . penyebut($nilai - 100);
    } else if ($nilai < 1000) {
        $temp = penyebut($nilai/100) . " ratus" . penyebut($nilai % 100);
    } else if ($nilai < 2000) {
        $temp = " seribu" . penyebut($nilai - 1000);
    } else if ($nilai < 1000000) {
        $temp = penyebut($nilai/1000) . " ribu" . penyebut($nilai % 1000);
    } else if ($nilai < 1000000000) {
        $temp = penyebut($nilai/1000000) . " juta" . penyebut($nilai % 1000000);
    } else if ($nilai < 1000000000000) {
        $temp = penyebut($nilai/1000000000) . " milyar" . penyebut(fmod($nilai,1000000000));
    } else if ($nilai < 1000000000000000) {
        $temp = penyebut($nilai/1000000000000) . " trilyun" . penyebut(fmod($nilai,1000000000000));
    }     
    return $temp;
}

function terbilang($nilai) {
    if($nilai<0) {
        $hasil = "minus ". trim(penyebut($nilai));
    } else {
        $hasil = trim(penyebut($nilai));
    }     		
    return $hasil . ' rupiah';
}

/**
 * mengubah angka sistem menjadi format decimal 
 * @param integer $number
 * @return string 
 */
function formatdecimals($number, $status_transaksi='')
{
    $result = number_format($number, 0, '', '.');

    if($status_transaksi != ''){
        if($status_transaksi == 1){
            $color = "<p class='text-success'>{$result}</p>";
        }else{
            $color = "<p class='text-danger'>{$result}</p>";
        }
    }else{
        $color = $result;
    }
    return $color;
}


/**
 * format pemotong string
 * @param string $kalimat
 * @param integer $panjang
 * @return string 
 */
function formatStringCutter(string $kalimat, int $panjang)
{
    if (strlen($kalimat) <= $panjang) {
        return $kalimat;
    }

    $hasil = substr(strip_tags($kalimat), 0, $panjang);
    return $hasil . ' ...';
}

/**
 * memngubah field flag sistem menjadi format HTML 
 * @param string $type
 * @param string $flag
 * @return string html
 */
function formatFlag($type, $flag)
{
    $data['basic'] = [
        '0' => ['badge' => 'danger', 'text' => 'Nonaktif'],
        '1' => ['badge' => 'success', 'text' => 'Aktif'],
    ];
    $data['pelanggan'] = [
        '0' => ['badge' => 'danger', 'text' => 'Nonaktif'],
        '1' => ['badge' => 'success', 'text' => 'Aktif'],
        '2' => ['badge' => 'warning', 'text' => 'Ditangguhkan'],
    ];
    $template['basic'] = '<h4><span class="badge badge-{badge}">{text}</span></h4>';
    $template['pelanggan'] = '<h4><span class="badge badge-{badge}">{text}</span></h4>';

    $getType = $data[$type][$flag];

    // replace badge
    $template[$type] = str_replace('{badge}', $getType['badge'], $template[$type]);
    // replace text
    $template[$type] = str_replace('{text}', $getType['text'], $template[$type]);

    return $template[$type];
}
function formatStatus($type, $flag)
{
    $data['transaksi'] = [
        '1' => ['badge' => 'success', 'text' => 'Masuk'],
        '2' => ['badge' => 'danger', 'text' => 'Keluar'],
    ];
    $data['transaksi_renovasi'] = [
        '0' => ['badge' => 'danger', 'text' => 'Batal'],
        '1' => ['badge' => 'warning', 'text' => 'Proses'],
        '2' => ['badge' => 'success', 'text' => 'Pembayaran Selesai'],
        '3' => ['badge' => 'warning', 'text' => 'Pengerjaan'],
        '4' => ['badge' => 'success', 'text' => 'Selesai'],
        '5' => ['badge' => 'info', 'text' => 'Penawaran'],
    ];
    $data['anggota'] = [
        '0' => ['badge' => 'danger', 'text' => 'Tidak Aktif'],
        '1' => ['badge' => 'success', 'text' => 'Aktif'],
    ];

    $data['pinjaman'] = [
        '0' => ['badge' => 'info', 'text' => 'Belum Terverifikasi'],
        '1' => ['badge' => 'primary', 'text' => 'Sudah Disetujui'],
        '2' => ['badge' => 'success', 'text' => 'Lunas'],
        '3' => ['badge' => 'danger', 'text' => 'Ditolak'],
    ];
    $data['ppob'] = [
        '0' => ['badge' => 'info', 'text' => 'Pending'],
        '1' => ['badge' => 'success', 'text' => 'Success'],
        '2' => ['badge' => 'danger', 'text' => 'Gagal'],
        '3' => ['badge' => 'primary', 'text' => 'Refund'],
    ];
    $template['transaksi'] = '<h4 class="mb-0"><span class="badge badge-{badge}">{text}</span></h4>';
    $template['transaksi_renovasi'] = '<h4 class="mb-0"><span class="badge badge-{badge}">{text}</span></h4>';
    $template['anggota'] = '<h4 class="mb-0"><span class="badge badge-{badge}">{text}</span></h4>';
    $template['pinjaman'] = '<h5 class="mb-0"><span class="badge badge-{badge}">{text}</span></h5>';
    $template['ppob'] = '<h5 class="mb-0"><span class="badge badge-{badge}">{text}</span></h5>';

    $getType = $data[$type][$flag];

    // replace badge
    $template[$type] = str_replace('{badge}', $getType['badge'], $template[$type]);
    // replace text
    $template[$type] = str_replace('{text}', $getType['text'], $template[$type]);

    return $template[$type];
}
function formatAngsuran($angsuran){
    $hasil = $angsuran . ' Bulan ('. $angsuran . 'X)';
    return $hasil;
}
function formatImg($file)
{
    $template = '<img src="{src}" class="img-thumbnail">';

    // replace src
    $template = str_replace('{src}', base_url('uploads/' . $file), $template);

    return $template;
}
function formatTestimoni($type, $testimoni)
{
    if ($type == 'foto') {
        $template = formatImg($testimoni);
    } else if ($type == 'video') {
        $template = '<iframe class="w-100" src="' . $testimoni . '"> </iframe>';
    } else {
        $template = $testimoni;
    }

    return $template;
}

function formatTanggalCount($tgl, $count, $var){
	$today = date('d-m-Y', strtotime( $tgl . " +{$count} {$var}"));
	$date = new \DateTime($today);
	return $date->format('Y-m-d');
}

function formatFullYear($data)
{
    // for ($i = 1; $i <= 12; $i++) {
    //     $isNull = true;
    //     foreach ($data as $key => $value) {
    //         if($i == )
    //     }
    // }
}

/**
 * # ------------------------------------------------------------------ 
 * * FORMATTING ARRAY COMPATIBLE WITH CHART COREUI
 * 
 * Description
 * -
 */
function formatChartCoreui($array)
{
    $data = [];
    if (count($array) >= 1) {
        //Declare key
        foreach ($array[0] as $key => $val) {
            $data[$key] = [];
        }

        //Push value
        foreach ($array as $key => $item) {
            foreach ($array[$key] as $keyI => $val) {
                if ($keyI == 'bulan') {
                    array_push($data[$keyI], formatBlnIndo($val));
                } else {
                    array_push($data[$keyI], $val);
                }
            }
        }
    }

    return $data;
}

function formatDiskon($type, $diskon)
{
    if ($type == 2) {
        return $diskon . '%';
    } else {
        return formatRupiah((int)$diskon);
    }
}


function formatPersen($angka){
    return "{$angka}%";
}