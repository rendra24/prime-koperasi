<?php

function ckInit($string = '')
{
    $sess = (session()->get('ckeditor') ? session()->get('ckeditor') : []);

    if ($string === '') {
        $arr = [];
    } else {
        preg_match_all('@src="([^"]+)"@', $string, $match);
        $arr = array_pop($match);
    }

    foreach ($sess as $i) {
        if (!in_array($i, $arr)) {
            $i = str_replace(base_url() . '/', '', $i);
            @unlink($i);
        }
    }
    session()->set('ckeditor', $arr);
}

function ckCheck($string)
{
    $sess = session()->get('ckeditor');
    foreach ($sess as $i) {
        if (strpos($string, $i) === false) {
            $i = str_replace(base_url() . '/', '', $i);
            @unlink($i);
        }
    }
    session()->remove('ckeditor');
}

function ckAdd($path)
{
    $get_sess = session()->get('ckeditor');
    array_push($get_sess, $path);

    session()->set('ckeditor', $get_sess);
}
