<?php

namespace App\Models;

use CodeIgniter\Model;

class AnggotaModels extends Model
{
	protected $DBGroup              = 'default';
	protected $table                = 'anggota';
	protected $primaryKey           = 'id';
	protected $useAutoIncrement     = true;
	protected $insertID             = 0;
	protected $returnType           = 'array';
	protected $useSoftDelete        = false;
	protected $protectFields        = true;
	protected $allowedFields        = [
		'kode_anggota',
		'nama',
		'pangkat',
		'nrp',
		'jabatan',
		'fungsi',
		'alamat',
		'email',
		'password',
		'simpanan_pokok',
		'simpanan_wajib',
		'simpanan_sukarela',
		'simpanan_tabungan',
		'onesignal_token',
		'remember_token',
		'flag'
	];

	// Dates
	protected $useTimestamps        = true;
	protected $dateFormat           = 'datetime';
	protected $createdField         = 'created_at';
	protected $updatedField         = 'updated_at';
	protected $deletedField         = 'deleted_at';

	public function get_anggota($cari)
	{
		$builder = $this->db->table($this->table)->like('nama', $cari, 'both');
		return $builder->get();
	}


	public function updaetSaldo($id_anggota, $total, $id_simpanan)
	{
		$get = $this->db->table($this->table)->where('id', $id_anggota)->get()->getRowArray();

		if ($id_simpanan == 1) {
			$data['simpanan_wajib'] = $get['simpanan_wajib'] + $total;
		} else if ($id_simpanan == 2) {
			$data['simpanan_sukarela'] = $get['simpanan_sukarela'] + $total;
		} else if ($id_simpanan == 3) {
			$data['simpanan_pokok'] = $get['simpanan_pokok'] + $total;
		} else if ($id_simpanan == 4) {
			$data['simpanan_tabungan'] = $get['simpanan_tabungan'] + $total;
		}

		$builder = $this->db->table($this->table)->where('id', $id_anggota);

		return $builder->update($data);
	}
}
