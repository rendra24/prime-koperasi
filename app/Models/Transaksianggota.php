<?php

namespace App\Models;

use CodeIgniter\Model;

class Transaksianggota extends Model
{
	protected $DBGroup              = 'default';
	protected $table                = 'transaksi_anggota';
	protected $primaryKey           = 'id';
	protected $useAutoIncrement     = true;
	protected $insertID             = 0;
	protected $returnType           = 'array';
	protected $useSoftDelete        = false;
	protected $protectFields        = true;
	protected $allowedFields        = [
		'id_transaksi',
		'id_anggota',
		'nominal',
		'keterangan',
		'id_user'
	];

	// Dates
	protected $useTimestamps        = true;
	protected $dateFormat           = 'datetime';
	protected $createdField         = 'created_at';
	protected $updatedField         = 'updated_at';
	protected $deletedField         = 'deleted_at';

	public function TransaksiAnggota($post)
	{
		$get = $this->table($this->table)
					->select('b.nama as nama_anggota, transaksi_anggota.*')
					->join('anggota b', 'transaksi_anggota.id_anggota = b.id', 'LEFT')
					->orderBy('transaksi_anggota.created_at','DESC');
					
					if(!empty($post['created_at'])){
						$start_date = $post['created_at'];
						$end_date = $post['end_date'];
						$get->where("DATE(transaksi_anggota.created_at) BETWEEN '$start_date' AND '$end_date' ");
					}

					return $get->get();
	}

}