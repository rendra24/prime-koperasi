<?php

namespace App\Models;

use CodeIgniter\Model;

class TransaksiModel extends Model
{
	protected $DBGroup              = 'default';
	protected $table                = 'transaksi';
	protected $primaryKey           = 'id';
	protected $useAutoIncrement     = true;
	protected $insertID             = 0;
	protected $returnType           = 'array';
	protected $useSoftDelete        = false;
	protected $protectFields        = true;
	protected $allowedFields        = [
		'kode_transaksi',
		'id_asset',
		'id_transaksi',
		'id_anggota',
		'total',
		'status_transaksi',
		'keterangan',
		'id_user'
	];

	// Dates
	protected $useTimestamps        = true;
	protected $dateFormat           = 'datetime';
	protected $createdField         = 'created_at';
	protected $updatedField         = 'updated_at';
	protected $deletedField         = 'deleted_at';

	public function saveTransaksi($post){
		$builder = $this->db->table($this->table);
		return $builder->insert($post);                                 
	}

	public function getLastTransaksi(){
		$builder = $this->db->table($this->table)
							->select('created_at,total,status_transaksi')
							->orderBy('id','DESC')
							->limit(5);
		return $builder->get();
							
	}

	public function getTotalKas(){
		$builder = $this->db->table($this->table)
							->select('SUM(CASE 
									WHEN status_transaksi = 1
									THEN total 
									ELSE 0 
									END) AS kas_masuk,

									SUM(CASE 
									WHEN status_transaksi = 2
									THEN total 
									ELSE 0 
									END) AS kas_keluar')
							->get();
		return $builder;							
	}

	public function getSaldoUser($id_user){
		$builder = $this->db->table('anggota')
							->where('id',$id_user);
		return $builder->get();
	}

	public function LaporanTransaksi(){
		$builder = $this->db->table('transaksi a')
							->select('a.*, b.nama, c.nama_transaksi')
							->join('anggota b','a.id_anggota = b.id','LEFT')
							->join('data_transaksi c','a.id_transaksi=c.id','LEFT')
							->where('id_transaksi', 6)
							->orderBy('a.created_at','DESC');
		return $builder->get();
	}
}