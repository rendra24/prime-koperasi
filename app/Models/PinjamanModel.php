<?php

namespace App\Models;

use CodeIgniter\Model;
use \DateTime;
use \DateInterval;

class PinjamanModel extends Model
{
	protected $DBGroup              = 'default';
	protected $table                = 'transaksi_pinjaman';
	protected $primaryKey           = 'id';
	protected $useAutoIncrement     = true;
	protected $insertID             = 0;
	protected $returnType           = 'array';
	protected $useSoftDelete        = false;
	protected $protectFields        = true;
	protected $allowedFields        = [
		'kode_transaksi',
		'id_anggota',
		'id_pinjaman',
		'id_asset',
		'total_pinjaman',
		'angsuran',
		'bunga',
		'cicilan',
		'nama_anggota',
		'status_pinjaman',
		
	];

	// Dates
	protected $useTimestamps        = true;
	protected $dateFormat           = 'datetime';
	protected $createdField         = 'created_at';
	protected $updatedField         = 'updated_at';
	protected $deletedField         = 'deleted_at';

	public function verifikasiPinjaman($post){
		$transaksi = model('TransaksiModel');
		$AssetMod = model('MasterAsset');

		$this->db->transStart();
		$id = $post['id'];
		$row = $this->db->table($this->table)->where('id', $id)->get()->getRowArray();
		$asset = $this->db->table('data_asset')->where('id', $row['id_asset'])->get()->getRowArray();

		$d = date('d-m-Y');
		$today = date('d-m-Y', strtotime( $d . " +1 days"));
		$date = new \DateTime($today);
		
		for ($i=1; $i <= $row['angsuran']; $i++) { 
			$days = cal_days_in_month(CAL_GREGORIAN, $date->format('n'), $date->format('Y'));
			$date->add(new \DateInterval('P' . $days .'D'));

			$add['tgl_bayar'] = $date->format('Y-m-d');
			$add['id_transaksi_pinjaman'] = $id;
			$add['angsuran'] = $i;
			$add['cicilan'] = $row['cicilan'];
			$add['bunga'] = (float)$row['bunga'] / 100 * (int)$row['total_pinjaman'];
			$add['status_pembayaran'] = 0;
			$add['created_at'] = date('Y-m-d H:i:s');
			$add['updated_at'] = date('Y-m-d H:i:s');

			$this->db->table('transaksi_pinjaman_detail')->insert($add);
		}
		
		$kode_transaksi = 'TR'.rand();
		
		$data['status_pinjaman'] = 1;
		$data['kode_transaksi'] = $kode_transaksi;

		$this->db->table($this->table)->where('id', $id)->update($data);
		$total_pinjaman = $row['total_pinjaman'];
		$nama_asset = $asset['nama_asset'];

		$upp['kode_transaksi'] = $kode_transaksi;
        $upp['status_transaksi'] = 2;
    	$upp['id_transaksi'] = 2;
		$upp['id_asset'] = $row['id_asset'];
        $upp['total'] = $row['total_pinjaman'];
        $upp['id_anggota'] = $row['id_anggota'];
        $upp['id_user'] = user()->id;
		$upp['keterangan'] = "Pinjaman sebesar {$total_pinjaman} dari Asset {$nama_asset} dengan kode transaksi {$kode_transaksi}";
		
        $transaksi->save($upp);

		$gData['total'] = $asset['total'] - $row['total_pinjaman'];
		$gData['id'] = $row['id_asset'];
		$AssetMod->save($gData);
		
		$this->db->transComplete();
	}

	public function get_detail($id){
		$builder = $this->db->table('transaksi_pinjaman_detail')->where('id_transaksi_pinjaman',$id);
		return $builder->get();
	}
	public function angsurPinjaman($post){
		$transaksi = model('TransaksiModel');
		$AssetMod = model('MasterAsset');
		
		$this->db->transStart();

		$id = $post['id'];
		$data['status_pembayaran'] = 1;
		$data['cicilan'] = $post['cicilan'];
		$data['bunga'] = $post['bunga'];
		$data['updated_at'] = date('Y-m-d H:i:s');

		$builder = $this->db->table('transaksi_pinjaman_detail')->where('id',$id);

		$builder->update($data);

		$get = $this->db->table('transaksi_pinjaman')->where('id', $post['id_pinjaman'])->get()->getRowArray();
		$asset = $this->db->table('data_asset')->where('id', $get['id_asset'])->get()->getRowArray();

		//cek jika pinjaman sudah lunas
		$cek_cicilan = $this->db->table('transaksi_pinjaman_detail')->where('id_transaksi_pinjaman',$post['id_pinjaman'])->where('status_pembayaran','0');
		if($cek_cicilan->countAllResults() == 0){
			$pData['status_pinjaman'] = 2;

			$this->db->table('transaksi_pinjaman')->where('id', $post['id_pinjaman'])->update($pData);
		}

		$upp['id_asset'] = 1;
		$upp['kode_transaksi'] = 'ANG'.rand();
        $upp['status_transaksi'] = 1;
    	$upp['id_transaksi'] = 3;
        $upp['total'] = $post['cicilan'];
        $upp['id_anggota'] = $get['id_anggota'];
        $upp['id_user'] = user()->id;
        $transaksi->save($upp);

		$gData['total'] = $post['cicilan'] + $asset['total'];
		$gData['id'] = 1;
		$AssetMod->save($gData);

		return $this->db->transComplete();
	}

	public function getPinjamanAll($status=''){
		$builder = $this->db->table($this->table)
							->select('SUM(total_pinjaman) as total');
		if($status != ''){
			$builder->whereIn('status_pinjaman', $status);
		}
							
		return $builder->get();
	}

	public function getPinjaman(){
		$builder = $this->db->table('transaksi_pinjaman tp')
							->join('data_pinjaman dp', 'tp.id_pinjaman = dp.id')
							->select('tp.*, dp.nama_pinjaman')
							->orderBy('created_at', 'desc');
		return $builder->get();
	}
}