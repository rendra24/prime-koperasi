<?php

namespace App\Models;

use CodeIgniter\Model;

class Simpananexcel extends Model
{
	protected $DBGroup              = 'default';
	protected $table                = 'simpanan_excel';
	protected $primaryKey           = 'id';
	protected $useAutoIncrement     = true;
	protected $insertID             = 0;
	protected $returnType           = 'array';
	protected $useSoftDelete        = false;
	protected $protectFields        = true;
	protected $allowedFields        = [
		'nrp',
		'pokok',
		'wajib',
		'sukarela',
		'tabungan',
	];

	public function getSimpanan()
	{
		$get = $this->db->table('simpanan_excel se')
			->select('se.id, se.nrp, se.pokok, se.wajib, se.sukarela, se.tabungan, ag.id as id_anggota, ag.nama')
			->join('anggota ag', 'se.nrp = ag.nrp', 'LEFT')->get();
		return $get;
	}
}
