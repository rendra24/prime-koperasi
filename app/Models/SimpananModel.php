<?php

namespace App\Models;

use CodeIgniter\Model;

class SimpananModel extends Model
{
	protected $DBGroup              = 'default';
	protected $table                = 'transaksi_simpanan';
	protected $primaryKey           = 'id';
	protected $useAutoIncrement     = true;
	protected $insertID             = 0;
	protected $returnType           = 'array';
	protected $useSoftDelete        = false;
	protected $protectFields        = true;
	protected $allowedFields        = [
		'kode_transaksi',
		'id_anggota',
		'id_simpanan',
		'total_simpanan',
		'nama_anggota',
		'status_simpanan'
	];

	// Dates
	protected $useTimestamps        = true;
	protected $dateFormat           = 'datetime';
	protected $createdField         = 'created_at';
	protected $updatedField         = 'updated_at';
	protected $deletedField         = 'deleted_at';

	public function getSimpanan($id_user = '')
	{
		$bulan = date('m');
		$tahun = date('Y');
		$builder = $this->db->table('transaksi_simpanan')
			->select('transaksi_simpanan.*, b.nama_simpanan')
			->join('data_jenis_simpanan b', 'transaksi_simpanan.id_simpanan = b.id', 'left')
			->where('MONTH(transaksi_simpanan.created_at)', $bulan)
			->where('YEAR(transaksi_simpanan.created_at)', $tahun)
			->orderBy('transaksi_simpanan.created_at', 'DESC');

		if ($id_user != '') {
			$builder->where('id_anggota', $id_user);
		}
		return $builder->get();
	}

	public function getApiSimpanan($id_user = '')
	{
		$builder = $this->db->table('transaksi_simpanan')
			->select('transaksi_simpanan.*, b.nama_simpanan')
			->join('data_jenis_simpanan b', 'transaksi_simpanan.id_simpanan = b.id', 'left')
			->orderBy('transaksi_simpanan.created_at', 'DESC');

		if ($id_user != '') {
			$builder->where('id_anggota', $id_user);
		}
		return $builder->get();
	}

	public function getSimpananAll()
	{
		$builder = $this->db->table('anggota')
			->select('(SUM(simpanan_wajib) + SUM(simpanan_pokok) + SUM(simpanan_sukarela) + SUM(simpanan_tabungan)) as total')
			->get();
		return $builder;
	}

	public function getTopAnggota()
	{
		$builder = $this->db->table($this->table)
			->select('SUM(total_simpanan) as total, b.nama')
			->join('anggota b', 'transaksi_simpanan.id_anggota= b.id', 'left')
			->groupBy('id_anggota')
			->orderBy('total', 'DESC')
			->limit(5)
			->get();
		return $builder;
	}
}