<?php

namespace App\Models;

use CodeIgniter\Model;

class GlobalModel extends Model
{
    protected $table      = 'layanan';
    // Uncomment below if you want add primary key
    // protected $primaryKey = 'id';

    public function send_email($judul, $msg, $to)
    {

        $email_smtp = \Config\Services::email();

        $email_smtp->setFrom("beresinyuk.apk@gmail.com", "Beresin Yuk");
        $email_smtp->setTo($to);

        $email_smtp->setSubject($judul);
        $email_smtp->setMessage($msg);


        $email_smtp->send();
    }

    public function send_sms($phone, $msg)
    {
        $curl = curl_init();
        $token = "";
        $data = [
            'phone' => $phone,
            'message' => $msg,
        ];

        curl_setopt(
            $curl,
            CURLOPT_HTTPHEADER,
            array(
                "Authorization: $token",
            )
        );
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($curl, CURLOPT_URL, "{ domain_api }/api/sms/send");
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        $result = curl_exec($curl);
        curl_close($curl);

        echo "<pre>";
        print_r($result);
    }

    public function send_text_wa($phone, $msg)
    {
        $curl = curl_init();
        $token = "";
        $data = [
            'phone' => $phone,
            'message' => $msg,
            'secret' => false, // or true
            'priority' => false, // or true
        ];

        curl_setopt(
            $curl,
            CURLOPT_HTTPHEADER,
            array(
                "Authorization: $token",
            )
        );
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($curl, CURLOPT_URL, "{ domain_api }/api/send-message");
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        $result = curl_exec($curl);
        curl_close($curl);

        echo "<pre>";
        print_r($result);
    }

    public function multiquery()
    {
        $db = db_connect();

        $db->transStart();
        $db->query('AN SQL QUERY...');
        $db->query('ANOTHER QUERY...');
        $db->query('AND YET ANOTHER QUERY...');
        $db->transComplete();

        if ($db->transStatus() === FALSE) {
            // generate an error... or use the log_message() function to log your error
        }
    }
}
