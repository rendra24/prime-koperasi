<?php

namespace App\Models;

use CodeIgniter\Model;
use Faker\Factory;
use Pusher\Pusher;

class ObrolanModel extends Model
{
	protected $DBGroup              = 'default';
	protected $table                = 'obrolan';
	protected $primaryKey           = 'id';
	protected $useAutoIncrement     = true;
	protected $insertID             = 0;
	protected $returnType           = 'array';
	protected $useSoftDelete        = false;
	protected $protectFields        = true;
	protected $allowedFields        = [
		'room_id',
		'role',
		'user_id',
		'user_nama',
		'isi',
		'read'
	];

	// Dates
	protected $useTimestamps        = true;
	protected $dateFormat           = 'datetime';
	protected $createdField         = 'created_at';
	protected $updatedField         = 'updated_at';
	protected $deletedField         = 'deleted_at';

	// Validation
	protected $validationRules      = [];
	protected $validationMessages   = [];
	protected $skipValidation       = false;
	protected $cleanValidationRules = true;

	// Callbacks
	protected $allowCallbacks       = true;
	protected $beforeInsert         = ['checkRoom', 'genrateID'];
	protected $afterInsert          = ['updateLastMessage', 'pusherNotif'];
	protected $beforeUpdate         = [];
	protected $afterUpdate          = [];
	protected $beforeFind           = [];
	protected $afterFind            = [];
	protected $beforeDelete         = [];
	protected $afterDelete          = [];

	public function checkRoom(array $data)
	{
		if (empty($data['data']['room_id'])) {
			$modelRoom = model('ObrolanRoomModel');

			$room['role'] = $data['data']['role'];
			$room['user_id'] = $data['data']['user_id'];
			$room['nama_room'] = $data['data']['user_nama'];
			$room['pesan_baru_member'] = 0;
			$room['pesan_baru_admin'] = 0;

			$modelRoom->save($room);

			$data['data']['room_id'] = $modelRoom->insertID;
		}
		return $data;
	}
	public function genrateID(array $data)
	{
		$faker = Factory::create();
		$id = '';
		for ($i = 0; $i < 20; $i++) {
			$id .= $faker->bothify('?##?');
			$id .= strtoupper($faker->bothify('?##?'));
			$id = $faker->shuffle($id);
		}
		$data['data']['id'] = $id;

		return $data;
	}

	public function updateLastMessage(array $param)
	{
		$modelRoom = model('ObrolanRoomModel');

		$getExistingVal = $modelRoom->find($param['data']['room_id']);

		$data['id'] = $param['data']['room_id'];
		$data['pesan_terakhir'] = $param['data']['isi'];
		if ($param['data']['role'] == 0) {
			$data['pesan_baru_admin'] = $getExistingVal['pesan_baru_admin'] + 1;
		} elseif ($param['data']['role'] == 1) {
			$data['pesan_baru_member'] = $getExistingVal['pesan_baru_member'] + 1;
		}
		$modelRoom->save($data);

		return $param;
	}

	public function pusherNotif(array $param)
	{
		$options = array(
			'cluster' => 'ap1',
			'useTLS' => true
		);
		$pusher = new Pusher(
			'40b94e01fcc8c51278e4',
			'8e6ee3063802dc83f6c2',
			'1215782',
			$options
		);

		$data['threatId'] = $param['data']['room_id'];
		$pusher->trigger('chat-channel', 'event-notif', $data);

		$dataMenu['threatId'] = $param['data']['room_id'];
		$dataMenu['role'] = $param['data']['role'];
		$dataMenu['notifMenu'] = countChat();
		$dataMenu['user_nama'] = $param['data']['user_nama'];
		$dataMenu['waktu'] = formatJam($param['data']['created_at']);
		$dataMenu['isi'] = $param['data']['isi'];

		$pusher->trigger('chat-channel', 'event-menu-chat', $dataMenu);

		if ($param['data']['role'] == 0) {

			$modelRoom = model('ObrolanRoomModel');
			$modelPelanggan = model('PelangganModel');

			$getRoom = $modelRoom->find($param['data']['room_id']);
			if ($getRoom['role'] == 1) {
				$dataUser = $modelPelanggan->select('onesignal_token')->find($getRoom['user_id']);

				$title = strip_tags($param['data']['isi']);
				$player_id = $dataUser['onesignal_token'];
				$page = 'chat';
				send_notif_mobile($title, $player_id, $page, $param['data']['room_id']);
			}
		}

		return $param;
	}
}
