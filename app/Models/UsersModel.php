<?php

namespace App\Models;

use CodeIgniter\Model;

class UsersModel extends Model
{
    protected $table      = 'users';
    // Uncomment below if you want add primary key
    protected $primaryKey = 'id';
    protected $allowedFields = [
        'name',
        'phone',
        'email',
        'username',
        'password_hash',
        'photo'
    ];
    protected $useTimestamps = true;

    public function saveGroup($data)
    {
        if (@$data['insertID']) {
            $group['group_id'] = $data['group_id'];
            $group['user_id'] = $data['insertID'];
            $this->db->table('auth_groups_users')
                ->insert($group);
        } else {
            $group['group_id'] = $data['group_id'];
            $this->db->table('auth_groups_users')
                ->where('user_id', $data['id'])
                ->update($group);
        }
    }
}
