<?php

namespace App\Models;

use CodeIgniter\Model;

class LaporanModel extends Model
{
	protected $DBGroup              = 'default';
	protected $table                = 'transaksi';
	protected $primaryKey           = 'id';
	protected $useAutoIncrement     = true;
	protected $insertID             = 0;
	protected $returnType           = 'array';
	protected $useSoftDelete        = false;
	protected $protectFields        = true;
	protected $allowedFields        = [];

	// Dates
	protected $useTimestamps        = true;
	protected $dateFormat           = 'datetime';
	protected $createdField         = 'created_at';
	protected $updatedField         = 'updated_at';
	protected $deletedField         = 'deleted_at';

	public function LaporanAnggota(){
		$builder = $this->db->table('anggota')
							->select('nrp,nama,pangkat,jabatan,simpanan_sukarela,simpanan_wajib,simpanan_pokok,created_at')
							->orderBy('nama','ASC');
		return $builder->get();
	}

	public function LaporanTransaksi($post){
		$builder = $this->db->table('transaksi a')
							->select('a.*, b.nama, c.nama_transaksi')
							->join('anggota b','a.id_anggota = b.id','LEFT')
							->join('data_transaksi c','a.id_transaksi=c.id','LEFT')
							->orderBy('a.created_at','DESC');
					if(!empty($post['start_date'])){
						// $builder->where('a.created_at >=', $post['start_date']);
						// $builder->where('a.created_at <=', $post['end_date']);
						$start_date = $post['start_date'];
						$end_date = $post['end_date'];
						$builder->where("DATE(a.created_at) BETWEEN '$start_date' AND '$end_date' ");
					}else{
						$datenow = date('Y-m-d');
						$builder->where("DATE(a.created_at) = '$datenow' ");
					}
		return $builder->get();
	}
}