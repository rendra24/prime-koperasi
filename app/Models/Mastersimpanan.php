<?php

namespace App\Models;

use CodeIgniter\Model;

class Mastersimpanan extends Model
{
	protected $DBGroup              = 'default';
	protected $table                = 'data_jenis_simpanan';
	protected $primaryKey           = 'id';
	protected $useAutoIncrement     = true;
	protected $insertID             = 0;
	protected $returnType           = 'array';
	protected $useSoftDelete        = false;
	protected $protectFields        = true;
	protected $allowedFields        = [
		'nama_simpanan',
		'total',
		'flag'
	];

	// Dates
	protected $useTimestamps        = true;
	protected $dateFormat           = 'datetime';
	protected $createdField         = 'created_at';
	protected $updatedField         = 'updated_at';
	protected $deletedField         = 'deleted_at';
}