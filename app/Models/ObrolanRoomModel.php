<?php

namespace App\Models;

use CodeIgniter\Model;
use Faker\Factory;

class ObrolanRoomModel extends Model
{
	protected $DBGroup              = 'default';
	protected $table                = 'obrolan_room';
	protected $primaryKey           = 'id';
	protected $useAutoIncrement     = false;
	protected $insertID             = 0;
	protected $returnType           = 'array';
	protected $useSoftDelete        = false;
	protected $protectFields        = true;
	protected $allowedFields        = [
		'role',
		'user_id',
		'nama_room',
		'pesan_terakhir',
		'pesan_baru_admin',
		'pesan_baru_member',
	];

	// Dates
	protected $useTimestamps        = true;
	protected $dateFormat           = 'datetime';
	protected $createdField         = 'created_at';
	protected $updatedField         = 'updated_at';

	// Validation
	protected $validationRules      = [];
	protected $validationMessages   = [];
	protected $skipValidation       = false;
	protected $cleanValidationRules = true;

	// Callbacks
	protected $allowCallbacks       = true;
	protected $beforeInsert         = ['genrateID'];
	protected $afterInsert          = [];
	protected $beforeUpdate         = [];
	protected $afterUpdate          = [];
	protected $beforeFind           = [];
	protected $afterFind            = [];
	protected $beforeDelete         = [];
	protected $afterDelete          = [];


	public function genrateID(array $data)
	{
		$faker = Factory::create();
		$id = '';
		for ($i = 0; $i < 5; $i++) {
			$id .= $faker->bothify('?##?');
			$id .= strtoupper($faker->bothify('?##?'));
			$id = $faker->shuffle($id);
		}
		$data['data']['id'] = $id;

		return $data;
	}

	public function countChatAdmin()
	{
		return $this->db->table($this->table)
			->select('count(id) as notif')
			->where('pesan_baru_member >', '0')
			->get()
			->getRowArray();
	}

	public function readAdmin($id)
	{
		$data['pesan_baru_member'] = 0;

		$this->db->table($this->table)
			->where('id', $id)
			->update($data);

		$dataRoom['read'] = 1;
		$this->db->table('obrolan')
			->where('room_id', $id)
			->where('(role = 1 OR role = 2)')
			->update($dataRoom);
	}

	public function readMember($id)
	{
		$data['pesan_baru_admin'] = 0;

		$this->db->table($this->table)
			->where('id', $id)
			->update($data);
	}
}
