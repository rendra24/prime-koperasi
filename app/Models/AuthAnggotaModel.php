<?php
 
namespace App\Models;
 
use CodeIgniter\Model;
use Exception;

class AuthAnggotaModel extends Model{
 
    protected $table = "anggota";
    

    public function register($data)
    {
        
        $query = $this->db->table($this->table)->insert($data);
        return $query ? true : false;
    }
                                
    public function findUserByEmailAddress(string $emailAddress)
    {
        $user = $this->db->table($this->table)->where('nrp', $emailAddress)->get()->getRowArray();

        if (!$user) {
            throw new Exception('Anggota belum terdaftar , cek nrp kamu');
        }

        if($user['flag'] == 0){
            throw new Exception('Anggota tidak aktif ');
        }

        return $user;
    }
}