<?php

namespace App\Models;

use CodeIgniter\Model;

class AuthGroupModel extends Model
{
	protected $DBGroup              = 'default';
	protected $table                = 'auth_groups';
	protected $primaryKey           = 'id';
	protected $useAutoIncrement     = true;
	protected $insertID             = 0;
	protected $returnType           = 'array';
	protected $useSoftDelete        = false;
	protected $protectFields        = true;
	protected $allowedFields        = [
		'name',
		'description'
	];

	// Dates
	protected $useTimestamps        = false;

	// Validation
	protected $validationRules      = [];
	protected $validationMessages   = [];
	protected $skipValidation       = false;
	protected $cleanValidationRules = true;

	// Callbacks
	protected $allowCallbacks       = true;
	protected $beforeInsert         = [];
	protected $afterInsert          = [];
	protected $beforeUpdate         = [];
	protected $afterUpdate          = [];
	protected $beforeFind           = [];
	protected $afterFind            = [];
	protected $beforeDelete         = [];
	protected $afterDelete          = [];

	public function getGroupPermissions($id)
	{
		return $this->db->table('auth_groups_permissions')
			->where('group_id', $id)
			->get()
			->getResultArray();
	}
	public function saveGroupPermissions($data)
	{
		$this->db->table('auth_groups_permissions')
			->insert($data);
	}
	public function cleanGroupPermission($id)
	{
		$this->db->table('auth_groups_permissions')
			->where('group_id', $id)
			->delete();
	}
	public function saveGroupUsers($data)
	{
		$this->db->table('auth_groups_users')
			->insert($data);
	}
}
