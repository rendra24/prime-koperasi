<?php

namespace App\Models;

use CodeIgniter\Model;

class MasterAsset extends Model
{
	protected $DBGroup              = 'default';
	protected $table                = 'data_asset';
	protected $primaryKey           = 'id';
	protected $useAutoIncrement     = true;
	protected $insertID             = 0;
	protected $returnType           = 'array';
	protected $useSoftDelete        = false;
	protected $protectFields        = true;
	protected $allowedFields        = [
		'nama_asset',
		'id_jenis_asset',
		'total',
		'nama_jenis_asset',
		'flag'
	];

	// Dates
	protected $useTimestamps        = true;
	protected $dateFormat           = 'datetime';
	protected $createdField         = 'created_at';
	protected $updatedField         = 'updated_at';
	protected $deletedField         = 'deleted_at';

	public function getsaldo($id){
		$builder = $this->db->table($this->table)
							->where('id',$id);
		
		return $builder->get();
	}
}