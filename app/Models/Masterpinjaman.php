<?php

namespace App\Models;

use CodeIgniter\Model;

class Masterpinjaman extends Model
{
	protected $DBGroup              = 'default';
	protected $table                = 'data_pinjaman';
	protected $primaryKey           = 'id';
	protected $useAutoIncrement     = true;
	protected $insertID             = 0;
	protected $returnType           = 'array';
	protected $useSoftDelete        = false;
	protected $protectFields        = true;
	protected $allowedFields        = [
		'nama_pinjaman',
		'bunga',
		'flag'
	];

	// Dates
	protected $useTimestamps        = true;
	protected $dateFormat           = 'datetime';
	protected $createdField         = 'created_at';
	protected $updatedField         = 'updated_at';
	protected $deletedField         = 'deleted_at';
}