<?php

namespace App\Models;

use CodeIgniter\Model;

class Ppob extends Model
{
	protected $DBGroup              = 'default';
	protected $table                = 'ppob';
	protected $primaryKey           = 'id';
	protected $useAutoIncrement     = true;
	protected $insertID             = 0;
	protected $returnType           = 'array';
	protected $useSoftDelete        = false;
	protected $protectFields        = true;
	protected $allowedFields        = [
		'nama_layanan',
		'icon',
		'link',
		'flag'
	];

}