<?php

namespace App\Models\Ppob;

use CodeIgniter\Model;

class TransaksiPpobTemp extends Model
{
	protected $DBGroup              = 'default';
	protected $table                = 'ppob_transaksi_temp';
	protected $primaryKey           = 'id';
	protected $useAutoIncrement     = true;
	protected $insertID             = 0;
	protected $returnType           = 'array';
	protected $useSoftDelete        = false;
	protected $protectFields        = true;
	protected $allowedFields        = [
		'tr_id',
		'anggota_id',
		'ref_id',
		'customer_id',
		'product_code',
		'price',
		'price_sell',
		'status',
		'message',
		'created_at',
	];

}