<?php

namespace App\Models\Ppob;

use CodeIgniter\Model;

class TransaksiPln extends Model
{
	protected $DBGroup              = 'default';
	protected $table                = 'ppob_transaksi_pln';
	protected $primaryKey           = 'id';
	protected $useAutoIncrement     = true;
	protected $insertID             = 0;
	protected $returnType           = 'array';
	protected $useSoftDelete        = false;
	protected $protectFields        = true;
	protected $allowedFields        = [
		'id_ppob_transaksi',
		'tr_name',
		'period',
		'nominal',
		'admin',
		'message',
		'price',
		'selling_price',
		'balance',
		'lembar_tagihan',
		'meter_awal',
		'meter_akhir',
		'denda',
		'total',
	];

}