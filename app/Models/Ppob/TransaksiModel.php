<?php

namespace App\Models\Ppob;

use CodeIgniter\Model;

class TransaksiModel extends Model
{
	protected $DBGroup              = 'default';
	protected $table                = 'ppob_transaksi';
	protected $primaryKey           = 'id';
	protected $useAutoIncrement     = true;
	protected $insertID             = 0;
	protected $returnType           = 'array';
	protected $useSoftDelete        = false;
	protected $protectFields        = true;
	protected $allowedFields        = [
		'anggota_id',
		'ref_id',
		'customer_id',
		'product_code',
		'price',
		'price_sell',
		'keuntungan',
		'status',
		'message',
		'sn',
		'created_at',
	];

	// Dates
	protected $useTimestamps        = false;
	protected $dateFormat           = 'datetime';
	protected $createdField         = 'created_at';
	protected $updatedField         = 'updated_at';
	protected $deletedField         = 'deleted_at';

	// Validation
	protected $validationRules      = [];
	protected $validationMessages   = [];
	protected $skipValidation       = false;
	protected $cleanValidationRules = true;

	// Callbacks
	protected $allowCallbacks       = true;
	protected $beforeInsert         = [];
	protected $afterInsert          = [];
	protected $beforeUpdate         = [];
	protected $afterUpdate          = [];
	protected $beforeFind           = [];
	protected $afterFind            = [];
	protected $beforeDelete         = [];
	protected $afterDelete          = [];

	public function LaporanPpob($post)
	{
		$get = $this->table('ppob_transaksi')
					->select('ppob_transaksi.*, agt.nama')
					->join('anggota as agt','ppob_transaksi.anggota_id = agt.id','LEFT')
					->orderBy('ppob_transaksi.created_at','DESC');

					if(!empty($post['start_date'])){
						$start_date = $post['start_date'];
						$end_date = $post['end_date'];
						$get->where("DATE(ppob_transaksi.created_at) BETWEEN '$start_date' AND '$end_date' ");
					}

		return $get->get();
	}
}